CREATE TABLE `wp_wpc_client_objects_assigns` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `object_type` enum('file','file_category','portal_page','portal_page_category','post_category','ez_hub','manager','feedback_wizard','invoice','accum_invoice','repeat_invoice','estimate','request_estimate','shutter','shutter_category','form','brand','campaign','shutter_order','chain','new_message','trash_chain','archive_chain','ticket','private_post','ams_service','ams_level') COLLATE utf8mb4_unicode_ci NOT NULL,
  `object_id` bigint(20) DEFAULT NULL,
  `assign_type` enum('circle','client','email_list') COLLATE utf8mb4_unicode_ci NOT NULL,
  `assign_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `objectid_assignid` (`object_id`,`assign_id`),
  KEY `objectid` (`object_id`),
  KEY `assignid` (`assign_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci