CREATE TABLE `wp_wpc_client_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` text COLLATE utf8mb4_unicode_ci,
  `type` enum('file','portal_page','shutter','shutter_size','ticket_cats','ticket_types') COLLATE utf8mb4_unicode_ci NOT NULL,
  `cat_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci