CREATE TABLE `wp_wpc_client_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `time` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_download` text COLLATE utf8mb4_unicode_ci,
  `size` int(32) NOT NULL,
  `filename` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `cat_id` int(11) DEFAULT NULL,
  `protect_url` tinyint(1) DEFAULT NULL,
  `external` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci