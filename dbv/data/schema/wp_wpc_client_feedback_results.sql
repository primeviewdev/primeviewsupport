CREATE TABLE `wp_wpc_client_feedback_results` (
  `result_id` int(11) NOT NULL AUTO_INCREMENT,
  `wizard_id` int(11) NOT NULL DEFAULT '0',
  `wizard_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `wizard_version` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `result_text` text COLLATE utf8mb4_unicode_ci,
  `time` text COLLATE utf8mb4_unicode_ci,
  `client_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`result_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci