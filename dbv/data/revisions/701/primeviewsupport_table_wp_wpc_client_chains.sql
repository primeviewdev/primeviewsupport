
-- --------------------------------------------------------

--
-- Table structure for table `wp_wpc_client_chains`
--

DROP TABLE IF EXISTS `wp_wpc_client_chains`;
CREATE TABLE `wp_wpc_client_chains` (
  `id` int(11) NOT NULL,
  `subject` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `wp_wpc_client_chains`
--

TRUNCATE TABLE `wp_wpc_client_chains`;
--
-- Dumping data for table `wp_wpc_client_chains`
--

INSERT INTO `wp_wpc_client_chains` (`id`, `subject`) VALUES
(1, 'Test Support Issue');
