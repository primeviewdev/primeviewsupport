
-- --------------------------------------------------------

--
-- Table structure for table `wp_cf7dbplugin_submits`
--

DROP TABLE IF EXISTS `wp_cf7dbplugin_submits`;
CREATE TABLE `wp_cf7dbplugin_submits` (
  `submit_time` decimal(16,4) NOT NULL,
  `form_name` varchar(127) CHARACTER SET utf8 DEFAULT NULL,
  `field_name` varchar(127) CHARACTER SET utf8 DEFAULT NULL,
  `field_value` longtext CHARACTER SET utf8,
  `field_order` int(11) DEFAULT NULL,
  `file` longblob
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `wp_cf7dbplugin_submits`
--

TRUNCATE TABLE `wp_cf7dbplugin_submits`;