
-- --------------------------------------------------------

--
-- Table structure for table `wp_wpc_temp_ids`
--

DROP TABLE IF EXISTS `wp_wpc_temp_ids`;
CREATE TABLE `wp_wpc_temp_ids` (
  `block_key` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `wp_wpc_temp_ids`
--

TRUNCATE TABLE `wp_wpc_temp_ids`;