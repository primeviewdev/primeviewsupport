
-- --------------------------------------------------------

--
-- Table structure for table `wp_wpc_client_groups`
--

DROP TABLE IF EXISTS `wp_wpc_client_groups`;
CREATE TABLE `wp_wpc_client_groups` (
  `group_id` int(11) NOT NULL,
  `group_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `auto_select` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `auto_add_files` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `auto_add_pps` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `auto_add_manual` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `auto_add_self` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `wp_wpc_client_groups`
--

TRUNCATE TABLE `wp_wpc_client_groups`;