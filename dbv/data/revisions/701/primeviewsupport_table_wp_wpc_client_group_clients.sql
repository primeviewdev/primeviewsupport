
-- --------------------------------------------------------

--
-- Table structure for table `wp_wpc_client_group_clients`
--

DROP TABLE IF EXISTS `wp_wpc_client_group_clients`;
CREATE TABLE `wp_wpc_client_group_clients` (
  `group_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `wp_wpc_client_group_clients`
--

TRUNCATE TABLE `wp_wpc_client_group_clients`;