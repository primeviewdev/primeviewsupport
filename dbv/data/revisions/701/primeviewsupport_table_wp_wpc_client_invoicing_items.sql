
-- --------------------------------------------------------

--
-- Table structure for table `wp_wpc_client_invoicing_items`
--

DROP TABLE IF EXISTS `wp_wpc_client_invoicing_items`;
CREATE TABLE `wp_wpc_client_invoicing_items` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `rate` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `use_r_est` int(1) DEFAULT NULL,
  `data` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `wp_wpc_client_invoicing_items`
--

TRUNCATE TABLE `wp_wpc_client_invoicing_items`;
--
-- Dumping data for table `wp_wpc_client_invoicing_items`
--

INSERT INTO `wp_wpc_client_invoicing_items` (`id`, `name`, `description`, `rate`, `use_r_est`, `data`) VALUES
(1, 'Test Work on WEbsiite', '', '1600.00', 0, 'a:0:{}'),
(2, 'work', 'test design project', '20000.00', 0, 'a:0:{}'),
(3, '300 Hours of development ', '', '30000.00', 0, 'a:0:{}'),
(4, '12 hours of updates', 'updates', '2400.00', 0, 'a:0:{}'),
(5, '12 hours dev time', 'updates to website', '2400.00', 0, 'a:0:{}'),
(6, 'dev time', '3 hours', '300.00', 0, 'a:0:{}');
