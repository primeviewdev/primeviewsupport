
-- --------------------------------------------------------

--
-- Table structure for table `wp_wpc_client_files_download_log`
--

DROP TABLE IF EXISTS `wp_wpc_client_files_download_log`;
CREATE TABLE `wp_wpc_client_files_download_log` (
  `id` int(11) NOT NULL,
  `file_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `download_date` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `wp_wpc_client_files_download_log`
--

TRUNCATE TABLE `wp_wpc_client_files_download_log`;