
-- --------------------------------------------------------

--
-- Table structure for table `wp_wpc_client_clients_page`
--

DROP TABLE IF EXISTS `wp_wpc_client_clients_page`;
CREATE TABLE `wp_wpc_client_clients_page` (
  `id` mediumint(9) NOT NULL,
  `pagename` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `template` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `users` tinytext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `wp_wpc_client_clients_page`
--

TRUNCATE TABLE `wp_wpc_client_clients_page`;