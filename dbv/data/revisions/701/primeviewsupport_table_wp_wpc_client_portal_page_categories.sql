
-- --------------------------------------------------------

--
-- Table structure for table `wp_wpc_client_portal_page_categories`
--

DROP TABLE IF EXISTS `wp_wpc_client_portal_page_categories`;
CREATE TABLE `wp_wpc_client_portal_page_categories` (
  `cat_id` int(11) NOT NULL,
  `cat_name` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `wp_wpc_client_portal_page_categories`
--

TRUNCATE TABLE `wp_wpc_client_portal_page_categories`;