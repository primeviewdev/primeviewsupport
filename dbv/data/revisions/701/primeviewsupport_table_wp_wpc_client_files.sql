
-- --------------------------------------------------------

--
-- Table structure for table `wp_wpc_client_files`
--

DROP TABLE IF EXISTS `wp_wpc_client_files`;
CREATE TABLE `wp_wpc_client_files` (
  `id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `time` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_download` text COLLATE utf8mb4_unicode_ci,
  `size` int(32) NOT NULL,
  `filename` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `cat_id` int(11) DEFAULT NULL,
  `protect_url` tinyint(1) DEFAULT NULL,
  `external` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `wp_wpc_client_files`
--

TRUNCATE TABLE `wp_wpc_client_files`;
--
-- Dumping data for table `wp_wpc_client_files`
--

INSERT INTO `wp_wpc_client_files` (`id`, `order_id`, `user_id`, `page_id`, `time`, `last_download`, `size`, `filename`, `name`, `title`, `description`, `cat_id`, `protect_url`, `external`) VALUES
(1, NULL, 1, 0, '1555092532', '', 2140, '1955primeview-logo.png', 'primeview-logo.png', 'primeview-logo.png', 'undefined', 1, NULL, 0);
