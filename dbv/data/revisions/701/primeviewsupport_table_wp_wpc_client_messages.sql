
-- --------------------------------------------------------

--
-- Table structure for table `wp_wpc_client_messages`
--

DROP TABLE IF EXISTS `wp_wpc_client_messages`;
CREATE TABLE `wp_wpc_client_messages` (
  `id` int(11) NOT NULL,
  `chain_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `wp_wpc_client_messages`
--

TRUNCATE TABLE `wp_wpc_client_messages`;
--
-- Dumping data for table `wp_wpc_client_messages`
--

INSERT INTO `wp_wpc_client_messages` (`id`, `chain_id`, `author_id`, `content`, `date`) VALUES
(1, 1, 1, 'Sorry you\\\'re having those challenges. The team is on it,', '1554937396'),
(2, 1, 4, 'I dont care if you\\\'re sorry. Fix it now!!!!!!! ', '1554937457'),
(3, 1, 1, 'calm down there sir\n', '1554938076');
