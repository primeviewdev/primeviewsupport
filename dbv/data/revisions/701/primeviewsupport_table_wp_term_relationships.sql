
-- --------------------------------------------------------

--
-- Table structure for table `wp_term_relationships`
--

DROP TABLE IF EXISTS `wp_term_relationships`;
CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `wp_term_relationships`
--

TRUNCATE TABLE `wp_term_relationships`;
--
-- Dumping data for table `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(14, 2, 0),
(19, 2, 0),
(25, 2, 0),
(27, 2, 0),
(29, 2, 0),
(31, 2, 0),
(33, 2, 0),
(36, 2, 0),
(38, 2, 0),
(40, 2, 0),
(42, 2, 0),
(44, 2, 0),
(46, 2, 0),
(48, 2, 0),
(50, 2, 0),
(52, 2, 0),
(54, 2, 0),
(56, 2, 0),
(58, 2, 0),
(60, 2, 0),
(62, 2, 0),
(64, 2, 0),
(66, 2, 0),
(68, 2, 0),
(70, 2, 0),
(72, 2, 0),
(74, 2, 0),
(76, 2, 0),
(78, 2, 0),
(80, 2, 0),
(82, 2, 0),
(84, 2, 0),
(86, 2, 0),
(88, 2, 0),
(90, 2, 0),
(92, 2, 0),
(94, 2, 0),
(96, 2, 0),
(99, 2, 0),
(104, 2, 0),
(106, 2, 0),
(110, 2, 0),
(1139, 2, 0),
(1171, 5, 0),
(1227, 5, 0),
(1452, 7, 0),
(1489, 7, 0),
(1492, 7, 0),
(1558, 7, 0),
(1559, 7, 0),
(1560, 7, 0),
(1561, 7, 0),
(1562, 7, 0),
(1563, 7, 0),
(1564, 7, 0),
(1565, 7, 0),
(1566, 7, 0),
(1567, 7, 0),
(1568, 7, 0),
(1569, 7, 0),
(1570, 7, 0),
(1571, 7, 0),
(1572, 7, 0),
(1573, 8, 0),
(1574, 8, 0),
(1575, 8, 0),
(1576, 8, 0),
(1577, 8, 0),
(1578, 8, 0),
(1579, 8, 0),
(1580, 8, 0),
(1581, 8, 0),
(1582, 8, 0),
(1583, 8, 0),
(1584, 8, 0),
(1585, 8, 0),
(1586, 8, 0);
