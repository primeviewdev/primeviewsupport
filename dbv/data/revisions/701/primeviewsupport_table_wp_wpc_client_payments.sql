
-- --------------------------------------------------------

--
-- Table structure for table `wp_wpc_client_payments`
--

DROP TABLE IF EXISTS `wp_wpc_client_payments`;
CREATE TABLE `wp_wpc_client_payments` (
  `id` int(11) NOT NULL,
  `order_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_status` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `function` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_method` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_type` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `amount` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8mb4_unicode_ci,
  `transaction_id` text COLLATE utf8mb4_unicode_ci,
  `transaction_status` text COLLATE utf8mb4_unicode_ci,
  `time_created` text COLLATE utf8mb4_unicode_ci,
  `time_paid` text COLLATE utf8mb4_unicode_ci,
  `subscription_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subscription_status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `next_payment_date` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `wp_wpc_client_payments`
--

TRUNCATE TABLE `wp_wpc_client_payments`;
--
-- Dumping data for table `wp_wpc_client_payments`
--

INSERT INTO `wp_wpc_client_payments` (`id`, `order_id`, `order_status`, `function`, `payment_method`, `payment_type`, `client_id`, `amount`, `currency`, `data`, `transaction_id`, `transaction_status`, `time_created`, `time_paid`, `subscription_id`, `subscription_status`, `next_payment_date`) VALUES
(1, '39a2c2b63d5f', 'selected_gateway', 'invoicing', 'paypal-express', 'one_time', 4, '300', 'USD', '{\"item_name\":\"11100000000001\",\"invoice_id\":\"1482\"}', NULL, NULL, '1555098019', NULL, NULL, NULL, NULL);
