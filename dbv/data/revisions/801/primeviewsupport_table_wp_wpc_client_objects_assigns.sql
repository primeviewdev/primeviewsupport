
-- --------------------------------------------------------

--
-- Table structure for table `wp_wpc_client_objects_assigns`
--

DROP TABLE IF EXISTS `wp_wpc_client_objects_assigns`;
CREATE TABLE IF NOT EXISTS `wp_wpc_client_objects_assigns` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `object_type` enum('file','file_category','portal_page','portal_page_category','post_category','ez_hub','manager','feedback_wizard','invoice','accum_invoice','repeat_invoice','estimate','request_estimate','shutter','shutter_category','form','brand','campaign','shutter_order','chain','new_message','trash_chain','archive_chain','ticket','private_post','ams_service','ams_level') COLLATE utf8mb4_unicode_ci NOT NULL,
  `object_id` bigint(20) DEFAULT NULL,
  `assign_type` enum('circle','client','email_list') COLLATE utf8mb4_unicode_ci NOT NULL,
  `assign_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `objectid_assignid` (`object_id`,`assign_id`),
  KEY `objectid` (`object_id`),
  KEY `assignid` (`assign_id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_wpc_client_objects_assigns`
--

INSERT INTO `wp_wpc_client_objects_assigns` (`id`, `object_type`, `object_id`, `assign_type`, `assign_id`) VALUES
(42, 'portal_page', 1204, 'client', 4),
(43, '', 1158, 'client', 4),
(44, '', 1158, 'client', 4),
(50, 'portal_page', 1332, 'client', 4),
(51, 'chain', 1, 'client', 4),
(52, 'chain', 1, 'client', 1),
(55, 'new_message', 3, 'client', 4),
(56, 'file', 1, 'client', 4),
(57, 'portal_page', 1477, 'client', 4),
(58, 'invoice', 1482, 'client', 4),
(59, 'portal_page', 1483, 'client', 4),
(60, 'portal_page', 1214, 'client', 4);
