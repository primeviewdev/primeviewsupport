
-- --------------------------------------------------------

--
-- Table structure for table `wp_wpc_client_feedback_wizards`
--

DROP TABLE IF EXISTS `wp_wpc_client_feedback_wizards`;
CREATE TABLE IF NOT EXISTS `wp_wpc_client_feedback_wizards` (
  `wizard_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `feedback_type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `version` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `clients_id` text COLLATE utf8mb4_unicode_ci,
  `groups_id` text COLLATE utf8mb4_unicode_ci,
  `time` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`wizard_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
