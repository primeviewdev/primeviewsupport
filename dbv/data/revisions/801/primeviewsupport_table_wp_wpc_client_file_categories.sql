
-- --------------------------------------------------------

--
-- Table structure for table `wp_wpc_client_file_categories`
--

DROP TABLE IF EXISTS `wp_wpc_client_file_categories`;
CREATE TABLE IF NOT EXISTS `wp_wpc_client_file_categories` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` text COLLATE utf8mb4_unicode_ci,
  `folder_name` text COLLATE utf8mb4_unicode_ci,
  `cat_order` int(11) DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_wpc_client_file_categories`
--

INSERT INTO `wp_wpc_client_file_categories` (`cat_id`, `cat_name`, `folder_name`, `cat_order`, `parent_id`) VALUES
(1, 'General', 'general', 1, 0);
