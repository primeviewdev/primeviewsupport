
-- --------------------------------------------------------

--
-- Table structure for table `wp_wpc_client_feedback_items`
--

DROP TABLE IF EXISTS `wp_wpc_client_feedback_items`;
CREATE TABLE IF NOT EXISTS `wp_wpc_client_feedback_items` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `file_name` text COLLATE utf8mb4_unicode_ci,
  `file` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
