
-- --------------------------------------------------------

--
-- Table structure for table `wp_wpc_client_login_redirects`
--

DROP TABLE IF EXISTS `wp_wpc_client_login_redirects`;
CREATE TABLE IF NOT EXISTS `wp_wpc_client_login_redirects` (
  `rul_type` enum('user','circle','role','level','all') COLLATE utf8mb4_unicode_ci NOT NULL,
  `rul_value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `rul_url` longtext COLLATE utf8mb4_unicode_ci,
  `rul_first_url` longtext COLLATE utf8mb4_unicode_ci,
  `rul_url_logout` longtext COLLATE utf8mb4_unicode_ci,
  `rul_order` int(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
