
-- --------------------------------------------------------

--
-- Table structure for table `wp_wpc_client_feedback_wizard_items`
--

DROP TABLE IF EXISTS `wp_wpc_client_feedback_wizard_items`;
CREATE TABLE IF NOT EXISTS `wp_wpc_client_feedback_wizard_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wizard_id` int(11) NOT NULL DEFAULT '0',
  `item_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
