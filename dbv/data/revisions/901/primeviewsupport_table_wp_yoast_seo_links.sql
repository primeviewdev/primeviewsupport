
-- --------------------------------------------------------

--
-- Table structure for table `wp_yoast_seo_links`
--

DROP TABLE IF EXISTS `wp_yoast_seo_links`;
CREATE TABLE IF NOT EXISTS `wp_yoast_seo_links` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL,
  `target_post_id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `link_direction` (`post_id`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=929 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_yoast_seo_links`
--

INSERT INTO `wp_yoast_seo_links` (`id`, `url`, `post_id`, `target_post_id`, `type`) VALUES
(705, '#', 1158, 0, 'internal'),
(706, '#', 1158, 0, 'internal'),
(707, '/portal/hub-page/', 1214, 0, 'internal'),
(708, 'http://support.local.com/events/2019-04-18/', 1214, 0, 'internal'),
(709, 'http://support.local.com/event/biltmore-test-meeting/', 1214, 1227, 'internal'),
(710, '[[=permalink]]', 1214, 0, 'internal'),
(711, '[[=permalink]]', 1214, 0, 'internal'),
(712, '[[=permalink]]', 1214, 0, 'internal'),
(875, 'http://kb.mailchimp.com', 1542, 0, 'external'),
(876, 'http://fast.wistia.net/embed/iframe/wxl2m43r4z?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600&amp;plugin%5Bcaptions-v1%5D%5BonByDefault%5D=false', 1542, 0, 'external'),
(877, 'http://fast.wistia.net/embed/iframe/vfcimgx8av?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600&amp;plugin%5Bcaptions-v1%5D%5BonByDefault%5D=falsess', 1542, 0, 'external'),
(878, 'http://fast.wistia.net/embed/iframe/y208aiqfxz?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600&amp;plugin%5Bcaptions-v1%5D%5BonByDefault%5D=false', 1542, 0, 'external'),
(879, 'http://fast.wistia.net/embed/iframe/2ywwm3j48y?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600&amp;plugin%5Bcaptions-v1%5D%5BonByDefault%5D=false', 1542, 0, 'external'),
(880, 'http://fast.wistia.net/embed/iframe/4eof2yo3vj?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600&amp;plugin%5Bcaptions-v1%5D%5BonByDefault%5D=false', 1542, 0, 'external'),
(881, 'http://fast.wistia.net/embed/iframe/r6r6w52sem?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600&amp;plugin%5Bcaptions-v1%5D%5BonByDefault%5D=false', 1542, 0, 'external'),
(882, 'http://fast.wistia.net/embed/iframe/0h6fhha18e?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600&amp;plugin%5Bcaptions-v1%5D%5BonByDefault%5D=false', 1542, 0, 'external'),
(883, 'http://fast.wistia.net/embed/iframe/7e7t033oyj?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600&amp;plugin%5Bcaptions-v1%5D%5BonByDefault%5D=false', 1542, 0, 'external'),
(884, 'http://fast.wistia.net/embed/iframe/pmubfohrcb?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600&amp;plugin%5Bcaptions-v1%5D%5BonByDefault%5D=false', 1542, 0, 'external'),
(885, 'http://fast.wistia.net/embed/iframe/z23mk89c22?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600&amp;plugin%5Bcaptions-v1%5D%5BonByDefault%5D=false', 1542, 0, 'external'),
(886, 'http://fast.wistia.net/embed/iframe/5ou4sscmze?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600&amp;plugin%5Bcaptions-v1%5D%5BonByDefault%5D=false', 1542, 0, 'external'),
(887, 'http://fast.wistia.net/embed/iframe/sdeiq9vzbn?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600&amp;plugin%5Bcaptions-v1%5D%5BonByDefault%5D=false', 1542, 0, 'external'),
(888, 'http://fast.wistia.net/embed/iframe/hl39q02yxg?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600&amp;plugin%5Bcaptions-v1%5D%5BonByDefault%5D=false', 1542, 0, 'external'),
(889, 'http://fast.wistia.net/embed/iframe/h1uv75dsn8?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600&amp;plugin%5Bcaptions-v1%5D%5BonByDefault%5D=false', 1542, 0, 'external'),
(890, 'http://fast.wistia.net/embed/iframe/a924jh4mvg?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600&amp;plugin%5Bcaptions-v1%5D%5BonByDefault%5D=false', 1542, 0, 'external'),
(891, 'http://fast.wistia.net/embed/iframe/tdm1ympl3a?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600', 1542, 0, 'external'),
(892, 'http://fast.wistia.net/embed/iframe/uli3bnht43?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600&amp;plugin%5Bcaptions-v1%5D%5BonByDefault%5D=false', 1542, 0, 'external'),
(893, 'http://fast.wistia.net/embed/iframe/w2f1d799pw?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600&amp;plugin%5Bcaptions-v1%5D%5BonByDefault%5D=false', 1542, 0, 'external'),
(894, 'http://fast.wistia.net/embed/iframe/46llqn8jgu?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600&amp;plugin%5Bcaptions-v1%5D%5BonByDefault%5D=false', 1542, 0, 'external'),
(895, 'http://fast.wistia.net/embed/iframe/17ka2vhdry?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600&amp;plugin%5Bcaptions-v1%5D%5BonByDefault%5D=false', 1542, 0, 'external'),
(896, 'http://fast.wistia.net/embed/iframe/21cmdckjhx?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600&amp;plugin%5Bcaptions-v1%5D%5BonByDefault%5D=false', 1542, 0, 'external'),
(897, 'http://fast.wistia.net/embed/iframe/gkv9cspq7p?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600&amp;plugin%5Bcaptions-v1%5D%5BonByDefault%5D=false', 1542, 0, 'external'),
(898, 'http://fast.wistia.net/embed/iframe/jka438cy9q?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600&amp;plugin%5Bcaptions-v1%5D%5BonByDefault%5D=false', 1542, 0, 'external'),
(899, 'http://fast.wistia.net/embed/iframe/zv073f7dib?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600&amp;plugin%5Bcaptions-v1%5D%5BonByDefault%5D=false', 1542, 0, 'external'),
(900, 'http://fast.wistia.net/embed/iframe/eg6dgxte90?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600&amp;plugin%5Bcaptions-v1%5D%5BonByDefault%5D=false', 1542, 0, 'external'),
(901, 'http://fast.wistia.net/embed/iframe/wcv59vxk8h?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600&amp;plugin%5Bcaptions-v1%5D%5BonByDefault%5D=false', 1542, 0, 'external'),
(902, 'http://fast.wistia.net/embed/iframe/drjjjunkbl?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600&amp;plugin%5Bcaptions-v1%5D%5BonByDefault%5D=false', 1542, 0, 'external'),
(903, 'http://fast.wistia.net/embed/iframe/y0ielbc2kb?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600&amp;plugin%5Bcaptions-v1%5D%5BonByDefault%5D=false', 1542, 0, 'external'),
(904, 'http://fast.wistia.net/embed/iframe/clhzw5365e?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600&amp;plugin%5Bcaptions-v1%5D%5BonByDefault%5D=false', 1542, 0, 'external'),
(905, 'http://fast.wistia.net/embed/iframe/2lx74jjcer?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600&amp;plugin%5Bcaptions-v1%5D%5BonByDefault%5D=false', 1542, 0, 'external'),
(906, 'http://fast.wistia.net/embed/iframe/vrexa78nam?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600', 1542, 0, 'external'),
(907, 'http://fast.wistia.net/embed/iframe/y22tdw7xo3?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600', 1542, 0, 'external'),
(908, 'http://fast.wistia.net/embed/iframe/dbebbec831?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600', 1542, 0, 'external'),
(909, 'http://fast.wistia.net/embed/iframe/0af748c30d?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600', 1542, 0, 'external'),
(910, 'http://fast.wistia.net/embed/iframe/2gglesua89?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600', 1542, 0, 'external'),
(911, 'http://fast.wistia.net/embed/iframe/pephp5nh1s?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600', 1542, 0, 'external'),
(912, 'http://fast.wistia.net/embed/iframe/al59v81l7a?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600&amp;plugin%5Bcaptions-v1%5D%5BonByDefault%5D=false', 1542, 0, 'external'),
(913, 'http://fast.wistia.net/embed/iframe/k3rz8mlh3q?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600&amp;plugin%5Bcaptions-v1%5D%5BonByDefault%5D=false', 1542, 0, 'external'),
(914, 'http://fast.wistia.net/embed/iframe/8qkuko7q5c?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600&amp;plugin%5Bcaptions-v1%5D%5BonByDefault%5D=false', 1542, 0, 'external'),
(915, 'http://fast.wistia.net/embed/iframe/n2v73v1ulm?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600&amp;plugin%5Bcaptions-v1%5D%5BonByDefault%5D=false', 1542, 0, 'external'),
(916, 'http://fast.wistia.net/embed/iframe/dd6f518e3g?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600&amp;plugin%5Bcaptions-v1%5D%5BonByDefault%5D=false', 1542, 0, 'external'),
(917, 'http://fast.wistia.net/embed/iframe/2ndbj1pdel?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600&amp;plugin%5Bcaptions-v1%5D%5BonByDefault%5D=false', 1542, 0, 'external'),
(918, 'http://fast.wistia.net/embed/iframe/79661gm5oi?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600&amp;plugin%5Bcaptions-v1%5D%5BonByDefault%5D=false', 1542, 0, 'external'),
(919, 'http://fast.wistia.net/embed/iframe/p8pcduh1h5?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600&amp;plugin%5Bcaptions-v1%5D%5BonByDefault%5D=false', 1542, 0, 'external'),
(920, 'http://fast.wistia.net/embed/iframe/rzrbx9sb1f?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600&amp;plugin%5Bcaptions-v1%5D%5BonByDefault%5D=false', 1542, 0, 'external'),
(921, 'http://fast.wistia.net/embed/iframe/ogqos86f6i?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600&amp;plugin%5Bcaptions-v1%5D%5BonByDefault%5D=false', 1542, 0, 'external'),
(922, 'http://fast.wistia.net/embed/iframe/n84pk0c05l?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600&amp;plugin%5Bcaptions-v1%5D%5BonByDefault%5D=false', 1542, 0, 'external'),
(923, 'http://fast.wistia.net/embed/iframe/vc84qhznep?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600&amp;plugin%5Bcaptions-v1%5D%5BonByDefault%5D=false', 1542, 0, 'external'),
(924, 'http://fast.wistia.net/embed/iframe/waecpw1yjn?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600&amp;plugin%5Bcaptions-v1%5D%5BonByDefault%5D=false', 1542, 0, 'external'),
(925, 'http://fast.wistia.net/embed/iframe/tjxoabiqj6?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600&amp;plugin%5Bcaptions-v1%5D%5BonByDefault%5D=false', 1542, 0, 'external'),
(926, 'http://fast.wistia.net/embed/iframe/b9rvzjkhlk?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600&amp;plugin%5Bcaptions-v1%5D%5BonByDefault%5D=false', 1542, 0, 'external'),
(927, 'http://fast.wistia.net/embed/iframe/h6t8z6uq2k?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600', 1542, 0, 'external'),
(928, 'http://fast.wistia.net/embed/iframe/a924jh4mvg?autoPlay=true&amp;controlsVisibleOnLoad=true&amp;popover=true&amp;version=v1&amp;videoHeight=400&amp;videoWidth=600&amp;plugin%5Bcaptions-v1%5D%5BonByDefault%5D=false', 1542, 0, 'external');
