
-- --------------------------------------------------------

--
-- Table structure for table `wp_wpc_client_sms_broadcasts_send`
--

DROP TABLE IF EXISTS `wp_wpc_client_sms_broadcasts_send`;
CREATE TABLE IF NOT EXISTS `wp_wpc_client_sms_broadcasts_send` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `broadcast_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
