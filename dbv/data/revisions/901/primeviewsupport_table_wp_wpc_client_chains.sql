
-- --------------------------------------------------------

--
-- Table structure for table `wp_wpc_client_chains`
--

DROP TABLE IF EXISTS `wp_wpc_client_chains`;
CREATE TABLE IF NOT EXISTS `wp_wpc_client_chains` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_wpc_client_chains`
--

INSERT INTO `wp_wpc_client_chains` (`id`, `subject`) VALUES
(1, 'Test Support Issue');
