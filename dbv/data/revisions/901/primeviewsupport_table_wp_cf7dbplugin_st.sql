
-- --------------------------------------------------------

--
-- Table structure for table `wp_cf7dbplugin_st`
--

DROP TABLE IF EXISTS `wp_cf7dbplugin_st`;
CREATE TABLE IF NOT EXISTS `wp_cf7dbplugin_st` (
  `submit_time` decimal(16,4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
