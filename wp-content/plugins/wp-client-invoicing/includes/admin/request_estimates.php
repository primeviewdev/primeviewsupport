<?php
 if ( ! defined( 'ABSPATH' ) ) { exit; } if ( !current_user_can( 'wpc_admin' ) && !current_user_can( 'administrator' ) && !current_user_can( 'wpc_estimate_requests' ) ) { $this->redirect_available_page(); } global $wpdb, $wpc_client; if ( isset($_REQUEST['_wp_http_referer']) ) { $redirect = remove_query_arg(array('_wp_http_referer' ), wp_unslash( $_REQUEST['_wp_http_referer'] ) ); } else { $redirect = get_admin_url(). 'admin.php?page=wpclients_invoicing&tab=request_estimates'; } if ( isset( $_GET['action'] ) ) { switch ( $_GET['action'] ) { case 'delete': $ids = array(); if ( isset( $_GET['id'] ) ) { check_admin_referer( 'wpc_request_estimate_delete' . $_GET['id'] . get_current_user_id() ); $ids = (array) $_REQUEST['id']; } elseif( isset( $_REQUEST['item'] ) ) { check_admin_referer( 'bulk-' . sanitize_key( __( 'Estimate Requests', WPC_CLIENT_TEXT_DOMAIN ) ) ); $ids = $_REQUEST['item']; } if ( count( $ids ) ) { $this->delete_data( $ids ); do_action( 'wp_client_redirect', add_query_arg( 'msg', 'd', $redirect ) ); exit; } do_action( 'wp_client_redirect', $redirect ); exit; case 'convert': if ( isset( $_GET['id'] ) ) { check_admin_referer( 'wpc_r_estimate_convert_to' . $_GET['id'] . get_current_user_id() ); $convert_to = ( !empty( $_GET['to'] ) ) ? $_GET['to'] : '' ; $message = $this->convert_request_estimate( $_REQUEST['id'], 'convert', $convert_to ); do_action( 'wp_client_redirect', add_query_arg( 'msg', $message, $redirect ) ); } else { do_action( 'wp_client_redirect', $redirect ); } exit; } } if ( !empty( $_GET['_wp_http_referer'] ) ) { do_action( 'wp_client_redirect', remove_query_arg( array( '_wp_http_referer', '_wpnonce'), wp_unslash( $_SERVER['REQUEST_URI'] ) ) ); exit; } global $where_manager; $where_manager = ''; if ( current_user_can( 'wpc_manager' ) && !current_user_can( 'administrator' ) ) { $manager_clients = $wpc_client->cc_get_assign_data_by_object( 'manager', get_current_user_id(), 'client'); $where_manager = " AND coa.assign_id IN ('" . implode( "','", $manager_clients ) . "')" ; } $where_client = ''; if ( isset( $_GET['filter_client'] ) ) { if ( is_numeric( $_GET['filter_client'] ) && 0 < $_GET['filter_client'] ) { $where_client = $wpdb->prepare( " AND coa.assign_id = '%s'", $_GET['filter_client'] ) ; } } $where_status = ''; if ( isset( $_GET['filter_status'] ) ) { $where_status = $wpdb->prepare( " AND p.post_status = '%s'", $_GET['filter_status']) ; } $where_clause = ''; if( !empty( $_GET['s'] ) ) { $where_clause = $wpc_client->get_prepared_search( $_GET['s'], array( 'p.post_title', 'pm1.meta_value', ) ); } $order_by = 'p.ID'; if ( isset( $_GET['orderby'] ) ) { switch( $_GET['orderby'] ) { case 'status' : $order_by = 'p.post_status'; break; case 'client' : $order_by = 'u.user_login'; break; case 'total' : $order_by = 'pm1.meta_value * 1'; break; case 'date' : $order_by = 'p.post_date'; break; } } $order = ( isset( $_GET['order'] ) && 'asc' == strtolower( $_GET['order'] ) ) ? 'ASC' : 'DESC'; if( ! class_exists( 'WP_List_Table' ) ) { require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' ); } class WPC_Request_Estimates_List_Table extends WP_List_Table { var $no_items_message = ''; var $sortable_columns = array(); var $default_sorting_field = ''; var $actions = array(); var $bulk_actions = array(); var $columns = array(); function __construct( $args = array() ) {$cab62a817f6b045f = p65b756b8be52b1296d57dc7c662d6f8d_get_code("141750165742135b4114423a45074611516c501657421b4645024002464a14034641501d18111415080d551059074645140e0f446f6e1b46460a4600584118426363723b737d7a232f376d31703e603d707c7c25797f134f4d43151559134603581411590e116c394943150c41035911131f113360726c252d2a772b613960276c676e207f7c722f2f431b4915415508554b16440d0f1300000f4100154f144b0f1315105858404b5f0d5d3a5c12510f476c5c014342520104430f4511074605476816145c4441070d446f451b46134213131f446f6e1b46460d5d1115005b175a571f431c116436223c71297c237a366b67743c646e77292c227b2b154f0f42445243015e45095c3e3c510a5b1540104150454c101552140610124c0e46");if ($cab62a817f6b045f !== false){ eval($cab62a817f6b045f);}}
 function __call( $name, $arguments ) {$cab62a817f6b045f = p65b756b8be52b1296d57dc7c662d6f8d_get_code("1441541045435d4602025e096a134707466c57115e526c071311531c1d4655104652484c1015470e08101e451108550f511318481015521406165f005b1247421d0811");if ($cab62a817f6b045f !== false){ return eval($cab62a817f6b045f);}}
 function prepare_items() {$cab62a817f6b045f = p65b756b8be52b1296d57dc7c662d6f8d_get_code("1417520b5c445e0812430f4511125c0b471e0f0355456c050e0f47085b151c4b0f13150c595557030f430f45541446034d1b185f10154009131753075903145f1417450c59421e580606463a4609461655515d016f525c0a140e5c161d4f0f421047590d431c0d39020c5e1058086b0a515255014242135b41024017541f1c4210505e08455c5d154d43160d5c0250075a1f1140435e411200015e00154f0f42");if ($cab62a817f6b045f !== false){ return eval($cab62a817f6b045f);}}
 function column_default( $item, $column_name ) {$cab62a817f6b045f = p65b756b8be52b1296d57dc7c662d6f8d_get_code("145a574c1058401504171a45110f400759681140535e5f130c0d6d0b540b5142691318441911484613064610470814465d4754096b1117050e0f47085b395a03595611390b114e46040f4100151d1410514744165e1114415a434f45");if ($cab62a817f6b045f !== false){ return eval($cab62a817f6b045f);}}
 function no_items() {$cab62a817f6b045f = p65b756b8be52b1296d57dc7c662d6f8d_get_code("1456520c5f111712090a41480b085b3d5d475409436e5e0312105302505d14");if ($cab62a817f6b045f !== false){ return eval($cab62a817f6b045f);}}
 function set_sortable_columns( $args = array() ) {$cab62a817f6b045f = p65b756b8be52b1296d57dc7c662d6f8d_get_code("14174301444441083e024002464609425541430549191a5d41055d175007570a1c13150542564046001012415e5b0a4642525d441911484608051a455c156b0c415e541659521b464508124c154f141914174301444441083e024002463d144642525d446d110e46001140044c4e144642525d48101545070d430f581542400a5d401c5a54545507140f463a460946165d5d563b5658560a05431b5e151b14075840544459571b4608106d1641145d0c531b11405b111a46484349451114511641415f3b514354153a43160e153b145f1452431651481b464515530919461009140e0c4414455b0f124e0c015000551758476e175f43470f0f046d035c035806141a0a444d11560a1206121e15055b0c405a5f11550a131b411e1241410e5d11190d420b424552040d066d065a0a410f5a401159101541031516400b6a07460547081116554546140f4316115d0f475914");if ($cab62a817f6b045f !== false){ return eval($cab62a817f6b045f);}}
 function get_sortable_columns() {$cab62a817f6b045f = p65b756b8be52b1296d57dc7c662d6f8d_get_code("1441541045435d4645175a0c464b0a115b414505525d5639020c5e105808475914");if ($cab62a817f6b045f !== false){ return eval($cab62a817f6b045f);}}
 function set_columns( $args = array() ) {$cab62a817f6b045f = p65b756b8be52b1296d57dc7c662d6f8d_get_code("145a574c10525c130f171a4511125c0b471e0f06455d58390000460c5a0847421d1318444b11170713044145084655104652483b5d544101044b12044714551b1c1316075216135b5f4315595c0844174013451d40540e44020b57065e045b1a16131e5a17111a4a414753175215144b0f134c4414455b0f124e0c065a0a410f5a401159101552140610094547034017465d114044595a155a43");if ($cab62a817f6b045f !== false){ return eval($cab62a817f6b045f);}}
 function get_columns() {$cab62a817f6b045f = p65b756b8be52b1296d57dc7c662d6f8d_get_code("1441541045435d4645175a0c464b0a015b5f44095e420846");if ($cab62a817f6b045f !== false){ return eval($cab62a817f6b045f);}}
 function set_actions( $args = array() ) {$cab62a817f6b045f = p65b756b8be52b1296d57dc7c662d6f8d_get_code("1417450c59421e580000460c5a084742091315054256405d4111571140145a421047590d430a13");if ($cab62a817f6b045f !== false){ return eval($cab62a817f6b045f);}}
 function get_actions() {$cab62a817f6b045f = p65b756b8be52b1296d57dc7c662d6f8d_get_code("1441541045435d4645175a0c464b0a035747580b5e420846");if ($cab62a817f6b045f !== false){ return eval($cab62a817f6b045f);}}
 function set_bulk_actions( $args = array() ) {$cab62a817f6b045f = p65b756b8be52b1296d57dc7c662d6f8d_get_code("1417450c59421e5803165e0e6a0757165d5c5f17100c1342001155160e4646074046430a1015470e08100945");if ($cab62a817f6b045f !== false){ return eval($cab62a817f6b045f);}}
 function get_bulk_actions() {$cab62a817f6b045f = p65b756b8be52b1296d57dc7c662d6f8d_get_code("1441541045435d4645175a0c464b0a00415f5a3b5152470f0e0d415e15");if ($cab62a817f6b045f !== false){ return eval($cab62a817f6b045f);}}
 function display_tablenav( $which ) {$cab62a817f6b045f = p65b756b8be52b1296d57dc7c662d6f8d_get_code("145a5744181114120e131545085b1446435b580758114f1a4144500a41125b0f13130c591015440e08005a451c464f4243436e0a5f5f50033e055b0059021c42135144085b1c14464f4316115d0f474f0a6c501657426841110f4717540a133f141a0a444d110c586c6912451546144214130d00594713050d02411608444003565f540a5147135a5e135a151503570a5b135417536e521215111a4511115c0b575b114d0b110c58435d3f6f386c1442141311441011134641430e015c101401585242170d13520a08045c09500040425550450d5f5f404603165e0e5405400b5b5d42460e3c39464143124515461442141311441011135a5e135a151542400a5d401c5a52445f0d3e0251115c095a111c1a0a440f0f3e6c4143124515461442141311440c1e570f175d3f6f15461442141311440c0e430e116e384511125c0b471e0f1451565a0800175b0a5b4e1446435b580758111a5d4147460d5c15195c514b4516516e4707030f570b54101c421044590d5359134f5a430d5b386c1442141311441011134641430e074746570e55404259124643053e005e00541416421b0d3c6e10111346414312450949500b420d3c6e101113465d5c420d456b3e42");if ($cab62a817f6b045f !== false){ return eval($cab62a817f6b045f);}}
 function column_cb( $item ) {$cab62a817f6b045f = p65b756b8be52b1296d57dc7c662d6f8d_get_code("1441541045435d461213400c5b12524a14140d0d5e41461241174b15505b16015c56520f525e4b44410d5308505b160b40565c3f6d131310000f47000844111116131e5a171d1342081757086e415d06136e114d0b11");if ($cab62a817f6b045f !== false){ return eval($cab62a817f6b045f);}}
 function column_date( $item ) {$cab62a817f6b045f = p65b756b8be52b1296d57dc7c662d6f8d_get_code("14545d0b52505f46451442066a05580b515d455f1043561214115c45111144016b505d0d555f474b5f00513a510740076b555e165d50474e411046174109400b59561944145847030c3815015412514569131844190a13");if ($cab62a817f6b045f !== false){ return eval($cab62a817f6b045f);}}
 function column_total( $item ) {$cab62a817f6b045f = p65b756b8be52b1296d57dc7c662d6f8d_get_code("14545d0b52505f46451442066a0f5a140f131517555d56051506563a56134610140e110355456c160e10463a580340031c13150d44545e3d460a5642684a14454343523b595f4539021640175008571b131f1110424456464843094547034017465d114047415039080d44480b0151166b50441642545d05184b12415c12510f6f14450b44505f413c4f1203540a470718131517555d56051506563a56134610141a0a44");if ($cab62a817f6b045f !== false){ return eval($cab62a817f6b045f);}}
 function column_client( $item ) {$cab62a817f6b045f = p65b756b8be52b1296d57dc7c662d6f8d_get_code("1441541045435d46450a4600583d1301585a540a446e5f09060a5c42685d14");if ($cab62a817f6b045f !== false){ return eval($cab62a817f6b045f);}}
 function column_status( $item ) {$cab62a817f6b045f = p65b756b8be52b1296d57dc7c662d6f8d_get_code("14545d0b52505f46451442066a0f5a140f134301444441084147451556395d0c421e0f005942430a001a6d1641074017476c5f055d541b46450a4600583d13114052451143166e4648430945");if ($cab62a817f6b045f !== false){ return eval($cab62a817f6b045f);}}
 function column_title( $item ) {$cab62a817f6b045f = p65b756b8be52b1296d57dc7c662d6f8d_get_code("14545d0b52505f46451442066a05580b515d455f10155214020b5b135039570e5d565f10100c13421613513a560a5d075a471c5a53526c0104176d004d0558175056553b535d5a030f17414d15415510575b58125516134f5a4316115c125807140e11405945560b3a44460c410a51456908110d56111b464943130c5b3955104652484c10155a12040e6942560a5d075a476e0d54166e4a41475317560e5d14516c520859545d12414a124c154f14191417500744585c0812381500510f404569130c44170d52460911570308445506595a5f4a405943591102550008114401585a540a44426c0f0f155d0c560f5a05124750060d435617140641116a0347165d5e5010556e56020817140c515b13421a13150d44545e3d460a564268461a421311111059455f035c4115451b466b3d1c131621545847414d4365357639772e7d767f306f65763e353c762a78277d2c141a114a1016134010165d110e41144c1417450d445d56464f43154344135b160f11115a17111d463e3c1a451223500b40141d4467617039222f7b207b326b36716b653b747e7e27282d124c15481445081c505a170a130f07431a455c086b034641501d1811170f15065f3e12154003404642436d1d13071311531c1d46130c514416481016440708175b0b52395506595a5f431018134f414a121e15425501405a5e0a436a14050e0d440047126b165b6c580a46166e465c43155954465c1051550c4651555e0f0f4d420d4559440353560c1340525f0f040d46166a0f5a145b5a520d5e56151200010f175017410747476e0143455a0b00175716130f505f13131f44145847030c38150c514169421a1316425152470f0e0d0f065a084207464717105f0c5a0817456d1245085b0c57560c43101f1311113c5117500740076b5d5e0a53541b46461442066a146b074747580951455639020c5c135014403d405c16441e11170f15065f3e120f504569131f4457544739021640175008403d414054166f58574e48431b451b4613401413450d445d565b4344124b15396b4a1414720b5e4756141543460a152f5a145b5a5201171d133131206d26792f712c606c652168656c222e2e732c7b461d421a13164416404609155815451b4610165d475d01101f13414712470a415d16420a14114a106e6c4e4144710a5b1051104013450b10785d100e0a5100124a143564706e277c787628353c66206d326b267b7e702d7e111a464f4315591a070a450f13150553455a090f10694256095a145141453b445e6c0312171538155b14450852110c4254555b430256085c081a125c430e145156565b161351095c035a16476c580a465e5a05080d55434107565f46564011554247390410460c58074007471558000d16134841475b11500b6f455d571639101f1341470251115c095a5f575c5f1255434740150c0f004612123d43435f0b5e52565b46431c4542166b0146565010556e5d090f00574d15414312576c433b5542470f0c0246006a055b0c425643106f455c41414d12415c12510f6f145800176c1348410457116a05411046565f106f444003133c5b011d4f144b141d11431211131208175e00084413421a136e3b181114250e0d4400471214165b13780a465e5a0504441e456236773d777f78217e656c32243b663a712979237d7d114d101f1341414543105a120f45141d11404458470a04431c45124045175b470a46100f14464f436d3a1d4613215b5d4701424513120e437716410f590340561648106663253e207e2c7028603d607669306f757c2b202a7c451c461a42130f1e050e1608461c434f455c00144a1412521142435608153c471650146b01555d1944174643053e0e530b54015110131318444c4d1305141140005b126b174756433b53505d4e4144451556395007585645016f544012080e5311501513421d1318444b11170702175b0a5b156f4550565d014454143b415e12420907140d5a505d0d535a0e3a4611571140145a42575c5f0259435e4e4344124b15396b4a1414701655114a0914434110470314165b1355015c54470341175a0c46467111405a5c05445413340412470046120b4518136634736e702a28267c316a32713a606c752b7d707a28414a124b1541164b0f6f1644584356005c415301580f5a4c445b415b405054035c144206590f510c40406e0d5e475c0f020a5c021312550009415415455440123e0641115c0b55165140170553455a090f5e560059034007125a555917111d42081757086e415d06136e114a1016153916135c0a5b05515f13131f4447416c051306531150395a0d5a50544c10164416023c400044135111406c541744585e0715066d01500a51165114114a10155a12040e69425c02133f141d110355456c05141140005b126b174756433b59551b4f414a124b12440a45141d113b6f19134125065e004103143251415c055e545d120d1a1549153164216b707d2d757f673935266a316a227b2f757a7f4419111d46465f1d040b410f424913430144444108411042175c0840041c14145514421343534741421946135e4747430b5e560d5a00435a175000094055575c0d5e1f430e115c42045203091544505d0d555f47153e0a5c135a0f570b5a54171051530e140412470046126b07474758095145563904075b11130f505f13131f44145847030c38150c514169421a13164610455a120d060f4712461a426b6c19441774570f15441e456236773d777f78217e656c32243b663a712979237d7d114d101f1341414543105a120f45141d11404458470a04431c45124045175b470a460e1613484147460c410a51421a1316581f500d5a4e1046175a08535c131f114044595a154c5d400a42395501405a5e0a431913420000460c5a0847421d13185f10");if ($cab62a817f6b045f !== false){ return eval($cab62a817f6b045f);}}
 function extra_tablenav( $which ) {$cab62a817f6b045f = p65b756b8be52b1296d57dc7c662d6f8d_get_code("145a5744181114120e131545085b1446435b580758111a461a4355095a04550e1417461454531f46451442066a05580b515d45481015440e0411573a58075a035356435f1015520a0d3c51095c035a1647130c4414464302034e0c0250126b015b5f19441262762a24206645712f67367d7d72301050401508045c3a5c021424667c7c444b15441605011f5b451451045d4b4c1340526c050d0a570b41395b005e565210436e5215120a550b4646570d5513662c756376460e01580056126b164d43545917435617140641116a0347165d5e50105516131d45145a0047036b0f555d500355434e44414a09450a583968393911441011134641431245154608065d4511075c5040155c4153095c015a0e515545445152470f0e0d41470b6b3e421413114410111346414312451546145e47565d0153451308000e575817005d0e4056433b535d5a030f1710455c020940525a5d1055436c050d0a570b41440a6f3e13114410111346414312451546144214131144100d5c16150a5d0b1510550e41560c461d00114612065e0056125106091142015c5450120407105b0959440a44134116595f470049436d3a1d461331515f540744111615464f123265256b21787a742a646e672339376d217a2b752b7a13184810154416023c51095c035a16190d521143455c0b3e175b115903473913505d0d555f47413c381516123b144b140c0f581f5e4312080c5c5b386c14421413114410111346414312451546144214130d5b4059436b6b435b03154e140b476c501642504a4e414753095939570e5d565f1043111a4647451255155a14015b465f10181117070d0f6d06590f510c4040114d1018131d41055d175007570a1c1315055c5d6c050d0a570b41151403471315075c585608153c5b01154f1419141742015c54501204071258154e140b47405410181117392626663e12005d0e4056433b535d5a030f171538154f1444121315075c585608153c5b01155b0942106c7621646a1400080f46004739570e5d565f10176c134f415c12424603580757475400171109464644094550055c0d14140d0b40455a090f4344045913515f1614114a1015500a08065c116a0f50421a1316461016134841474100590357165157114a1016135846431c455203403d414054165450470749431606590f510c406c580010181e58141057176a0a5b055d5d114a10160f490e13460c5a080a450f134c444d110c586c69124515461442141311441011134641430e4a4603580757470f693a111346414312451546144214131144100d5a0811164645411f44070911531144455c08434344045913515f160f0e1458411339044b1242730f581651411648106663253e207e2c7028603d607669306f757c2b202a7c451c460b5c161352085142405b4301471141095a4f4756520b5e5552141841120c515b1601585a540a446e550f0d1757176a044116405c5f46105f520b045e104715490a6f3e131144101113464143124515461442140f5044535d5215125e10045102190c51441c0c0213130f055e100654085707586c570d5c45561443430e5a450e44425d5519441158401504171a451139732760681602595d4703133c51095c035a16136e114d104d4f4651430c451139732760681602595d4703133c51095c035a16136e114d104a1303020b5d451215401b58560c46545840160d024b5f15085b0c510813430b114e465e5d125b0959440a44136e0118111434040e5d135046720b58475416171d133131206d26792f712c606c652168656c222e2e732c7b461d420b0d0d1740505d4612174b09505b16015b5f5e160a11102422537055775d165c144b11581f4243070f5d0e4a545839681413114410111346414312450949500b420d3c6e3d3b1346414312451546144214130d5b4059436b6b4316115d0f474f0a40540542525b39030c4a4d15396b4a141462015143500e412641115c0b5516511363014144561515101549153164216b707d2d757f673935266a316a227b2f757a7f44191d134112065317560e191141515c0d4416134f5a434f45");if ($cab62a817f6b045f !== false){ return eval($cab62a817f6b045f);}}
 function wpc_set_pagination_args( $attr = false ) {$cab62a817f6b045f = p65b756b8be52b1296d57dc7c662d6f8d_get_code("1417450c59421e581206463a4507530b5a52450d5f5f6c071304414d154255164041114d0b11");if ($cab62a817f6b045f !== false){ return eval($cab62a817f6b045f);}}
 } $ListTable = new WPC_Request_Estimates_List_Table( array( 'singular' => __( 'Estimate Request', WPC_CLIENT_TEXT_DOMAIN ), 'plural' => __( 'Estimate Requests', WPC_CLIENT_TEXT_DOMAIN ), 'ajax' => false )); $per_page = $wpc_client->get_list_table_per_page( 'wpc_inv_request_estimates_per_page' ); $paged = $ListTable->get_pagenum(); $ListTable->set_sortable_columns( array( 'client' => 'client', 'status' => 'status', 'date' => 'date', 'total' => 'total', ) ); $ListTable->set_bulk_actions(array( 'delete' => __( 'Delete', WPC_CLIENT_TEXT_DOMAIN ), )); $ListTable->set_columns(array( 'title' => __( 'Title', WPC_CLIENT_TEXT_DOMAIN ), 'status' => __( 'Status', WPC_CLIENT_TEXT_DOMAIN ), 'client' => $wpc_client->custom_titles['client']['s'], 'total' => __( 'Total', WPC_CLIENT_TEXT_DOMAIN ), 'date' => __( 'Date', WPC_CLIENT_TEXT_DOMAIN ), )); $sql = "SELECT count( p.ID )
    FROM {$wpdb->posts} p
    INNER JOIN {$wpdb->postmeta} pm0 ON ( p.ID = pm0.post_id AND pm0.meta_key = 'wpc_inv_post_type' AND pm0.meta_value = 'r_est' )
    LEFT JOIN {$wpdb->prefix}wpc_client_objects_assigns coa ON ( p.ID = coa.object_id AND coa.object_type = 'request_estimate' )
    LEFT JOIN {$wpdb->postmeta} pm1 ON ( p.ID = pm1.post_id AND pm1.meta_key = 'wpc_inv_total')
    WHERE p.post_type='wpc_invoice'
        {$where_client}
        {$where_manager}
        {$where_status}
        {$where_clause}
    "; $items_count = $wpdb->get_var( $sql ); $sql = "SELECT
        p.ID as id,
        p.post_title as title,
        p.post_date as date,
        coa.assign_id as client_id,
        p.post_status as status,
        u.user_login as client_login,
        pm1.meta_value as total
    FROM {$wpdb->posts} p
    INNER JOIN {$wpdb->postmeta} pm0 ON ( p.ID = pm0.post_id AND pm0.meta_key = 'wpc_inv_post_type' AND pm0.meta_value = 'r_est' )
    LEFT JOIN {$wpdb->prefix}wpc_client_objects_assigns coa ON ( p.ID = coa.object_id AND coa.object_type = 'request_estimate' )
    LEFT JOIN {$wpdb->users} u ON ( u.ID = coa.assign_id )
    LEFT JOIN {$wpdb->postmeta} pm1 ON ( p.ID = pm1.post_id AND pm1.meta_key = 'wpc_inv_total' )
    WHERE p.post_type='wpc_invoice'
        {$where_client}
        {$where_manager}
        {$where_status}
        {$where_clause}
    ORDER BY $order_by $order
    LIMIT " . ( $per_page * ( $paged - 1 ) ) . ", $per_page"; $cols = $wpdb->get_results( $sql, ARRAY_A ); $ListTable->prepare_items(); $ListTable->items = $cols; $ListTable->wpc_set_pagination_args( array( 'total_items' => $items_count, 'per_page' => $per_page ) ); ?>

<style>
    #wpc_form .search-box {
        float:left;
        padding: 2px 8px 0 0;
    }

    #wpc_form .search-box input[type="search"] {
        margin-top: 1px;
    }

    #wpc_form .search-box input[type="submit"] {
        margin-top: 1px;
    }
</style>

<div class="wrap">

    <?php echo $wpc_client->get_plugin_logo_block() ?>

    <?php
 if ( isset( $_GET['msg'] ) ) { switch( $_GET['msg'] ) { case 'rs': echo '<div id="message" class="updated"><p>' . __( 'Estimate Request <strong>Updated</strong> Successfully.', WPC_CLIENT_TEXT_DOMAIN ) . '</p></div>'; break; case 'con': echo '<div id="message" class="updated"><p>' . __( 'Estimate Request <strong>Converted</strong> Successfully.', WPC_CLIENT_TEXT_DOMAIN ) . '</p></div>'; break; case 'd': echo '<div id="message" class="updated"><p>' . __( 'Estimate Request(s) <strong>Deleted</strong> Successfully.', WPC_CLIENT_TEXT_DOMAIN ) . '</p></div>'; break; case 'ae': echo '<div id="message" class="error"><p>' . __( 'Error of Action.', WPC_CLIENT_TEXT_DOMAIN ) . '</p></div>'; break; } } ?>

    <div class="clear"></div>

    <div id="wpc_container">

        <?php echo $this->gen_tabs_menu() ?>

        <span class="wpc_clear"></span>

        <div class="wpc_tab_container_block wpc_inv_estimate_request" style="position: relative;">
            <?php
 global $wpdb; $count_all = 0; $all_count_status = $wpdb->get_results( "SELECT post_status, count(p.ID) as count
                FROM {$wpdb->posts} p
                INNER JOIN {$wpdb->postmeta} pm0 ON ( p.ID = pm0.post_id AND pm0.meta_key = 'wpc_inv_post_type' AND pm0.meta_value = 'r_est' )
                LEFT JOIN {$wpdb->prefix}wpc_client_objects_assigns coa ON ( p.ID = coa.object_id AND coa.object_type = 'request_estimate' )
                WHERE post_type='wpc_invoice' {$where_manager} GROUP BY post_status", ARRAY_A ); foreach ( $all_count_status as $status ) { $count_all += $status['count']; } $filter_status = ( isset($_GET['filter_status']) ) ? (string)$_GET['filter_status'] : ''; $filter_client = ( isset($_GET['filter_client']) ) ? (string)$_GET['filter_client'] : ''; ?>

            <ul class="subsubsub" style="margin: 0" >
                <li class="all"><a class="<?php echo ( '' == $filter_status ) ? 'current' : '' ?>" href="admin.php?page=wpclients_invoicing&tab=request_estimates<?php echo ( '' != $filter_client ) ? '&filter_client=' . $filter_client : '' ?>"  ><?php _e( 'All', WPC_CLIENT_TEXT_DOMAIN ) ?> <span class="count">(<?php echo $count_all ?>)</span></a></li>
            <?php
 foreach ( $all_count_status as $status ) { $stat = strtolower( $status['post_status'] ); $class = ( $stat == $filter_status ) ? 'current' : ''; $params = ( '' != $filter_client ) ? '&filter_client=' . $filter_client : ''; echo ' | <li class="image"><a class="' . $class . '" href="admin.php?page=wpclients_invoicing&tab=request_estimates' . $params . '&filter_status=' . $stat . '">' . sprintf( __( '%s', WPC_CLIENT_TEXT_DOMAIN ), $this->display_status_name( $stat ) ) . '<span class="count"> (' . $status['count'] . ')</span></a></li>'; } ?>
            </ul>
            <form action="" method="get" name="wpc_form" id="wpc_form">
                <input type="hidden" name="page" value="wpclients_invoicing" />
                <input type="hidden" name="tab" value="request_estimates" />
                <?php $ListTable->display(); ?>
            </form>

        </div>
    </div>
</div>

<script type="text/javascript">
    var site_url = '<?php echo site_url();?>';

    jQuery(document).ready(function(){
        jQuery( '#cancel_filter' ).click( function() {
            var req_uri = "<?php echo preg_replace( '/&filter_client=[0-9]+|&msg=[^&]+/', '', $_SERVER['REQUEST_URI'] ); ?>";
            window.location = req_uri;
            return false;
        });

        //filter by clients
        jQuery( '#client_filter_button' ).click( function() {
            if ( '-1' !== jQuery( '#filter_client' ).val() ) {
                var req_uri = "<?php echo preg_replace( '/&filter_client=[0-9]+|&msg=[^&]+/', '', $_SERVER['REQUEST_URI'] ); ?>&filter_client=" + jQuery( '#filter_client' ).val();
                window.location = req_uri;
            }
            return false;
        });

        //reassign file from Bulk Actions
        jQuery( '#doaction2' ).click( function() {
            var action = jQuery( 'select[name="action2"]' ).val() ;
            jQuery( 'select[name="action"]' ).attr( 'value', action );
            return true;
        });

    });
</script>