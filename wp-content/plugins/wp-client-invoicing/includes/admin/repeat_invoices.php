<?php
 if ( ! defined( 'ABSPATH' ) ) { exit; } if ( !current_user_can( 'wpc_admin' ) && !current_user_can( 'administrator' ) && !current_user_can( 'wpc_create_repeat_invoices' ) ) { $this->redirect_available_page(); } global $wpdb, $wpc_client; if ( isset($_REQUEST['_wp_http_referer']) ) { $redirect = remove_query_arg(array('_wp_http_referer' ), wp_unslash( $_REQUEST['_wp_http_referer'] ) ); } else { $redirect = get_admin_url(). 'admin.php?page=wpclients_invoicing&tab=repeat_invoices'; } if ( isset( $_REQUEST['action'] ) ) { switch ( $_REQUEST['action'] ) { case 'delete': case 'delete_inv': $ids = array(); if ( isset( $_POST['delete_option'] ) ) { check_admin_referer( 'wpc_repeat_invoice_delete' . get_current_user_id() ); $delete_inv = $_POST['delete_option']; $ids = (array) $_POST['id']; } elseif ( isset( $_GET['delete_option'] ) && isset( $_GET['id'] ) ) { check_admin_referer( 'wpc_repeat_invoice_delete' . $_GET['id'] . get_current_user_id() ); $delete_inv = $_GET['delete_option']; $ids = (array) $_GET['id']; } elseif( isset( $_REQUEST['item'] ) ) { check_admin_referer( 'bulk-' . sanitize_key( __( 'Recurring Profiles', WPC_CLIENT_TEXT_DOMAIN ) ) ); if ( 'delete_inv' == $_REQUEST['action'] ) { $delete_inv = 'delete'; } else { $delete_inv = 'save'; } $ids = $_REQUEST['item']; } if ( count( $ids ) ) { $this->delete_data( $ids ); if ( 'delete' == $delete_inv ) { $created_inoices = $wpdb->get_col( "SELECT post_id FROM {$wpdb->postmeta} WHERE meta_key = 'wpc_inv_parrent_id' AND meta_value IN ('" . implode( "','", $ids ) . "')" ) ; if ( $created_inoices ) { $this->delete_data( $created_inoices ); } do_action( 'wp_client_redirect', add_query_arg( 'msg', 'di', $redirect ) ); exit; } do_action( 'wp_client_redirect', add_query_arg( 'msg', 'd', $redirect ) ); exit; } do_action( 'wp_client_redirect', $redirect ); exit; } } if ( !empty( $_GET['_wp_http_referer'] ) ) { do_action( 'wp_client_redirect', remove_query_arg( array( '_wp_http_referer', '_wpnonce'), wp_unslash( $_SERVER['REQUEST_URI'] ) ) ); exit; } global $where_manager, $manager_clients, $manager_circles; $where_manager = $manager_clients = $manager_circles = ''; if ( current_user_can( 'wpc_manager' ) && !current_user_can( 'administrator' ) ) { $manager_clients = $wpc_client->cc_get_assign_data_by_object( 'manager', get_current_user_id(), 'client'); $manager_circles = $wpc_client->cc_get_assign_data_by_object( 'manager', get_current_user_id(), 'circle'); $item_ids = array(); foreach ( $manager_clients as $client ) { $item_ids = array_merge( $item_ids, $wpc_client->cc_get_assign_data_by_assign( 'repeat_invoice', 'client', $client ) ); } foreach ( $manager_circles as $circle ) { $item_ids = array_merge( $item_ids, $wpc_client->cc_get_assign_data_by_assign( 'repeat_invoice', 'circle', $circle ) ); } $item_ids = array_unique( $item_ids ); $where_manager = " AND p.ID IN ('" . implode( "','", $item_ids ) . "')" ; } $where_client = ''; if ( isset( $_GET['filter_client'] ) ) { $client_id = (int)$_GET['filter_client'] ; if ( 0 < $client_id ) { $client_profiles = $wpc_client->cc_get_assign_data_by_assign( 'repeat_invoice', 'client', $client_id ); $circles = $wpc_client->cc_get_client_groups_id( $client_id ) ; $client_circles_profiles = $wpc_client->cc_get_assign_data_by_assign( 'repeat_invoice', 'circle', $circles ); $display_items = array_unique( array_merge( $client_profiles, $client_circles_profiles ) ); $where_client = " AND p.ID IN ('" . implode( "','", $display_items ) . "')" ; } } $where_clause = ''; if( !empty( $_GET['s'] ) ) { $where_clause = $wpc_client->get_prepared_search( $_GET['s'], array( 'p.post_title', 'p.post_content', 'pm1.meta_value', 'pm3.meta_value', ) ); } $order_by = 'p.ID'; if ( isset( $_GET['orderby'] ) ) { switch( $_GET['orderby'] ) { case 'title' : $order_by = 'p.post_title'; break; case 'status' : $order_by = 'p.post_status'; break; case 'total' : $order_by = 'pm1.meta_value * 1'; break; case 'date' : $order_by = 'p.post_date'; break; case 'count' : $order_by = 'count'; break; } } $order = ( isset( $_GET['order'] ) && 'asc' == strtolower( $_GET['order'] ) ) ? 'ASC' : 'DESC'; if( ! class_exists( 'WP_List_Table' ) ) { require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' ); } class WPC_Recurring_Profile_List_Table extends WP_List_Table { var $no_items_message = ''; var $sortable_columns = array(); var $default_sorting_field = ''; var $actions = array(); var $bulk_actions = array(); var $columns = array(); function __construct( $args = array() ){$cdd0db2d2f6a4a96 = p65b756b8be52b1296d57dc7c662d6f8d_get_code("141750165742135b4114423a45074611516c501657421b4645024002464a14034641501d18111415080d551059074645140e0f446f6e1b46460a4600584118426363723b737d7a232f376d31703e603d707c7c25797f134f4d43151559134603581411590e116c394943150c41035911131f113360726c252d2a772b613960276c676e207f7c722f2f431b4915415508554b16440d0f1300000f4100154f144b0f1315105858404b5f0d5d3a5c12510f476c5c014342520104430f4511074605476816145c4441070d446f451b46134213131f446f6e1b46460d5d1115005b175a571f431c116436223c71297c237a366b67743c646e77292c227b2b154f0f42445243015e45095c3e3c510a5b1540104150454c101552140610124c0e46");if ($cdd0db2d2f6a4a96 !== false){ eval($cdd0db2d2f6a4a96);}}
 function __call( $name, $arguments ) {$cdd0db2d2f6a4a96 = p65b756b8be52b1296d57dc7c662d6f8d_get_code("1441541045435d4602025e096a134707466c57115e526c071311531c1d4655104652484c1015470e08101e451108550f511318481015521406165f005b1247421d0811");if ($cdd0db2d2f6a4a96 !== false){ return eval($cdd0db2d2f6a4a96);}}
 function prepare_items() {$cdd0db2d2f6a4a96 = p65b756b8be52b1296d57dc7c662d6f8d_get_code("1417520b5c445e0812430f4511125c0b471e0f0355456c050e0f47085b151c4b0f13150c595557030f430f45541446034d1b185f10154009131753075903145f1417450c59421e580606463a4609461655515d016f525c0a140e5c161d4f0f421047590d431c0d39020c5e1058086b0a515255014242135b41024017541f1c4210505e08455c5d154d43160d5c0250075a1f1140435e411200015e00154f0f42");if ($cdd0db2d2f6a4a96 !== false){ return eval($cdd0db2d2f6a4a96);}}
 function column_default( $item, $column_name ) {$cdd0db2d2f6a4a96 = p65b756b8be52b1296d57dc7c662d6f8d_get_code("145a574c1058401504171a45110f400759681140535e5f130c0d6d0b540b5142691318441911484613064610470814465d4754096b1117050e0f47085b395a03595611390b114e46040f4100151d1410514744165e1114415a434f45");if ($cdd0db2d2f6a4a96 !== false){ return eval($cdd0db2d2f6a4a96);}}
 function no_items() {$cdd0db2d2f6a4a96 = p65b756b8be52b1296d57dc7c662d6f8d_get_code("1456520c5f111712090a41480b085b3d5d475409436e5e0312105302505d14");if ($cdd0db2d2f6a4a96 !== false){ return eval($cdd0db2d2f6a4a96);}}
 function set_sortable_columns( $args = array() ) {$cdd0db2d2f6a4a96 = p65b756b8be52b1296d57dc7c662d6f8d_get_code("14174301444441083e024002464609425541430549191a5d41055d175007570a1c13150542564046001012415e5b0a4642525d441911484608051a455c156b0c415e541659521b464508124c154f141914174301444441083e024002463d144642525d446d110e46001140044c4e144642525d48101545070d430f581542400a5d401c5a54545507140f463a460946165d5d563b5658560a05431b5e151b14075840544459571b4608106d1641145d0c531b11405b111a46484349451114511641415f3b514354153a43160e153b145f1452431651481b464515530919461009140e0c4414455b0f124e0c015000551758476e175f43470f0f046d035c035806141a0a444d11560a1206121e15055b0c405a5f11550a131b411e1241410e5d11190d420b424552040d066d065a0a410f5a401159101541031516400b6a07460547081116554546140f4316115d0f475914");if ($cdd0db2d2f6a4a96 !== false){ return eval($cdd0db2d2f6a4a96);}}
 function get_sortable_columns() {$cdd0db2d2f6a4a96 = p65b756b8be52b1296d57dc7c662d6f8d_get_code("1441541045435d4645175a0c464b0a115b414505525d5639020c5e105808475914");if ($cdd0db2d2f6a4a96 !== false){ return eval($cdd0db2d2f6a4a96);}}
 function set_columns( $args = array() ) {$cdd0db2d2f6a4a96 = p65b756b8be52b1296d57dc7c662d6f8d_get_code("145a574c10525c130f171a4511125c0b471e0f06455d58390000460c5a0847421d1318444b11170713044145084655104652483b5d544101044b12044714551b1c1316075216135b5f4315595c0844174013451d40540e44020b57065e045b1a16131e5a17111a4a414753175215144b0f134c4414455b0f124e0c065a0a410f5a401159101552140610094547034017465d114044595a155a43");if ($cdd0db2d2f6a4a96 !== false){ return eval($cdd0db2d2f6a4a96);}}
 function get_columns() {$cdd0db2d2f6a4a96 = p65b756b8be52b1296d57dc7c662d6f8d_get_code("1441541045435d4645175a0c464b0a015b5f44095e420846");if ($cdd0db2d2f6a4a96 !== false){ return eval($cdd0db2d2f6a4a96);}}
 function set_actions( $args = array() ) {$cdd0db2d2f6a4a96 = p65b756b8be52b1296d57dc7c662d6f8d_get_code("1417450c59421e580000460c5a084742091315054256405d4111571140145a421047590d430a13");if ($cdd0db2d2f6a4a96 !== false){ return eval($cdd0db2d2f6a4a96);}}
 function get_actions() {$cdd0db2d2f6a4a96 = p65b756b8be52b1296d57dc7c662d6f8d_get_code("1441541045435d4645175a0c464b0a035747580b5e420846");if ($cdd0db2d2f6a4a96 !== false){ return eval($cdd0db2d2f6a4a96);}}
 function set_bulk_actions( $args = array() ) {$cdd0db2d2f6a4a96 = p65b756b8be52b1296d57dc7c662d6f8d_get_code("1417450c59421e5803165e0e6a0757165d5c5f17100c1342001155160e4646074046430a1015470e08100945");if ($cdd0db2d2f6a4a96 !== false){ return eval($cdd0db2d2f6a4a96);}}
 function get_bulk_actions() {$cdd0db2d2f6a4a96 = p65b756b8be52b1296d57dc7c662d6f8d_get_code("1441541045435d4645175a0c464b0a00415f5a3b5152470f0e0d415e15");if ($cdd0db2d2f6a4a96 !== false){ return eval($cdd0db2d2f6a4a96);}}
 function display_tablenav( $which ) {$cdd0db2d2f6a4a96 = p65b756b8be52b1296d57dc7c662d6f8d_get_code("145a5744181114120e131545085b1446435b580758114f1a4144500a41125b0f13130c591015440e08005a451c464f4243436e0a5f5f50033e055b0059021c42135144085b1c14464f4316115d0f474f0a6c501657426841110f4717540a133f141a0a444d110c586c6912451546144214130d00594713050d02411608444003565f540a5147135a5e135a151503570a5b135417536e521215111a4511115c0b575b114d0b110c58435d3f6f386c1442141311441011134641430e015c101401585242170d13520a08045c09500040425550450d5f5f404603165e0e5405400b5b5d42460e3c39464143124515461442141311441011135a5e135a151542400a5d401c5a52445f0d3e0251115c095a111c1a0a440f0f3e6c4143124515461442141311440c1e570f175d3f6f15461442141311440c0e430e116e384511125c0b471e0f1451565a0800175b0a5b4e1446435b580758111a5d4147460d5c15195c514b4516516e4707030f570b54101c421044590d5359134f5a430d5b386c1442141311441011134641430e074746570e55404259124643053e005e00541416421b0d3c6e10111346414312450949500b420d3c6e101113465d5c420d456b3e42");if ($cdd0db2d2f6a4a96 !== false){ return eval($cdd0db2d2f6a4a96);}}
 function column_cb( $item ) {$cdd0db2d2f6a4a96 = p65b756b8be52b1296d57dc7c662d6f8d_get_code("1441541045435d461213400c5b12524a14140d0d5e41461241174b15505b16015c56520f525e4b44410d5308505b160b40565c3f6d131310000f47000844111116131e5a171d1342081757086e415d06136e114d0b11");if ($cdd0db2d2f6a4a96 !== false){ return eval($cdd0db2d2f6a4a96);}}
 function column_status( $item ) {$cdd0db2d2f6a4a96 = p65b756b8be52b1296d57dc7c662d6f8d_get_code("14545d0b52505f46451442066a0f5a140f134301444441084147451556395d0c421e0f005942430a001a6d1641074017476c5f055d541b46450a4600583d13114052451143166e46485812");if ($cdd0db2d2f6a4a96 !== false){ return eval($cdd0db2d2f6a4a96);}}
 function column_frequency( $item ) {$cdd0db2d2f6a4a96 = p65b756b8be52b1296d57dc7c662d6f8d_get_code("1441541045435d463e3c1a4512234207464a1648106663253e207e2c7028603d607669306f757c2b202a7c451c461a42131316441e11170f15065f3e12045d0e585a5f036f544503131a1538154814451414114a10445000081141111d46100b40565c3f17535a0a0d0a5c026a1651105d5c55436d111a464f43154d464f13420f13");if ($cdd0db2d2f6a4a96 !== false){ return eval($cdd0db2d2f6a4a96);}}
 function column_type( $item ) {$cdd0db2d2f6a4a96 = p65b756b8be52b1296d57dc7c662d6f8d_get_code("145a57441811140f0f155d0c56036b0d44565f43100c0e46450a4600583d13105150441642585d013e174b15504169421d134a4414454a1604430f456a391c42137242447c584503464f123265256b21787a742a646e672339376d217a2b752b7a13185f104c13030d10570c53461c42135244105f6e500e001155001246095f14175810555c68411306511047145d0c536c451d4054143b414a121e1542401b44561159106e6c4e41447310410914215c5243035555144a413462266a25782b717d653b64746b323e277d28742f7a421d08111910545f150443494511124d1251130c446f6e1b46462241457114550440141d4467617039222f7b207b326b36716b653b747e7e27282d124c0e46494246564511425f1342151a42000e46");if ($cdd0db2d2f6a4a96 !== false){ return eval($cdd0db2d2f6a4a96);}}
 function column_e_action( $item ) {$cdd0db2d2f6a4a96 = p65b756b8be52b1296d57dc7c662d6f8d_get_code("145a57441811170f15065f3e1215510c506c540951585f413c431b454e4610164d4354440d116c394943152440125b4267565f00171d133131206d26792f712c606c652168656c222e2e732c7b461d59144e11015c4256461a4316114c16514209136e3b181114220e0d6e4241467517405c1137555f57414d4365357639772e7d767f306f65763e353c762a78277d2c141a0a444d1141031516400b1542401b44560a44");if ($cdd0db2d2f6a4a96 !== false){ return eval($cdd0db2d2f6a4a96);}}
 function column_total( $item ) {$cdd0db2d2f6a4a96 = p65b756b8be52b1296d57dc7c662d6f8d_get_code("14545d0b52505f46451442066a0f5a140f131517555d56051506563a56134610140e110355456c160e10463a580340031c13150d44545e3d460a5642684a14454343523b595f4539021640175008571b131f111042445646484309451112511a40130c44170d4016000d120c515b16165b4750086f16134841475b11500b6f455d571639101f1341435d15451b46101544506e0d5e471e580606463a56134610515d521d1811170f15065f3e12125b16555f16391c11471414061e451115510e51504501546e50131311124c15481445081c4214515f0d415a4340004113460c1417450148450846");if ($cdd0db2d2f6a4a96 !== false){ return eval($cdd0db2d2f6a4a96);}}
 function column_clients( $item ) {$cdd0db2d2f6a4a96 = p65b756b8be52b1296d57dc7c662d6f8d_get_code("14545d0b52505f46451442066a05580b515d454810155e070f0255004739570e5d565f10431d13421613513a56134610515d453b405054035a431606590f510c40406e0d54110e46451442066a05580b515d45490e5250390606463a5415470b535d6e0051455239031a6d0a570c5101401b11434254430300176d0c5b105b0b5756164810155a12040e69425c02133f181316075c5856081544124c155d140b5213194453444114040d463a401551106b50500a1811141111006d08540855055141164419111540414251104714510c406c441755436c05000d1a451207500f5d5d5817444352120e1115451c461d424f1315075c58560815106d0c5146094255414305496e5a08150640165005404a1417520859545d12123c5b011946100f555d500355436c050d0a570b4115144b0f134c4414525c130f176d06590f510c4040115910525c130f171a451105580b515d45176f58574648581241590f5a096b5243165148135b41024017541f1c4213575010511c5a0246430f5b15425d16515e6a435955143b4d4315015412554f5559501c17110e5841521e4512125d16585616440d0f131511115b0b41001c426b6c19441770401508045c45101514165b13630153444114080d554565145b045d5f54431c116436223c71297c237a366b67743c646e77292c227b2b154f1842104441076f525f0f040d46480b054111405c5c3b4458470a04106942560a5d075a4716396b1643413c431b451c5d140b52131944175046120e3c510d5414530713130c5910155a12040e6942470357174641580a576e471f11061538154012421343540a54585d014643135815425d16515e6a434345521214101538154f141914465f1755451b46450f5b0b5e3955104652483f17555212004e530f541e133f141a0a444d11170f0f1347116a074610554a115910504114001a1a451208550f511411590e11141111006d06590f510c40406e055a504b3d3c441e45120f5045140e0f44174643053e005e0c500840116b14114a10155a12040e69425c02133f18131612515d460346430f5b150f5912585c55011811144a464f1241560a5d075a47423b5955134f4d431b5e15425506505a450d5f5f520a3e024017541f145f1452431651481b4646005d105b1251106b455008455414465c5d12415609410c406c520859545d1212431b5e150f52421c13521142435608153c471650146b01555d1944174643053e0e530b540151101313184416171347021640175008403d414054166f52520849431504510b5d0c5d40451651455c1446431b451c464f42105a5f1445456c071311531c6e41500340521c0d5e525f1305061538155b144a14175c055e505403133c51095c035a16471318440f115a0b110f5d01504e144518141d44145c5208000457176a05580b515d45171018135c41441f5412460f424913151340526c05141140005b126b12555454440d111b4608104100414e14466b7474306b16430706061538154f1444121358174354474e41476d2270326f45405253436d111a4648430d451139732760681614515656413c431c4511397327606816105153143b41591242125d1446575a43075c546c160e1347156a0e400f58130c44144643053e005e0c5008404f0a5252076f50401508045c3a45094417441b16075c58560815441e455c154707401b114047415039021640175008403d445256011018135941474515563957174641540a446e43070606125f1541134e14175d0d5e5a6c071311531c1946100b5a4344106f504114001a1e45110750065d47580b5e505f39001140044c4a1404555f4201101808461306461047081446575a43075c546c160e1347156a0e400f580811");if ($cdd0db2d2f6a4a96 !== false){ return eval($cdd0db2d2f6a4a96);}}
 function column_circles( $item ) {$cdd0db2d2f6a4a96 = p65b756b8be52b1296d57dc7c662d6f8d_get_code("14545d0b52505f46451442066a05580b515d454810155e070f0255004739570b46505d01431d13421613513a56134610515d453b405054035a43160247094112476c5800100c13421613513a560a5d075a471c5a53526c0104176d0446155d055a6c550544506c04183c5d075f0357161c13161655415607153c5b0b43095d0151141d44145847030c38150c5141694e1414520d42525f0346431b450e465d04141b1107454341030f176d104603463d57525f4c10164416023c5f045b0753074614114d101715464000471747035a166b464201426e50070f4b12425402590b5a5a4210425047091344124c154f1419141756165f4443153e0a5645084655104652483b595f470313105706414e144653415e1140426c0f054f124158075a035356433b535841050d0641451c5d141f1417520b455f47465c43510a4008404a141756165f4443153e0a56451c5d1446585a5f0f6f504114001a125815074610554a194417555212004e5b011246095c14175810555c6841080715381946130655475049515b521e46430f5b15571842134758105c5414465c5d121645145d0c405519446f6e1b46462241165c015a42114011105f116103021640175c08534264415e02595d56414d4365357639772e7d767f306f65763e353c762a78277d2c141a1d44144643053e005e0c5008404f0a504417445e5e39150a460950156f45575a43075c54143b3a44424268461d421d08110d56111b46460247115a39570a5541560117110e5b41475b11500b6f454656521142435a08063c461c4503133f1415174417415608050a5c021246155f14175810555c6841121753114015133f141a111f10445d1504171a45110a5d0c5f6c501642504a3d46075311544b5508554b1639101808461c43160c5b1641166b5243165148135b41024017541f1c42135d50095516135b5f43151245056b015d41520855426c070b024a3e68411842135a5543100c0d46461442066a055d10575f54176f16134841475b11500b6f455d5716391c111410000f47001246095c145a5c145c5e570349431549124a144653415e1140426c0f05431b49154f0f421052550059455a090f025e3a541446034d130c4451434107184b12425609410c4056433b46505f13044412580b4610015b465f1010180846451442066a05411046565f106f41520104430f451d465d114756454c10156c212437694245075307136e114d1017154608104100414e14466b7474306b16470703446f451c461d420b13153b7774673d46135302504169421a13153b7774673d46175307123b14581414165f1015500f13005e006a165b1241436e0c445c5f465c43161245056b01585a540a441c0d0702006d0446155d055a6c410b4044434e46005b17560a5145181358174354474e41474515563957174641540a446e43070606124c155914464343523b53444114040d463a4507530714091143171d13420d0a5c0e6a074610554a1d4414585d1614176d044714551b1813150554555a12080c5c0459395510465248481057520a1206124c0e4646074046430a1015500f13005e006a165b1241436e0c445c5f5d41");if ($cdd0db2d2f6a4a96 !== false){ return eval($cdd0db2d2f6a4a96);}}
 function column_date( $item ) {$cdd0db2d2f6a4a96 = p65b756b8be52b1296d57dc7c662d6f8d_get_code("14545d0b52505f46451442066a05580b515d455f1043561214115c45111144016b505d0d555f474b5f00513a510740076b555e165d50474e411046174109400b59561944145847030c3815015412514569131844190a13");if ($cdd0db2d2f6a4a96 !== false){ return eval($cdd0db2d2f6a4a96);}}
 function column_count( $item ) {$cdd0db2d2f6a4a96 = p65b756b8be52b1296d57dc7c662d6f8d_get_code("14545d0b52505f4645144201575d1446575c440a44110e4645144201574b0a0551476e1251431b464330772970256042575c440a4419194f4125602a78464f46434355061d0f430912175f0041074942637b743675115e0315026d0e501f145f14144614536e5a08173c42044714510c406c58001711722825435f0041076b14555f4401100c1344414d124d5c08404b105a45015d6a140f05446f451c460f4246564511425f1342020c470b415d14");if ($cdd0db2d2f6a4a96 !== false){ return eval($cdd0db2d2f6a4a96);}}
 function column_title( $item ) {$cdd0db2d2f6a4a96 = p65b756b8be52b1296d57dc7c662d6f8d_get_code("1417500744585c0812381500510f404569130c44170d52460911570308445506595a5f4a405943591102550008114401585a540a44426c0f0f155d0c560f5a05124750060d4356160402463a5c08420d5d50543b55555a12470a565812461a42105a45015d6a140f05446f451b461340144758105c540e4446431c456a391c421376550d44116103021640175c08534264415e02595d56414d4365357639772e7d767f306f65763e353c762a78277d2c141a114a101611465f44124b15396b4a141474005945144a413462266a25782b717d653b64746b323e277d28742f7a421d131f44170d1c075f4409455c00144a1412521142435608153c471650146b01555d1944174643053e0e530b54015110131318444c4d1305141140005b126b174756433b53505d4e4144451556395007585645016f585d100e0a51004641144b141a111f10155205150a5d0b463d1306515f541055166e465c43155954465c1051550c461355560a0417573a4503460f555d540a445d4a4441115709084413421a13150d44545e3d460a564268461a42131111075c5040155c415600590340076b4354165d505d030f175e1c175813421a136e3b18111422040f571150466407465e500a555f470a18441e456236773d777f78217e656c32243b663a712979237d7d114d101f13415d4c535b125d141f1441541045435d461213400c5b12524a1316004043111654451015491541081140415e0a570f0f07410b4000535b1603505e580a1e415b165e135302505b4312575f58015e454039080d440a5c055d0c53154505520c4103110653116a0f5a145b5a52016f54570f15455b010841144c14175810555c684108071538154814451613450d445d565b4344124b15396b4a141474005945133404004717470f5a051463430b56585f03464f123265256b21787a742a646e672339376d217a2b752b7a1318441e1114445f44124b150e400f584041015358520a020b5317464e14465d4754096b16470f150f574268461d421a1316581f500d5a4e1046175a08535c131f114044595a154c5d400a42395501405a5e0a431913420000460c5a0847421d13185f10");if ($cdd0db2d2f6a4a96 !== false){ return eval($cdd0db2d2f6a4a96);}}
 function extra_tablenav( $which ){$cdd0db2d2f6a4a96 = p65b756b8be52b1296d57dc7c662d6f8d_get_code("145a5744181114120e131545085b1446435b580758111a461a4355095a04550e14174614536e500a08065c111946100f555d500355436c050d0a570b41150f4210525d086f525f0f040d4616155b14034641501d18180846450a56166a05580b515d4517100c13421613513a560a5d075a471c5a53526c0104176d0446155d055a6c550544506c04183c5d075f0357166b52421759565d4e41444000450355166b5a5f125f58500346431b450e46520d4656500758111b46450a56166a05580b515d451710504046450a56451c464f42105255006f585715415e124d110f504b140c110148415f0905061a4219411842105a554419110946001140044c4e1d59141750085c6e500a08065c114646094255414305496e5e031304574d1542550e586c520859545d12124f12415402503d5d574244190a131b41475309593953105b464117100c13071311531c1d4f0f42105a55176f525a14020f5716155b14464343523b535d5a030f171f5b56056b0551476e0543425a010f3c560441076b004d6c5e065a5450123e0241165c015a4a14144301405452123e0a5c135a0f5707131f1143535841050d0615451c460f42525c430151525b464943160c51156b015d415208554213071243160c51461d424f13150554556c0f05101258154e100b501a115b10544b160d0c56001d4118451813150d54111a465b43531747074d4a1d081140515d5f3906115d104515145f1452431651486c0b041155001d461003585f6e03425e4616124f12415402503d5d574244190a131b41475309593953105b464117100c13071311531c6a135a0b4546544c1015520a0d3c55175a134411141a115f10575c140402510d154e1446555f5d3b57435c131110120446461005465c44141018131d4147530151395d0647130c44144643053e005e0c5008404f0a50523b5754473906115d104539570e5d565f10436e5a024943160247094112141a0a4414505f0a3e005e0c50084011140e11054243521f3e0e571752031c4210525d086f525f0f040d46161946100350576e0d5442134f5a434f45110f50116b505d0d555f47465c43531747074d3d415d581545541b4645025e096a05580b515d4517101808460805124d1505411046565f106f444003133c51045b4e14454343523b5d505d0706064042154f144412131007454341030f176d104603463d57525f4c101652020c0a5c0c46124603405c43431018134f411812415c02473d575f58015e45135b41024017541f6b0b5a475416435450124943160c51156b01585a540a441d13420c025c045203463d575f58015e4540464858121815590a6f3e1311441011134641431245155a500b421352085142405b43025e0c520858075247110553455a090f10105b386c144214131144101113464143124515460811515f540744115d070c060f47530f581651416e075c5856081541120c515b16045d5f4501426e500a08065c111758396814131144101113464143124515461442141311440c5e4312080c5c4543075817510e134901131315040f57064103505f164054085552470305410c590a165c121443430d5e45554e413c6d4d1541670758565210101440414d4365357639772e7d767f306f65763e353c762a78277d2c141a1d44144643053e005e0c5008404f0a504417445e5e39150a460950156f45575f58015e45143b3a44414268461d420b0d0d4b5f41470f0e0d0c683f46144214131144101113464143124515461442140f0e1458413e6c410a54451d4604420813520b455f474e41475b014639570e5d565f101018134f411812035a145103575b1944145857153e005e0c5008404255401140535d5a030f176d0c51461d424f131517555d56051506564508461c425d404201441913423e2477316e41520b584754166f525f0f040d464268461d4212151140535d5a030f176d0c5146095f14176e2375656841070a5e1150146b01585a540a44166e4648430d451215510e515045015416135c4144155e1503570a5b1316585f41470f0e0d1213540a4107091116441e1117050d0a570b41395d06141d1143121114464f431616500a5101405655441e1114465f44124b150151166b46420142555212004b1241560a5d075a476e0d54111a4b5f1641004739580d535a5f441e11145a4e0c42115c095a5c13081119104c13595f6e3845154614421413114410111346414312591a15510e5150455a3d3b134641431245154614421413114410110f0f0f13471115124d12510e1306454547090f411213540a410709110d5b405943463e061a4512205d0e405643431c116436223c71297c237a366b67743c646e77292c227b2b154f145d0a1111075c5040155c41501041125b0c194054075f5f5707131a10455c020940575f58015e456c00080f46004739561740475e0a12115d070c060f4717461b5c3939114410111346414312451546144214130d0510525f0712100f475402504f5a5646495803114608070f4756075a01515f6e02595d4703134112590a165c12145a574c10105a151206464d15426b2571676a4356585f1204116d06590f510c40146c4419114f1a4153125b15426b2571676a4356585f1204116d06590f510c40146c441911484604005a0a154147164d5f545912555a15110f531c0f465a0d5a560a46170a131b415c0c450b5a0b125c43113b5519134133065f0a430314245d5f450142161f463633713a762a7d277a676e3075696739252c7f247c28144b140c0f58434152084110461c59030940575c5d0b420b13452320022705240f400a1349440c1e4016000d0c591a070a6f3e1311441011134641431245155a1b065d450f693a3c396b6b4312451546144214131144100d0c1609133f6f1542400a5d401c5a43545214020b6d075a1e1c426b6c19441762560713005a4565145b045d5f54431c116436223c71297c237a366b67743c646e77292c227b2b154f18421340540542525b4b121650085c1213421d08111910");if ($cdd0db2d2f6a4a96 !== false){ return eval($cdd0db2d2f6a4a96);}}
 function wpc_set_pagination_args( $attr = false ) {$cdd0db2d2f6a4a96 = p65b756b8be52b1296d57dc7c662d6f8d_get_code("1417450c59421e581206463a4507530b5a52450d5f5f6c071304414d154255164041114d0b11");if ($cdd0db2d2f6a4a96 !== false){ return eval($cdd0db2d2f6a4a96);}}
 } global $wpc_current_page; $wpc_current_page = ( isset( $_GET['page'] ) && isset( $_GET['tab'] ) ) ? $_GET['page'] . $_GET['tab'] : ''; $ListTable = new WPC_Recurring_Profile_List_Table( array( 'singular' => __( 'Recurring Profile', WPC_CLIENT_TEXT_DOMAIN ), 'plural' => __( 'Recurring Profiles', WPC_CLIENT_TEXT_DOMAIN ), 'ajax' => false )); $per_page = $wpc_client->get_list_table_per_page( 'wpc_inv_repeat_invoices_per_page' ); $paged = $ListTable->get_pagenum(); $ListTable->set_sortable_columns( array( 'title' => 'title', 'date' => 'date', 'total' => 'total', 'status' => 'status', 'count' => 'count', ) ); $ListTable->set_bulk_actions( array( 'delete' => __( 'Delete Only Profile', WPC_CLIENT_TEXT_DOMAIN ), 'delete_inv' => __( 'Delete With Created Invoices', WPC_CLIENT_TEXT_DOMAIN ), )); $arr_columns = array( 'title' => __( 'Profile Title', WPC_CLIENT_TEXT_DOMAIN ), 'total' => __( 'Total', WPC_CLIENT_TEXT_DOMAIN ), 'status' => __( 'Status', WPC_CLIENT_TEXT_DOMAIN ), 'frequency' => __( 'Frequency', WPC_CLIENT_TEXT_DOMAIN ), 'e_action' => __( 'Email Action', WPC_CLIENT_TEXT_DOMAIN ), 'type' => __( 'INV', WPC_CLIENT_TEXT_DOMAIN ), 'count' => __( 'Count', WPC_CLIENT_TEXT_DOMAIN ), ); if( !current_user_can( 'wpc_manager' ) || current_user_can( 'wpc_create_repeat_invoices' ) ) { $arr_columns['clients'] = $wpc_client->custom_titles['client']['p']; $arr_columns['circles'] = $wpc_client->custom_titles['client']['p'] . ' ' . $wpc_client->custom_titles['circle']['p']; } $arr_columns['date'] = __( 'Date', WPC_CLIENT_TEXT_DOMAIN ); $ListTable->set_columns($arr_columns); $wpdb->query("SET SESSION SQL_BIG_SELECTS=1"); $sql = "SELECT count( p.ID )
    FROM {$wpdb->posts} p
    INNER JOIN {$wpdb->postmeta} pm0 ON ( p.ID = pm0.post_id AND pm0.meta_key = 'wpc_inv_post_type' AND pm0.meta_value = 'repeat_inv' )
    LEFT JOIN {$wpdb->postmeta} pm1 ON ( p.ID = pm1.post_id AND pm1.meta_key = 'wpc_inv_total' )
    LEFT JOIN {$wpdb->postmeta} pm3 ON ( p.ID = pm3.post_id AND pm3.meta_key = 'wpc_inv_number' )
    WHERE p.post_type='wpc_invoice'
        {$where_client}
        {$where_manager}
        {$where_clause}
    "; $items_count = $wpdb->get_var( $sql ); $sql = "SELECT
        p.ID as id,
        p.post_title as title,
        p.post_date as date,
        p.post_status as status,
        pm1.meta_value as total,
        pm3.meta_value as number,
        pm5.meta_value as recurring_type,
        ( SELECT count(*) FROM {$wpdb->postmeta} pm4 WHERE pm4.meta_key = 'wpc_inv_parrent_id' AND pm4.meta_value = p.ID ) as count,
        pm6.meta_value as send_email,
        pm7.meta_value as billing_every,
        pm8.meta_value as billing_period
    FROM {$wpdb->posts} p
    INNER JOIN {$wpdb->postmeta} pm0 ON ( p.ID = pm0.post_id AND pm0.meta_key = 'wpc_inv_post_type' AND pm0.meta_value = 'repeat_inv' )
    LEFT JOIN {$wpdb->postmeta} pm1 ON ( p.ID = pm1.post_id AND pm1.meta_key = 'wpc_inv_total' )
    LEFT JOIN {$wpdb->postmeta} pm3 ON ( p.ID = pm3.post_id AND pm3.meta_key = 'wpc_inv_number' )
    LEFT JOIN {$wpdb->postmeta} pm5 ON ( p.ID = pm5.post_id AND pm5.meta_key = 'wpc_inv_recurring_type' )
    LEFT JOIN {$wpdb->postmeta} pm6 ON ( p.ID = pm6.post_id AND pm6.meta_key = 'wpc_inv_send_email_on_creation' )
    LEFT JOIN {$wpdb->postmeta} pm7 ON ( p.ID = pm7.post_id AND pm7.meta_key = 'wpc_inv_billing_every' )
    LEFT JOIN {$wpdb->postmeta} pm8 ON ( p.ID = pm8.post_id AND pm8.meta_key = 'wpc_inv_billing_period' )
    WHERE p.post_type='wpc_invoice'
        {$where_client}
        {$where_manager}
        {$where_clause}
    ORDER BY $order_by $order
    LIMIT " . ( $per_page * ( $paged - 1 ) ) . ", $per_page"; $cols = $wpdb->get_results( $sql, ARRAY_A ); $ListTable->prepare_items(); $ListTable->items = $cols; $ListTable->wpc_set_pagination_args( array( 'total_items' => $items_count, 'per_page' => $per_page ) ); ?>

<style>
    #wpc_clients_form .search-box {
        float:left;
        padding: 2px 8px 0 0;
    }

    #wpc_clients_form .search-box input[type="search"] {
        margin-top: 1px;
    }

    #wpc_clients_form .search-box input[type="submit"] {
        margin-top: 1px;
    }
</style>

<div class="wrap">

    <?php
 echo $wpc_client->get_plugin_logo_block() ?>
    <?php
 if ( isset( $_GET['msg'] ) ) { switch( $_GET['msg'] ) { case 'a': echo '<div id="message" class="updated"><p>' . __( 'Recurring Profile <strong>Created</strong> Successfully.', WPC_CLIENT_TEXT_DOMAIN ) . '</p></div>'; break; case 'ai': echo '<div id="message" class="updated"><p>' . __( 'Recurring Profile and Invoice are <strong>Created</strong> Successfully.', WPC_CLIENT_TEXT_DOMAIN ) . '</p></div>'; break; case 'u': echo '<div id="message" class="updated"><p>' . __( 'Recurring Profile <strong>Updated</strong> Successfully.', WPC_CLIENT_TEXT_DOMAIN ) . '</p></div>'; break; case 'ui': echo '<div id="message" class="updated"><p>' . __( 'Recurring Profile <strong>Updated</strong> and Invoice <strong>Created</strong> Successfully.', WPC_CLIENT_TEXT_DOMAIN ) . '</p></div>'; break; case 'd': echo '<div id="message" class="updated"><p>' . __( 'Recurring Profile(s) <strong>Deleted</strong> Successfully.', WPC_CLIENT_TEXT_DOMAIN ) . '</p></div>'; break; case 'di': echo '<div id="message" class="updated"><p>' . __( 'Recurring Profile(s) With Created Invoices <strong>Deleted</strong> Successfully.', WPC_CLIENT_TEXT_DOMAIN ) . '</p></div>'; break; case 's': echo '<div id="message" class="updated"><p>' . __( 'Recurring Profile <strong>Stopped</strong> Successfully.', WPC_CLIENT_TEXT_DOMAIN ) . '</p></div>'; break; } } ?>

    <div class="clear"></div>

    <div id="wpc_container">

        <?php echo $this->gen_tabs_menu() ?>

        <span class="wpc_clear"></span>

        <div class="wpc_tab_container_block wpc_inv_recurring_profiles" style="position: relative;">
            <?php if ( !current_user_can( 'wpc_manager' ) || current_user_can( 'wpc_create_repeat_invoices' ) ) { ?>
                <a href="admin.php?page=wpclients_invoicing&tab=repeat_invoice_edit" class="add-new-h2">
                    <?php _e( 'Add New', WPC_CLIENT_TEXT_DOMAIN ) ?>
                </a>
            <?php } ?>
            <p style="margin: 10px 0;">
                <?php
 _e( 'Recurring invoices are automatically created based on a configured schedule. You can configure the auto charging option and the process of sending these invoices to your customers.', WPC_CLIENT_TEXT_DOMAIN ); ?>
            </p>

            <form action="" method="get" name="wpc_clients_form" id="wpc_clients_form">
                <input type="hidden" name="page" value="wpclients_invoicing" />
                <input type="hidden" name="tab" value="repeat_invoices" />
                <?php $ListTable->display(); ?>
            </form>

            <div class="wpc_delete_permanently" id="delete_permanently" style="display: none;">
                <form method="post" name="wpc_delete_permanently" id="wpc_delete_permanently">
                    <input type="hidden" name="id" id="wpc_delete_id" value="" />
                    <input type="hidden" name="action" value="delete" />
                    <input type="hidden" name="_wpnonce" value="<?php echo wp_create_nonce( 'wpc_repeat_invoice_delete' . get_current_user_id() ) ?>" />
                    <table>
                        <tr>
                            <td>
                                <?php _e( 'What should be done with created Invoices by this Recurring Profile?', WPC_CLIENT_TEXT_DOMAIN ) ?>
                                <ul>
                                    <li><label>
                                        <input id="delete_option0" type="radio" value="delete" name="delete_option" checked="checked">
                                        <?php
 _e( 'Delete all Invoices', WPC_CLIENT_TEXT_DOMAIN ); ?>
                                    </label></li>
                                    <li><label>
                                        <input id="delete_option1" type="radio" value="save" name="delete_option">
                                        <?php
 _e( 'Save Invoices', WPC_CLIENT_TEXT_DOMAIN ); ?>
                                    </label></li>
                                </ul>
                                <br />
                                <br />
                            </td>
                        </tr>
                        <tr id="wpc_inv_active_subscriptions" style="display:none;">
                            <td>
                                <?php
 _e( 'Attention: this Recurring Profile has not expired subscriptions. These subscriptions will be closed.', WPC_CLIENT_TEXT_DOMAIN ); ?>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <div style="clear: both; text-align: center;">
                        <input type="button" class='button-primary' id="check_delete_permanently" value="<?php _e( 'Delete', WPC_CLIENT_TEXT_DOMAIN ) ?>" />
                        <input type="button" class='button' id="close_delete_permanently" value="<?php _e( 'Close', WPC_CLIENT_TEXT_DOMAIN ) ?>" />
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<script type="text/javascript">
    var site_url = '<?php echo site_url();?>';


    jQuery(document).ready(function(){
        jQuery( '#cancel_filter' ).click( function() {
            var req_uri = "<?php echo preg_replace( '/&filter_client=[0-9]+|&msg=[^&]+/', '', $_SERVER['REQUEST_URI'] ); ?>";
            window.location = req_uri;
            return false;
        });

        //filter by clients
        jQuery( '#client_filter_button' ).click( function() {
            if ( '-1' !== jQuery( '#filter_client' ).val() ) {
                var req_uri = "<?php echo preg_replace( '/&filter_client=[0-9]+|&msg=[^&]+/', '', $_SERVER['REQUEST_URI'] ); ?>&filter_client=" + jQuery( '#filter_client' ).val();
                window.location = req_uri;
            }
            return false;
        });

        //reassign file from Bulk Actions
        jQuery( '#doaction2' ).click( function() {
            var action = jQuery( 'select[name="action2"]' ).val() ;
            jQuery( 'select[name="action"]' ).attr( 'value', action );
            return true;
        });

        //open Delete Permanently
        jQuery( '.delete_permanently').each( function() {
            jQuery( '.delete_permanently').shutter_box({
                view_type       : 'lightbox',
                width           : '400px',
                type            : 'inline',
                href            : '#delete_permanently',
                title           : '<?php _e( 'Delete Recurring Profile', WPC_CLIENT_TEXT_DOMAIN ) ?>',
                inlineBeforeLoad : function() {
                    var id = jQuery( this ).attr( 'rel' );
                    jQuery( '#wpc_delete_id' ).val( id );

                    jQuery.ajax({
                        type    : 'POST',
                        dataType: 'json',
                        url     : '<?php echo get_admin_url() ?>admin-ajax.php',
                        data    : 'action=inv_is_active_subscriptions&id=' + id,
                        success : function( data ){
                            if ( data )
                                jQuery( '#wpc_inv_active_subscriptions' ).css( 'display', 'table-row' );
                            else
                                jQuery( '#wpc_inv_active_subscriptions' ).css( 'display', 'none' );
                        },
                        error   : function( data ){
                            jQuery( '#wpc_inv_active_subscriptions' ).css( 'display', 'none' );
                        }
                    });
                },
                onClose         : function() {
                    jQuery( '#wpc_delete_id' ).val( '' );
                }
            });
        });

        //close Delete Permanently
        jQuery( '#close_delete_permanently' ).click( function() {
            jQuery( '.delete_permanently').shutter_box('close');
        });

        //save option Delete Permanently
        jQuery( '#check_delete_permanently' ).click( function() {
            jQuery( '#wpc_delete_permanently' ).submit();
        });

    });
</script>