<?php
/**
 * Template Name: SMS: Profile Fields
 * Template Description: This template for [wpc_client_profile] shortcode SMS extension's additional fields
 * Template Tags: SMS
 *
 * This template can be overridden by copying it to your_current_theme/wp-client/sms_notifications/fields_on_profile.php.
 *
 * HOWEVER, on occasion WP-Client will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @author 	WP-Client
 */

//needs for translation
__( 'SMS: Profile Fields', WPC_CLIENT_TEXT_DOMAIN );
__( 'This template for [wpc_client_profile] shortcode SMS extension\'s additional fields', WPC_CLIENT_TEXT_DOMAIN );

if ( ! defined( 'ABSPATH' ) ) exit;

?>

<div class="wpc_form_line">

    <div class="mobile_number wpc_form_label">
        <label class="title" for="mobile_number">
            <?php _e( 'Mobile Number', WPC_CLIENT_TEXT_DOMAIN ); ?>
        </label>
    </div>

    <div class="wpc_form_field">
        <input type="text" id="mobile_number" name="mobile_number" value="<?php echo $mobile_number; ?>" />

        <span class="wpc_description">
            <?php _e( 'Used for SMS Notifications', WPC_CLIENT_TEXT_DOMAIN ); ?>
        </span>
    </div>

</div>

<?php if ( $need_carrier ) { ?>

    <div class="wpc_form_line">
        <div class="mobile_carrier wpc_form_label">
            <label class="title" for="mobile_carrier">
                <?php _e( 'Mobile Carrier', WPC_CLIENT_TEXT_DOMAIN ); ?>
            </label>
        </div>

        <div class="wpc_form_field">
            <select name="mobile_carrier" id="mobile_carrier">

                <option value="">
                    <?php _e( 'Select Mobile Carrier', WPC_CLIENT_TEXT_DOMAIN ); ?>
                </option>

                <?php if ( !empty( $carriers ) ) {
                    foreach( $carriers as $k => $carrier ) { ?>

                        <option value="<?php echo $k; ?>" <?php selected( $current_carrier == $k ); ?> >
                            <?php echo $carrier; ?>
                        </option>

                    <?php }

                } ?>

            </select>
        </div>
    </div>

<?php } ?>