<?php
global $wpdb, $wpc_client; $errors = ''; $wpc_sms_notification = $wpc_client->cc_get_settings( 'sms_notification' ); if ( isset( $_POST['wpc_sms_broadcast'] ) ) { if ( !isset( $_POST['wpc_sms_broadcast']['subject'] ) || '' == trim( $_POST['wpc_sms_broadcast']['subject'] ) ) { $errors .= __( 'Subject can not be empty!', WPC_CLIENT_TEXT_DOMAIN ) . '<br>'; } if ( !isset( $_POST['wpc_sms_broadcast']['message'] ) || '' == trim( $_POST['wpc_sms_broadcast']['message'] ) ) { $errors .= __( 'Message can not be empty!', WPC_CLIENT_TEXT_DOMAIN ) . '<br>'; } if ( ( !isset( $_POST['wpc_clients'] ) || '' == trim( $_POST['wpc_clients'] ) ) && ( !isset( $_POST['wpc_circles'] ) || '' == trim( $_POST['wpc_circles'] ) ) ) { $errors .= __( 'Send To can not be empty!', WPC_CLIENT_TEXT_DOMAIN ) . '<br>'; } if ( empty( $errors ) ) { $clients = $wpc_client->get_clients_from_popups( $_POST['wpc_clients'], $_POST['wpc_circles'] ); if ( count( $clients ) ) { $wpdb->query( $wpdb->prepare( "INSERT INTO {$wpdb->prefix}wpc_client_sms_broadcasts SET
                subject   = %s,
                message   = %s,
                creation_date   = %s
                ", trim( $_POST['wpc_sms_broadcast']['subject'] ), trim( $_POST['wpc_sms_broadcast']['message'] ), time() ) ); $broadcast_id = $wpdb->insert_id; foreach( $clients as $client_id ) { $wpdb->query( $wpdb->prepare( "INSERT INTO {$wpdb->prefix}wpc_client_sms_broadcasts_send SET
                    broadcast_id   = %d,
                    client_id   = %d,
                    status   = 0
                    ", $broadcast_id, $client_id ) ); } if ( ! wp_next_scheduled( 'wpc_sms_broadcast_send' ) ) { wp_schedule_event( time(), 'wpc_smsmin', 'wpc_sms_broadcast_send'); } do_action( 'wp_client_redirect', get_admin_url(). 'admin.php?page=wpclients_sms_notification&msg=c' ); exit; } $errors .= sprintf( __( 'Not found any %s in selected %s. Please try select another %s with %s', WPC_CLIENT_TEXT_DOMAIN ), $wpc_client->custom_titles['client']['p'], $wpc_client->custom_titles['circle']['s'], $wpc_client->custom_titles['circle']['s'], $wpc_client->custom_titles['client']['p'] ) . '<br>'; } } $carriers = array(); if ( isset( $wpc_sms_notification['carriers'] ) && count( $wpc_sms_notification['carriers'] ) ) { foreach( $wpc_sms_notification['carriers'] as $key => $value ) { if ( isset( $value['enable'] ) && 1 == $value['enable'] ) { $carriers[$key] = $value['title']; } } } ?>

<style type="text/css">
    .wrap input[type=text] {
        width:400px;
    }

    .wrap textarea {
        width:400px;
    }
</style>


<div class="wrap">

    <?php echo $wpc_client->get_plugin_logo_block() ?>

    <div class="wpc_clear"></div>

    <h2><?php _e( 'Create SMS Message', WPC_CLIENT_TEXT_DOMAIN ) ?></h2>


    <div id="message" class="error fade" <?php echo ( empty( $errors ) )? 'style="display: none;" ' : '' ?> ><?php echo $errors ?></div>


    <hr />

    <form method="post" name="wpc_sms_broadcast_form" id="wpc_sms_broadcast_form">
        <table class="form-table">
            <tr>
                <th>
                    <label for="wpc_sms_broadcast_subject"><?php _e( 'Subject', WPC_CLIENT_TEXT_DOMAIN ) ?>:</label>
                </th>
                <td>
                    <input type="text" id="wpc_sms_broadcast_subject" name="wpc_sms_broadcast[subject]" value="<?php if ( isset( $_POST['wpc_sms_broadcast']['subject'] ) ) echo esc_html( $_POST['wpc_sms_broadcast']['subject'] ) ?>" />
                </td>
            </tr>
            <tr>
                <th>
                    <label for="wpc_sms_broadcast_message"><?php _e( 'Message', WPC_CLIENT_TEXT_DOMAIN ) ?>:</label>
                </th>
                <td>
                    <textarea rows="8" id="wpc_sms_broadcast_message" name="wpc_sms_broadcast[message]"><?php if ( isset( $_POST['wpc_sms_broadcast']['message'] ) ) echo esc_html( $_POST['wpc_sms_broadcast']['message'] ) ?></textarea>
                    <p>
                        <span id="count_chars">160</span> <?php _e( 'characters remaining for next SMS.', WPC_CLIENT_TEXT_DOMAIN ) ?>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <span id="count_sms">1</span> <?php _e( 'SMS', WPC_CLIENT_TEXT_DOMAIN ) ?>
                    </p>
                </td>
            </tr>
            <tr>
                <th>
                    <label><?php _e( 'Send to', WPC_CLIENT_TEXT_DOMAIN ) ?> <?php echo $wpc_client->custom_titles['client']['s'] ?>:</label>
                </th>
                <td>
                    <?php
 $link_array = array( 'title' => sprintf( __( 'Select %s', WPC_CLIENT_TEXT_DOMAIN ), $wpc_client->custom_titles['client']['p'] ), 'text' => sprintf( __( 'Select %s', WPC_CLIENT_TEXT_DOMAIN ), $wpc_client->custom_titles['client']['p'] ) ); $input_array = array( 'name' => 'wpc_clients', 'id' => 'wpc_clients', 'value' => isset( $_POST['wpc_clients'] ) ? esc_html( $_POST['wpc_clients'] ) : '' ); $additional_array = array( 'counter_value' => ( isset( $_POST['wpc_clients'] ) && !empty( $_POST['wpc_clients'] ) ) ? count( explode( ',', esc_html( $_POST['wpc_clients'] ) ) ) : '0' ); $current_page = isset( $_GET['page'] ) ? $_GET['page'] : ''; $wpc_client->acc_assign_popup('client', isset( $current_page ) ? $current_page : '', $link_array, $input_array, $additional_array ); ?>
                </td>
            </tr>
            <tr>
                <th>
                    <label><?php _e( 'Send to', WPC_CLIENT_TEXT_DOMAIN ) ?> <?php echo $wpc_client->custom_titles['client']['s'] . ' ' . $wpc_client->custom_titles['circle']['p'] ?>:</label>
                </th>
                <td>
                    <?php
 if ( !isset ( $_POST['wpc_circles'] ) ) $count_circles = 0; else { if ( 'all' == $_POST['wpc_circles']) { if ( current_user_can( 'wpc_manager' ) && !current_user_can( 'administrator' ) ) { $count_circles = count( $wpc_client->cc_get_assign_data_by_object( 'manager', $manager_id, 'circle' ) ); } else { $count_circles = count( $wpc_client->cc_get_group_ids() ); } } else { $count_circles = count( explode( ',', $_POST['wpc_circles'] ) ); } } $link_array = array( 'title' => sprintf( __( 'Assign To %s', WPC_CLIENT_TEXT_DOMAIN ), $wpc_client->custom_titles['circle']['p'] ), 'text' => __( 'Select', WPC_CLIENT_TEXT_DOMAIN ) . ' ' . $wpc_client->custom_titles['client']['s'] . ' ' . $wpc_client->custom_titles['circle']['p'] ); $input_array = array( 'name' => 'wpc_circles', 'id' => 'wpc_circles', 'value' => isset( $_POST['wpc_circles'] ) ? esc_html( $_POST['wpc_circles'] ) : '' ); $additional_array = array( 'counter_value' => ( isset( $_POST['wpc_circles'] ) && !empty( $_POST['wpc_circles'] ) ) ? count( explode( ',', esc_html( $_POST['wpc_circles'] ) ) ) : '0' ); $current_page = isset( $_GET['page'] ) ? $_GET['page'] : ''; $wpc_client->acc_assign_popup('circle', isset( $current_page ) ? $current_page : '', $link_array, $input_array, $additional_array ); ?>
                </td>
            </tr>
            <tr>
                <th>
                     <input type="submit" value="<?php _e( 'Create and Send Message', WPC_CLIENT_TEXT_DOMAIN ) ?>" class="button-primary" id="create" name="create">
                </th>
                <td></td>
            </tr>
        </table>

        <hr />

        <table class="form-table">
            <tr>
                <th>
                    <label><?php _e( 'Send Test', WPC_CLIENT_TEXT_DOMAIN ) ?>:</label>
                </th>
                <td>
                    <table class="">
                        <tr>
                            <td><label for="mobile_number"><?php _e( 'Mobile Number', WPC_CLIENT_TEXT_DOMAIN ) ?></label></td>
                            <td><input type="text" style="width: 150px;"  id="mobile_number" value="" /></td>
                        </tr>
                        <?php if( $this->need_carrier() ) { ?>
                            <tr>
                                <td><label for="mobile_carrier"><?php _e( 'Mobile Carrier', WPC_CLIENT_TEXT_DOMAIN ) ?></label></td>
                                <td>
                                    <select id="mobile_carrier">
                                        <option value=""><?php _e( 'Select Mobile Carrier', WPC_CLIENT_TEXT_DOMAIN ) ?></option>
                                        <?php
 if ( count( $carriers ) ) { asort( $carriers ); foreach( $carriers as $key => $value ) { echo '<option value="' . $key . '">' . $value . '</option>'; } } ?>
                                    </select>
                                </td>
                        </tr>
                        <?php } ?>
                        <tr>
                            <td><input type="button" value="<?php _e( 'Send Test Now', WPC_CLIENT_TEXT_DOMAIN ) ?>" class="button" id="send_test"></td>
                            <td><div id="ajax_result" style="display: block;"></div></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>


    </form>
</div>


    <script type="text/javascript">

        jQuery(document).ready(function(){
            var site_url = '<?php echo site_url();?>';

            var sms_limit = 160;
            var sms_count = 1;
            //set maxlength
            jQuery('textarea').keyup(function(){
                //get the current text inside the textarea
                var text = jQuery( this ).val();
                //count the number of characters in the text
                var chars = text.length;

                var chars_left = chars % sms_limit;

                if ( 0 < chars && 0 == chars_left ) {
                    var count_chars = 0;
                } else {
                    var count_chars = sms_limit - ( chars % sms_limit );
                }


                sms_count =  Math.ceil( chars / sms_limit );

                jQuery( '#count_sms' ).html( sms_count );
                jQuery( '#count_chars' ).html( count_chars );

            });



            //send test
            jQuery( '#send_test' ).click( function() {
                var error = 0;

                jQuery( '.wpc_error' ).attr( 'class', '' );

                if ( '' == jQuery( '#mobile_number' ).val() ) {
                    jQuery( '#mobile_number' ).parents( 'span' ).attr( 'class', 'wpc_error' );
                    error = 1;
                }

                <?php if( $this->need_carrier() ) { ?>
                    if ( '' == jQuery( '#mobile_carrier option:selected' ).val() ) {
                        jQuery( '#mobile_carrier' ).parents( 'span' ).attr( 'class', 'wpc_error' );
                        error = 1;
                    }
                <?php } ?>

                if ( '' == jQuery( '#wpc_sms_broadcast_subject' ).val() ) {
                    jQuery( '#wpc_sms_broadcast_subject' ).parents( 'tr' ).attr( 'class', 'wpc_error' );
                    error = 1;
                }

                if ( '' == jQuery( '#wpc_sms_broadcast_message' ).val() ) {
                    jQuery( '#wpc_sms_broadcast_message' ).parents( 'tr' ).attr( 'class', 'wpc_error' );
                    error = 1;
                }

                if ( !error ) {

                    var mobile_number    = jQuery( '#mobile_number' ).val();
                    mobile_number = mobile_number.split('+').join('');
                    var mobile_carrier = '';
                    if( jQuery( '#mobile_carrier option:selected' ).length ) {
                        var mobile_carrier = jQuery( '#mobile_carrier option:selected' ).val();
                    }

                    var crypt_subject    = jQuery.base64Encode( jQuery( '#wpc_sms_broadcast_subject' ).val() );
                    crypt_subject        = crypt_subject.replace(/\+/g, "-");

                    var crypt_message    = jQuery.base64Encode( jQuery( '#wpc_sms_broadcast_message' ).val() );
                    crypt_message        = crypt_message.replace(/\+/g, "-");

                    jQuery("#ajax_result").html('');
                    jQuery("#ajax_result").show();
                    jQuery("#ajax_result").html('<div class="wpc_ajax_loading"></div>');

                    jQuery.ajax({
                    type: 'POST',
                    url: '<?php echo get_admin_url() ?>admin-ajax.php',
                    data: 'action=wpc_smsn_send_test&subject=' + crypt_subject + '&message=' + crypt_message + '&mobile_number=' + mobile_number + '&mobile_carrier=' +mobile_carrier,
                    dataType: "json",
                    success: function( data ) {
                        if(data.status) {
                            jQuery("#ajax_result").css('color', 'green');
                        } else {
                            jQuery("#ajax_result").css('color', 'red');
                        }

                        jQuery("#ajax_result").html(data.message);

                        setTimeout(function() {
                            jQuery("#ajax_result").fadeOut(10000);
                        }, 2500);

                    }
                    });

                }

                return false;
            });


            //send test
            jQuery( '#wpc_sms_broadcast_form' ).submit( function() {
                var error = 0;

                jQuery( '.wpc_error' ).attr( 'class', '' );

                if ( '' == jQuery( '#wpc_sms_broadcast_subject' ).val() ) {
                    jQuery( '#wpc_sms_broadcast_subject' ).parents( 'tr' ).attr( 'class', 'wpc_error' );
                    error = 1;
                }

                if ( '' == jQuery( '#wpc_sms_broadcast_message' ).val() ) {
                    jQuery( '#wpc_sms_broadcast_message' ).parents( 'tr' ).attr( 'class', 'wpc_error' );
                    error = 1;
                }

                if ( '' == jQuery( '#wpc_clients' ).val() && '' == jQuery( '#wpc_circles' ).val() ) {
                    if ( '' == jQuery( '#wpc_clients' ).val() ) {
                        jQuery( '#wpc_clients' ).parents( 'tr' ).attr( 'class', 'wpc_error' );
                        error = 1;
                    }

                    if ( '' == jQuery( '#wpc_circles' ).val() ) {
                        jQuery( '#wpc_circles' ).parents( 'tr' ).attr( 'class', 'wpc_error' );
                        error = 1;
                    }
                }

                if ( error ) {
                    return false;
                }

                return true;
            });




        });

    </script>