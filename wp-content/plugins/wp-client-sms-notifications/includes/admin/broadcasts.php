 <?php
global $wpdb, $wpc_client;

if ( isset($_REQUEST['_wp_http_referer']) ) {
    $redirect = remove_query_arg(array('_wp_http_referer' ), stripslashes_deep( $_REQUEST['_wp_http_referer'] ) );
} else {
    $redirect = get_admin_url(). 'admin.php?page=wpclients_sms_notification';
}
if ( isset( $_GET['action'] ) ) {
    switch ( $_GET['action'] ) {
        /* delete action */
        case 'delete':

            $cols_id = array();
            if ( isset( $_REQUEST['id'] ) ) {
                check_admin_referer( 'wpc_smsbroadcast_delete' .  $_REQUEST['id'] . get_current_user_id() );
                $cols_id = (array) $_REQUEST['id'];
            } elseif( isset( $_REQUEST['item'] ) )  {
                check_admin_referer( 'bulk-items' );
                $cols_id = $_REQUEST['item'];
            }

            if ( count( $cols_id ) ) {
                foreach ( $cols_id as $col_id ) {
                    $wpdb->query( $wpdb->prepare( "DELETE FROM {$wpdb->prefix}wpc_client_sms_broadcasts WHERE id = %d", $col_id ) );
                    $wpdb->query( $wpdb->prepare( "DELETE FROM {$wpdb->prefix}wpc_client_sms_broadcasts_send WHERE broadcast_id = %d", $col_id ) );
                }
                do_action( 'wp_client_redirect', add_query_arg( 'msg', 'd', $redirect ) );
                exit;
            }
            do_action( 'wp_client_redirect', $redirect );
            exit;

        break;
    }
}

//remove extra query arg
if ( !empty( $_GET['_wp_http_referer'] ) ) {
    do_action( 'wp_client_redirect', remove_query_arg( array( '_wp_http_referer', '_wpnonce'), stripslashes_deep( $_SERVER['REQUEST_URI'] ) ) );
    exit;
}

$where_clause = '';
if( !empty( $_GET['s'] ) ) {
    $where_clause .= $wpc_client->get_prepared_search( $_GET['s'], array(
        'b.subject',
    ) );
}

$order_by = 'id';
if ( isset( $_GET['orderby'] ) ) {
    switch( $_GET['orderby'] ) {
        case 'subject' :
            $order_by = 'b.subject';
            break;
        case 'creation_date' :
            $order_by = 'b.creation_date';
            break;
        case 'clients_count' :
            $order_by = 'clients_count';
            break;
        case 'clients_sent' :
            $order_by = 'clients_sent';
            break;
        case 'clients_fail' :
            $order_by = 'clients_fail';
            break;
    }
}

$order = ( isset( $_GET['order'] ) && 'asc' ==  strtolower( $_GET['order'] ) ) ? 'ASC' : 'DESC';


if( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class WPC_SMS_Broadcast_List_Table extends WP_List_Table {

    var $no_items_message = '';
    var $sortable_columns = array();
    var $default_sorting_field = '';
    var $actions = array();
    var $bulk_actions = array();
    var $columns = array();

    function __construct( $args = array() ){
        $args = wp_parse_args( $args, array(
            'singular'  => __( 'item', WPC_CLIENT_TEXT_DOMAIN ),
            'plural'    => __( 'items', WPC_CLIENT_TEXT_DOMAIN ),
            'ajax'      => false
        ) );

        $this->no_items_message = $args['plural'] . ' ' . __( 'not found.', WPC_CLIENT_TEXT_DOMAIN );

        parent::__construct( $args );


    }

    function __call( $name, $arguments ) {
        return call_user_func_array( array( $this, $name ), $arguments );
    }

    function prepare_items() {
        $columns  = $this->get_columns();
        $hidden   = array();
        $sortable = $this->get_sortable_columns();
        $this->_column_headers = array( $columns, $hidden, $sortable );
    }

    function column_default( $item, $column_name ) {
        if( isset( $item[ $column_name ] ) ) {
            return $item[ $column_name ];
        } else {
            return '';
        }
    }

    function no_items() {
        _e( $this->no_items_message, WPC_CLIENT_TEXT_DOMAIN );
    }

    function set_sortable_columns( $args = array() ) {
        $return_args = array();
        foreach( $args as $k=>$val ) {
            if( is_numeric( $k ) ) {
                $return_args[ $val ] = array( $val, $val == $this->default_sorting_field );
            } else if( is_string( $k ) ) {
                $return_args[ $k ] = array( $val, $k == $this->default_sorting_field );
            } else {
                continue;
            }
        }
        $this->sortable_columns = $return_args;
        return $this;
    }

    function get_sortable_columns() {
        return $this->sortable_columns;
    }

    function set_columns( $args = array() ) {
        if( count( $this->bulk_actions ) ) {
            $args = array_merge( array( 'cb' => '<input type="checkbox" />' ), $args );
        }
        $this->columns = $args;
        return $this;
    }


    function column_cb( $item ) {
        return sprintf(
            '<input type="checkbox" name="item[]" value="%s" />', $item['id']
        );
    }

    function column_subject( $item ) {
        $actions = array();

        $actions['delete'] = '<a onclick=\'return confirm("' . __( 'Are you sure to delete this Message?', WPC_CLIENT_TEXT_DOMAIN ) . '");\' href="admin.php?page=wpclients_sms_notification&action=delete&id=' . $item['id'] . '&_wpnonce=' . wp_create_nonce( 'wpc_smsbroadcast_delete' . $item['id'] . get_current_user_id() ) . '&_wp_http_referer=' . urlencode( wp_unslash( $_SERVER['REQUEST_URI'] ) ) . '" >' . __( 'Delete Permanently', WPC_CLIENT_TEXT_DOMAIN ) . '</a>';

        return sprintf('<span class="sms_subject" data-id="' . $item['id'] . '">%1$s</span> %2$s', $item['subject'], $this->row_actions( $actions ) );

    }

    function column_creation_date( $item ) {
        global $wpc_client;
        return $wpc_client->cc_date_format( $item['creation_date'] );
    }

    function column_clients_clients_sent( $item ) {
        return $item['clients_sent'];
    }

    function column_clients( $item ) {
        return $item['client_count'];
    }

    function column_clients_fail( $item ) {

        if ( 0 == $item['clients_fail'] ) {
            return 0;
        } else {
            return '<a class="various" href="#clients_fail_list">' . $item['clients_fail'] . '</a>';
        }

    }

    function extra_tablenav( $which ){
        if ( 'top' == $which ) {
            $this->search_box( __( 'Search Message', WPC_CLIENT_TEXT_DOMAIN ), 'search-submit' );
        }
    }

    function get_columns() {
        return $this->columns;
    }

    function set_actions( $args = array() ) {
        $this->actions = $args;
        return $this;
    }

    function get_actions() {
        return $this->actions;
    }

    function set_bulk_actions( $args = array() ) {
        $this->bulk_actions = $args;
        return $this;
    }

    function get_bulk_actions() {
        return $this->bulk_actions;
    }

    /**
     * Generate the table navigation above or below the table
     */
    function display_tablenav( $which ) {
        if ( 'top' == $which || 'bottom' == $which )
            wp_nonce_field( 'bulk-' . $this->_args['plural'] );
        ?>
        <div class="tablenav <?php echo esc_attr( $which ); ?>">

            <div class="alignleft actions bulkactions">
                <?php $this->bulk_actions(); ?>
            </div>
        <?php
            $this->pagination( $which );
            $this->extra_tablenav( $which );
        ?>
            <br class="wpc_clear" />
        </div>
    <?php
    }


    function wpc_set_pagination_args( $attr = false ) {
        $this->set_pagination_args( $attr );
    }

}


$ListTable = new WPC_SMS_Broadcast_List_Table( array(
    'singular'  => 'Item',
    'plural'    => 'Items',
    'ajax'      => false
));

$per_page   = $wpc_client->get_list_table_per_page( 'wpc_smsn_messaging_per_page' );
$paged      = $ListTable->get_pagenum();

$ListTable->set_sortable_columns( array(
    'subject'          => 'subject',
    'creation_date'    => 'creation_date',
    'clients_count'    => 'clients_count',
    'clients_sent'     => 'clients_sent',
    'clients_fail'     => 'clients_fail',
) );

$ListTable->set_bulk_actions(array(
    'delete'    => 'Delete',
));

$ListTable->set_columns(array(
    'cb'               => '<input type="checkbox" />',
    'subject'          => __( 'Subject', WPC_CLIENT_TEXT_DOMAIN ),
    'creation_date'    => __( 'Creation Date', WPC_CLIENT_TEXT_DOMAIN ),
    'clients_count'    => sprintf( __( 'Total %s', WPC_CLIENT_TEXT_DOMAIN ), $wpc_client->custom_titles['client']['p'] ),
    'clients_sent'     => __( 'Send Successful', WPC_CLIENT_TEXT_DOMAIN ),
    'clients_fail'     => __( 'Send Failure', WPC_CLIENT_TEXT_DOMAIN ),
));


$sql = "SELECT count( id )
    FROM {$wpdb->prefix}wpc_client_sms_broadcasts
    WHERE 1=1
        {$where_clause}
    ";
$items_count = $wpdb->get_var( $sql );

$sql = "

SELECT  b.*, COUNT( cc.client_id ) as clients_count, COUNT( cc1.client_id ) as clients_sent, COUNT( cc2.client_id ) as clients_fail
FROM {$wpdb->prefix}wpc_client_sms_broadcasts b, {$wpdb->prefix}wpc_client_sms_broadcasts_send cc
LEFT JOIN {$wpdb->prefix}wpc_client_sms_broadcasts_send cc1 ON cc1.id = cc.id AND  cc1.`status` = 1
LEFT JOIN {$wpdb->prefix}wpc_client_sms_broadcasts_send cc2 ON cc2.id = cc.id AND  cc2.`status` = 2
WHERE b.id = cc.broadcast_id {$where_clause}

GROUP BY cc.broadcast_id
ORDER BY {$order_by} {$order}
LIMIT " . ( $per_page * ( $paged - 1 ) ) . ", {$per_page}";

$cols = $wpdb->get_results( $sql, ARRAY_A );

$ListTable->prepare_items();
$ListTable->items = $cols;
$ListTable->wpc_set_pagination_args( array( 'total_items' => $items_count, 'per_page' => $per_page ) ); ?>

<style>
    #wpc_clients_form .search-box {
         float:left;
         padding: 2px 8px 0 0;
    }

    #wpc_clients_form .search-box input[type="search"] {
        margin-top: 1px;
    }

    #wpc_clients_form .search-box input[type="submit"] {
        margin-top: 1px;
    }
</style>

<script type="text/javascript">
     jQuery(document).ready(function(){
         var site_url = '<?php echo site_url();?>';

         //get failure list
         jQuery('.various').each( function() {
             var id = jQuery( this ).parents( 'tr' ).find( 'input[type="checkbox"]' ).val();

             jQuery(this).shutter_box({
                 view_type       : 'lightbox',
                 width           : '500px',
                 height          : '400px',
                 type            : 'ajax',
                 dataType        : 'json',
                 href            : '<?php echo get_admin_url() ?>admin-ajax.php',
                 ajax_data       : 'action=wpc_smsn_get_failure_list&id=' + id,
                 setAjaxResponse : function( data ) {
                     jQuery( '.sb_lightbox_content_title' ).html( data.title );
                     jQuery( '.sb_lightbox_content_body' ).html( data.content );
                 }
             });
         });
     });
 </script>

<div class="wrap">

    <?php echo $wpc_client->get_plugin_logo_block() ?>

    <?php if ( isset( $_GET['msg'] ) ) {
        $msg = $_GET['msg'];
        switch( $msg ) {
            case 'c':
                echo '<div id="message" class="updated fade"><p>' . __( 'Message <strong>Created and started Sending</strong>.', WPC_CLIENT_TEXT_DOMAIN ) . '</p></div>';
                break;
            case 'd':
                echo '<div id="message" class="updated fade"><p>' . __( 'Message(s) <strong>Deleted</strong> Successfully.', WPC_CLIENT_TEXT_DOMAIN ) . '</p></div>';
                break;
        }
    } ?>

    <div class="wpc_clear"></div>

    <div id="container23">
        <h2><?php _e( 'SMS Messaging', WPC_CLIENT_TEXT_DOMAIN )?><a class="add-new-h2" style="margin-left: 15px;" href="admin.php?page=wpclients_sms_notification&tab=create_broadcast"><?php _e( 'Create SMS Message', WPC_CLIENT_TEXT_DOMAIN ) ?></a></h2>
        <p><?php _e( 'Here, you can see all SMS messages that were sent, and their Success/Failure Status.', WPC_CLIENT_TEXT_DOMAIN ) ?></p>
        <p><?php _e( 'Configure SMS Settings - ', WPC_CLIENT_TEXT_DOMAIN ) ?><a href="<?php echo get_admin_url()?>admin.php?page=wpclients_settings&tab=sms_notification"><?php _e( 'HERE', WPC_CLIENT_TEXT_DOMAIN ) ?></a></p>

        <span class="wpc_clear"></span>

        <hr />

        <form action="" method="get" name="wpc_clients_form" id="wpc_clients_form">
            <input type="hidden" name="page" value="wpclients_sms_notification" />
            <?php $ListTable->display(); ?>
        </form>
    </div>
</div>
