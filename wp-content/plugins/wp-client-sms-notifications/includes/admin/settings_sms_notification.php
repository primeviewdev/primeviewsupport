<?php
global $wpc_client, $wpdb; if ( isset( $_POST['update_settings'] ) ) { $settings = $_POST['settings']; $carriers = array(); if ( isset( $settings['carriers'] ) ) { foreach( $settings['carriers'] as $key => $carrier ) { $carrier['email'] = trim( $carrier['email'] ); if ( '' == $carrier['title'] && '' == $carrier['email'] ) continue; $carrier['enable'] = ( isset( $carrier['enable'] ) && 1 == $carrier['enable'] ) ? 1 : 0; if ( '' == $carrier['title'] || '' == $carrier['email'] ) { $carrier['enable'] = 0; } $carriers[$key] = $carrier; } } $settings['carriers'] = $carriers; do_action( 'wp_client_settings_update', $settings, 'sms_notification' ); $wpc_client->redirect( $wpc_client->settings()->get_current_setting_url() . '&msg=u' ); } $wpc_sms_notification = $wpc_client->cc_get_settings( 'sms_notification' ); if ( isset( $wpc_sms_notification['carriers'] ) && count( $wpc_sms_notification['carriers'] ) ) { foreach( $wpc_sms_notification['carriers'] as $key => $value ) { if ( isset( $value['enable'] ) && 1 == $value['enable'] ) { $carriers[$key] = $value['title']; } } } $section_fields = array( array( 'type' => 'title', 'label' => __( 'SMS Notification', WPC_CLIENT_TEXT_DOMAIN ), ), ); $wpc_client->settings()->render_settings_section( $section_fields ); ?>
<style type="text/css">
    .wrap input[type=text] {
        width:390px;
    }

    .wrap input[type=password] {
        width:390px;
    }

</style>


<table class="form-table">
    <tr valign="top">
        <th scope="row">
            <label for="mobile_number"><?php _e( 'Admin Mobile Number', WPC_CLIENT_TEXT_DOMAIN ) ?>:</label>
        </th>
        <td>
             <input type="text" style="width: 145px !important;" id="mobile_number" name="settings[admin_number]" value="<?php echo ( isset( $wpc_sms_notification['admin_number'] ) ) ? $wpc_sms_notification['admin_number'] : '' ?>" />
        </td>
    </tr>

    <tr valign="top">
        <th scope="row">
            <label for="mobile_carrier"><?php _e( 'Admin Mobile Carrier', WPC_CLIENT_TEXT_DOMAIN ) ?>:</label>
        </th>
        <td>
            <select name="settings[admin_carrier]" id="mobile_carrier">
            <option value=""><?php _e( 'Select Mobile Carrier', WPC_CLIENT_TEXT_DOMAIN ) ?></option>
            <?php
 if ( count( $carriers ) ) { asort( $carriers ); foreach( $carriers as $key => $value ) { $seleted = ( isset( $wpc_sms_notification['admin_carrier'] ) && $wpc_sms_notification['admin_carrier'] == $key ) ? 'selected' : ''; echo '<option value="' . $key . '" ' . $seleted . '>' . $value . '</option>'; } } ?>
        </select>
        </td>
    </tr>
</table>


<table class="form-table">
    <tbody>
        <tr>
            <th>
                <label for="provider"><b><?php _e( 'Send SMS via', WPC_CLIENT_TEXT_DOMAIN ) ?>:</b></label>
            </th>
            <td>
                <select name="settings[provider]" id="provider" >
                    <option value="" <?php echo !( isset( $wpc_sms_notification['provider'] ) && '' != $wpc_sms_notification['provider'] ) ? 'selected' : '' ?> ><?php _e( 'Disable', WPC_CLIENT_TEXT_DOMAIN ) ?></option>
                    <?php foreach( $this->sender_objects as $key=>$val ) { ?>
                        <option value="<?php echo $key; ?>" <?php echo ( isset( $wpc_sms_notification['provider'] ) && $key == $wpc_sms_notification['provider'] ) ? 'selected' : '' ?> ><?php echo $val->title ?></option>
                    <?php } ?>
                </select>
            </td>
        </tr>

    </tbody>
</table>

<div class="wpc_sms_notification_settings">
    <?php foreach( $this->sender_objects as $key=>$val ) { ?>
        <div class="sms_settings_block" id="wpc_sms_<?php echo $key; ?>">
            <?php $val->provider_settings_html(); ?>
        </div>
    <?php } ?>
    <br />
</div>


<script type="text/javascript" language="javascript">

    jQuery( document ).ready( function( $ ) {
        jQuery('#provider').change(function() {
            var value = jQuery(this).val();
            if( value == 'email' ) {
                jQuery('#mobile_carrier').parents('tr').show();
            } else {
                jQuery('#mobile_carrier').parents('tr').hide();
            }
            jQuery('.sms_settings_block').hide();
            jQuery('#wpc_sms_' + value ).show('slow');
        }).change();
    });
</script>