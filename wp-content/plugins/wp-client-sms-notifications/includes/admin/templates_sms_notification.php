<?php
global $wpc_client; $wpc_sms_notification = $wpc_client->cc_get_settings( 'sms_notification' ); $wpc_templates_sms = $wpc_client->cc_get_settings( 'templates_sms' ); $wpc_sms_array['new_client_password'] = array( 'tab_label' => sprintf( __( 'New %s Created', WPC_CLIENT_TEXT_DOMAIN ), $wpc_client->custom_titles['client']['s'] ), 'label' => sprintf( __( 'New %s Created by Admin', WPC_CLIENT_TEXT_DOMAIN ), $wpc_client->custom_titles['client']['s'] ), 'description' => sprintf( __( '  >> This SMS will be sent to %s (if "Send Password" is checked)', WPC_CLIENT_TEXT_DOMAIN ), $wpc_client->custom_titles['client']['s'] ), 'subject_description' => '', 'body_description' => '', ); $wpc_sms_array['client_updated'] = array( 'tab_label' => sprintf( __( '%s Password Updated', WPC_CLIENT_TEXT_DOMAIN ), $wpc_client->custom_titles['client']['s'] ), 'label' => sprintf( __( '%s Password Updated', WPC_CLIENT_TEXT_DOMAIN ), $wpc_client->custom_titles['client']['s'] ), 'description' => sprintf( __( '  >> This SMS will be sent to %s (if "Send Password" is checked)', WPC_CLIENT_TEXT_DOMAIN ), $wpc_client->custom_titles['client']['s'] ) , 'subject_description' => '', 'body_description' => '', ); $wpc_sms_array['new_client_registered'] = array( 'tab_label' => sprintf( __( 'New %s Registers', WPC_CLIENT_TEXT_DOMAIN ), $wpc_client->custom_titles['client']['s'] ), 'label' => sprintf( __( 'New %s registers using Self-Registration Form', WPC_CLIENT_TEXT_DOMAIN ), $wpc_client->custom_titles['client']['s'] ), 'description' => sprintf( __( '  >> This SMS will be sent to Admin after a new %s registers with client registration form', WPC_CLIENT_TEXT_DOMAIN ), $wpc_client->custom_titles['client']['s'] ), 'subject_description' => '', 'body_description' => __( '{site_title}, {contact_name}, {user_name}will not be change as these placeholders will be used in the SMS.', WPC_CLIENT_TEXT_DOMAIN ), ); $wpc_sms_array['account_is_approved'] = array( 'tab_label' => sprintf( __( '%s Account is approved', WPC_CLIENT_TEXT_DOMAIN ), $wpc_client->custom_titles['client']['s'] ), 'label' => sprintf( __( '%s Account is approved', WPC_CLIENT_TEXT_DOMAIN ), $wpc_client->custom_titles['client']['s'] ), 'description' => sprintf( __( '  >> This SMS will be sent to %s after their account will approved (if "Send approval email" is checked).', WPC_CLIENT_TEXT_DOMAIN ), $wpc_client->custom_titles['client']['s'] ), 'subject_description' => __( '{site_title} and {contact_name} will not be changed as these placeholders will be used in the SMS.', WPC_CLIENT_TEXT_DOMAIN ), 'body_description' => __( '{site_title}, {contact_name} and {user_name} will not be change as these placeholders will be used in the SMS.', WPC_CLIENT_TEXT_DOMAIN ), ); $wpc_sms_array['staff_created'] = array( 'tab_label' => sprintf( __( '%s Created', WPC_CLIENT_TEXT_DOMAIN ), $wpc_client->custom_titles['staff']['s'] ), 'label' => sprintf( __( '%s Created by website Admin', WPC_CLIENT_TEXT_DOMAIN ), $wpc_client->custom_titles['staff']['s'] ), 'description' => sprintf( __( '  >> This SMS will be sent to %s (if "Send Password" is checked)', WPC_CLIENT_TEXT_DOMAIN ), $wpc_client->custom_titles['staff']['s'] ), 'subject_description' => '', 'body_description' => __( '{contact_name}, {user_name} and {password} will not be changed as these placeholders will be used in the SMS.', WPC_CLIENT_TEXT_DOMAIN ), ); $wpc_sms_array['staff_registered'] = array( 'tab_label' => sprintf( __( '%s Registered', WPC_CLIENT_TEXT_DOMAIN ), $wpc_client->custom_titles['staff']['s'] ), 'label' => sprintf( __( '%s Registered by %s', WPC_CLIENT_TEXT_DOMAIN ), $wpc_client->custom_titles['staff']['s'], $wpc_client->custom_titles['client']['s'] ), 'description' => sprintf( __( '  >> This SMS will be sent to %s after %s registered him (if "Send Password" is checked)', WPC_CLIENT_TEXT_DOMAIN ), $wpc_client->custom_titles['staff']['s'], $wpc_client->custom_titles['client']['s'] ), 'subject_description' => '', 'body_description' => __( '{contact_name}, {user_name} and {password} will not be changed as these placeholders will be used in the SMS.', WPC_CLIENT_TEXT_DOMAIN ), ); $wpc_sms_array['client_page_updated'] = array( 'tab_label' => __( 'Portal Page Updated', WPC_CLIENT_TEXT_DOMAIN ), 'label' => __( 'Portal Page Updated', WPC_CLIENT_TEXT_DOMAIN ), 'description' => sprintf( __( '  >> This SMS will be sent to %s (if "Send Update to selected %s is checked") when Portal Page updating', WPC_CLIENT_TEXT_DOMAIN ), $wpc_client->custom_titles['client']['s'], $wpc_client->custom_titles['client']['p'] ), 'subject_description' => __( '{contact_name}, {user_name} and {page_title} will not be changed as these placeholders will be used in the SMS.', WPC_CLIENT_TEXT_DOMAIN ), 'body_description' => __( '{contact_name},and {page_title} will not be change as these placeholders will be used in the SMS.', WPC_CLIENT_TEXT_DOMAIN ), ); $wpc_sms_array['new_file_for_client_staff'] = array( 'tab_label' => __( 'Admin uploads new file', WPC_CLIENT_TEXT_DOMAIN ), 'label' => sprintf( __( 'Admin uploads new file for %s', WPC_CLIENT_TEXT_DOMAIN ), $wpc_client->custom_titles['client']['s'] ), 'description' => sprintf( __( '  >> This SMS will be sent to %s and his %s when Admin or %s will upload new file for %s.', WPC_CLIENT_TEXT_DOMAIN ), $wpc_client->custom_titles['client']['s'], $wpc_client->custom_titles['staff']['p'], $wpc_client->custom_titles['manager']['s'], $wpc_client->custom_titles['client']['s'] ), 'subject_description' => '', 'body_description' => __( '{site_title}, {file_name} and {file_category} will not be change as these placeholders will be used in the SMS.', WPC_CLIENT_TEXT_DOMAIN ), ); $wpc_sms_array['client_uploaded_file'] = array( 'tab_label' => sprintf( __( '%s Uploads new file', WPC_CLIENT_TEXT_DOMAIN ), $wpc_client->custom_titles['client']['s'] ), 'label' => sprintf( __( '%s Uploads new file', WPC_CLIENT_TEXT_DOMAIN ), $wpc_client->custom_titles['client']['s'] ), 'description' => sprintf( __( '  >> This SMS will be sent to Admin and %s when %s will upload file(s)', WPC_CLIENT_TEXT_DOMAIN ), $wpc_client->custom_titles['client']['s'] . ' ' . $wpc_client->custom_titles['manager']['s'], $wpc_client->custom_titles['client']['s'] ), 'subject_description' => '', 'body_description' => __( '{user_name}, {site_title}, {file_name}, {file_category} will not be change as these placeholders will be used in the SMS.', WPC_CLIENT_TEXT_DOMAIN ), ); $wpc_sms_array['client_downloaded_file'] = array( 'tab_label' => sprintf( __( '%s Downloaded File', WPC_CLIENT_TEXT_DOMAIN ), $wpc_client->custom_titles['client']['s'] ), 'label' => sprintf( __( '%s Downloaded File', WPC_CLIENT_TEXT_DOMAIN ), $wpc_client->custom_titles['client']['s'] ), 'description' => sprintf( __( '  >> This SMS will be sent to Admin and %s when %s Downloaded file(s)', WPC_CLIENT_TEXT_DOMAIN ), $wpc_client->custom_titles['client']['s'] . ' ' . $wpc_client->custom_titles['manager']['s'], $wpc_client->custom_titles['client']['s'] ), 'subject_description' => '', 'body_description' => __( '{user_name}, {site_title}, {file_name} will not be change as these placeholders will be used in the SMS.', WPC_CLIENT_TEXT_DOMAIN ), ); $wpc_sms_array['notify_client_about_message'] = array( 'tab_label' => sprintf( __( 'PM: Notify To %s', WPC_CLIENT_TEXT_DOMAIN ), $wpc_client->custom_titles['client']['s'] ), 'label' => sprintf( __( 'Private Message: Notify Message To %s', WPC_CLIENT_TEXT_DOMAIN ), $wpc_client->custom_titles['client']['s'] ), 'description' => sprintf( __( '  >> This SMS will be sent to %s when Admin/%s sent private message (if "Receive email notification of private messages from admin" in selected in plugin settings).', WPC_CLIENT_TEXT_DOMAIN ), $wpc_client->custom_titles['client']['s'], $wpc_client->custom_titles['manager']['s'] ), 'subject_description' => '', 'body_description' => __( '{user_name} and {site_title} will not be change as these placeholders will be used in the SMS.', WPC_CLIENT_TEXT_DOMAIN ), ); $wpc_sms_array['notify_admin_about_message'] = array( 'tab_label' => sprintf( __( 'PM: Notify To Admin/%s', WPC_CLIENT_TEXT_DOMAIN ), $wpc_client->custom_titles['manager']['s'] ), 'label' => sprintf( __( 'Private Message: Notify Message To Admin/%s', WPC_CLIENT_TEXT_DOMAIN ), $wpc_client->custom_titles['manager']['s'] ), 'description' => sprintf( __( '  >> This SMS will be sent to Admin/%s when %s sent private message (if "Receive SMS notification of private messages from %s" is selected in plugin settings).', WPC_CLIENT_TEXT_DOMAIN ), $wpc_client->custom_titles['manager']['s'], $wpc_client->custom_titles['client']['s'], $wpc_client->custom_titles['client']['p'] ), 'subject_description' => '', 'body_description' => __( '{user_name} and {site_title} will not be change as these placeholders will be used in the SMS.', WPC_CLIENT_TEXT_DOMAIN ), ); $wpc_sms_array = apply_filters( 'wpc_client_templates_sms_array', $wpc_sms_array ); ?>

<style type="text/css">

    input[type="text"]{
        width: 100% ! important;
    }

</style>

<script type="text/javascript" language="javascript">
    jQuery(document).ready(function() {
        var site_url = '<?php echo site_url();?>';

        jQuery(".submit_email").click(function() {
            var name    = jQuery(this).attr('name');
            var id      = jQuery(this).attr('name')+"_body";

            //get content from editor
            if ( jQuery( '#wp-' + id + '-wrap' ).hasClass( 'tmce-active' ) ) {
                var content = tinyMCE.get( id ).getContent();
            } else {
                var content = jQuery( '#' + id ).val();

            }

            var subject = jQuery( '#' + jQuery(this).attr('name') + '_subject' ).val();

            jQuery("#ajax_result_"+name).html('');
            jQuery("#ajax_result_"+name).show();
            jQuery("#ajax_result_"+name).css('display', 'inline');
            jQuery("#ajax_result_"+name).html('<div class="wpc_ajax_loading"></div>');
            var crypt_content    = jQuery.base64Encode( content );
            crypt_content        = crypt_content.replace(/\+/g, "-");

            var crypt_subject    = jQuery.base64Encode( subject );
            crypt_subject        = crypt_subject.replace(/\+/g, "-");
            jQuery.ajax({
                type: "POST",
                url: '<?php echo get_admin_url() ?>admin-ajax.php',
                data: "action=wpc_save_template&wpc_templates[wpc_templates_sms][" + name + "][subject]=" + crypt_subject + "&wpc_templates[wpc_templates_sms][" + name + "][body]=" + crypt_content,
                dataType: "json",
                success: function(data){
                    if(data.status) {
                        jQuery("#ajax_result_"+name).css('color', 'green');
                    } else {
                        jQuery("#ajax_result_"+name).css('color', 'red');
                    }
                    jQuery("#ajax_result_"+name).html(data.message);
                    setTimeout(function() {
                        jQuery("#ajax_result_"+name).fadeOut(1500);
                    }, 2500);
                },
                error: function(data) {
                    jQuery("#ajax_result_"+name).css('color', 'red');
                    jQuery("#ajax_result_"+name).html('Unknown error.');
                    setTimeout(function() {
                        jQuery("#ajax_result_"+name).fadeOut(1500);
                    }, 2500);
                }
            });
        });

        jQuery(".wpc_templates_enable").click( function() {

            if ( 'checked' == jQuery( this ).attr( 'checked' ) ) {
                var value    = jQuery.base64Encode( '1' );
                value        = value.replace(/\+/g, "-");
            } else {
                var value    = jQuery.base64Encode( '0' );
                value        = value.replace(/\+/g, "-");
            }

            var name        = jQuery(this).attr('data-key');
            var checkbox    = jQuery( this );

            checkbox.hide();
            jQuery( '#wpc_ajax_loading_' + name ).addClass( 'wpc_ajax_loading' );

            jQuery.ajax({
                type: "POST",
                url: '<?php echo get_admin_url() ?>admin-ajax.php',
                data: "action=wpc_save_template&wpc_templates[wpc_templates_sms][" + name + "][enable]=" + value,
                dataType: "json",
                success: function(data){
                    jQuery( '#wpc_ajax_loading_' + name ).removeClass( 'wpc_ajax_loading' );
                    if ( 'checked' == checkbox.attr( 'checked' ) ) {
                        checkbox.prop('checked', false).attr('checked', false);
                    } else {
                        checkbox.prop('checked', true).attr('checked', true);
                    }
                    checkbox.show();
                }
            });
        });
    });
</script>


<div class="icon32" id="icon-link-manager"></div>
<p><?php _e( 'From here you can edit the SMS templates and settings.', WPC_CLIENT_TEXT_DOMAIN ) ?></p>


<form action="" method="post">
    <?php $tabs = array(); if ( is_array( $wpc_sms_array )&& count( $wpc_sms_array ) ) { foreach( $wpc_sms_array as $key => $values ) { $checked = ( !isset( $wpc_templates_sms[$key]['enable'] ) || '1' == $wpc_templates_sms[$key]['enable'] ) ? 'checked' : ''; $tabs[] = array( 'before_label' => '<div style="float:left;width: 25px;margin: 0;padding:8px 0 0 5px;line-height:18px;box-sizing: border-box;"><span id="wpc_ajax_loading_' . $key . '"></span><input type="checkbox" data-key="' . $key . '" class="wpc_templates_enable" name="wpc_templates[sms][' . $key . '][enable]" value="1" ' . $checked . ' title="' . __( 'Disable\Enable Notification', WPC_CLIENT_TEXT_DOMAIN ) . '"  /></div>', 'label' => $values['tab_label'], 'href' => "#wpc_$key", 'active' => ( count( $tabs ) > 0 ) ? false : true, ); } } $args = array( 'width' => '25%' ); echo $wpc_client->gen_vertical_tabs( $tabs, $args ); ?>

    <div id="tab-container" style="width: 74%;">
        <?php if ( is_array( $wpc_sms_array )&& count( $wpc_sms_array ) ) { $i = 1; foreach( $wpc_sms_array as $key => $values ) { ?>
                <div id="wpc_<?php echo $key ?>" class="tab-content <?php echo ( $i > 1 ) ? 'invisible' : '' ?>">
                    <div class="postbox" style="width: 100%;">
                        <h3 class="hndle"><span><?php echo $values['label'] ?></span></h3>
                        <span class="description"><?php echo $values['description'] ?></span>
                        <div class="inside">
                            <table class="form-table">
                                <tbody>
                                    <tr valign="top">
                                        <td colspan="2">
                                            <label for="<?php echo $key ?>_subject"><?php _e( 'SMS Subject', WPC_CLIENT_TEXT_DOMAIN ) ?>:</label>
                                            <br>
                                            <input type="text" name="wpc_templates[sms][<?php echo $key ?>][subject]" id="<?php echo $key ?>_subject" value="<?php echo ( isset( $wpc_templates_sms[$key]['subject'] ) ) ? stripslashes( $wpc_templates_sms[$key]['subject'] ) : '' ?>" />
                                            <?php if ( isset( $values['subject_description'] ) && '' != $values['subject_description'] ) { ?>
                                            <span class="description"><?php echo $values['subject_description'] ?></span>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <tr valign="top">
                                        <td colspan="2">
                                            <label for="<?php echo $key ?>"><?php _e( 'SMS Body', WPC_CLIENT_TEXT_DOMAIN ) ?>:</label>
                                            <br>
                                            <?php wp_editor( ( isset( $wpc_templates_sms[$key]['subject'] ) ) ? strip_tags( stripslashes( $wpc_templates_sms[$key]['body'] ) ) : '', $key . '_body', array( 'textarea_name' => 'wpc_templates[sms][' . $key . '][body]', 'textarea_rows' => 10, 'wpautop' => false, 'media_buttons' => false ) ); ?>
                                            <?php if ( isset( $values['body_description'] ) && '' != $values['body_description'] ) { ?>
                                            <span class="description"><?php echo $values['body_description'] ?></span>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="middle" align="left">
                                            <input type="button" name="<?php echo $key ?>" class="button-primary submit_email" value="Update" />
                                            <div id="ajax_result_<?php echo $key ?>" style="display: inline;"></div>
                                        </td>
                                        <td valign="middle" align="right">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            <?php $i++; } } ?>
    </div>

</form>