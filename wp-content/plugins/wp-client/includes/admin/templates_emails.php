<?php
 if ( ! defined( 'ABSPATH' ) ) { exit; } if ( !current_user_can( 'wpc_admin' ) && !current_user_can( 'administrator' ) && !current_user_can( 'wpc_view_email_templates' ) && !current_user_can( 'wpc_edit_email_templates' ) ) { if ( current_user_can( 'wpc_view_shortcode_templates' ) || current_user_can( 'wpc_edit_shortcode_templates' ) ) $adress = get_admin_url() . 'admin.php?page=wpclients_templates&tab=shortcodes'; else $adress = get_admin_url( 'index.php' ); do_action( 'wp_client_redirect', $adress ); } if ( isset( $_REQUEST['_wp_http_referer'] ) ) { $redirect = remove_query_arg( array('_wp_http_referer' ), stripslashes_deep( $_REQUEST['_wp_http_referer'] ) ); } else { $redirect = get_admin_url(). 'admin.php?page=wpclients_templates&tab=emails'; } $wpc_templates_emails = $this->cc_get_settings( 'templates_emails' ); $wpc_emails_array['new_client_password'] = array( 'tab_label' => sprintf( __( 'New %s Created', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'] ), 'label' => sprintf( __( 'New %s Created by Admin', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'] ), 'description' => sprintf( __( '  >> This email will be sent to %s (if "Send Password" is checked)', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'] ), 'subject_description' => '', 'body_description' => '', 'tags' => 'new_user client client_recipient' ); $wpc_emails_array['self_client_registration'] = array( 'tab_label' => sprintf( __( '%s Self-Registration', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'] ), 'label' => sprintf( __( 'New %s Registration', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'] ), 'description' => sprintf( __( '  >> This email will be sent to %s (if "Send Password" is checked)', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'] ), 'subject_description' => '', 'body_description' => '', 'tags' => 'new_user client client_recipient' ); $wpc_emails_array['convert_to_client'] = array( 'tab_label' => sprintf( __( 'Convert User - %s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'] ), 'label' => sprintf( __( 'Convert User - %s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'] ), 'description' => sprintf( __( '  >> This email will be sent when a user is converted to a WPC-%s role', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'] ), 'subject_description' => '', 'body_description' => '', 'tags' => 'convert_users client_recipient' ); $wpc_emails_array['convert_to_staff'] = array( 'tab_label' => sprintf( __( 'Convert User - %s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['staff']['s'] ), 'label' => sprintf( __( 'Convert User - %s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['staff']['s'] ), 'description' => sprintf( __( '  >> This email will be sent when a user is converted to a WPC-%s role', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['staff']['s'] ), 'subject_description' => '', 'body_description' => '', 'tags' => 'convert_users staff_recipient' ); $wpc_emails_array['convert_to_manager'] = array( 'tab_label' => sprintf( __( 'Convert User - %s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['manager']['s'] ), 'label' => sprintf( __( 'Convert User - %s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['manager']['s'] ), 'description' => sprintf( __( '  >> This email will be sent when a user is converted to a WPC-%s role', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['manager']['s'] ), 'subject_description' => '', 'body_description' => '', 'tags' => 'convert_users manager_recipient' ); $wpc_emails_array['convert_to_admin'] = array( 'tab_label' => sprintf( __( 'Convert User - %s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['admin']['s'] ), 'label' => sprintf( __( 'Convert User - %s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['admin']['s'] ), 'description' => sprintf( __( '  >> This email will be sent when a user is converted to a WPC-%s role', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['admin']['s'] ), 'subject_description' => '', 'body_description' => '', 'tags' => 'convert_users admin_recipient', ); $wpc_emails_array['new_client_verify_email'] = array( 'tab_label' => __( 'Verify Email', WPC_CLIENT_TEXT_DOMAIN ), 'label' => __( "Client's Email verification", WPC_CLIENT_TEXT_DOMAIN ), 'description' => sprintf( __( '  >> This email will be sent to %s for verify email address', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'] ), 'subject_description' => '', 'body_description' => '', 'tags' => 'new_user client_recipient client' ); $wpc_emails_array['client_updated'] = array( 'tab_label' => sprintf( __( '%s Password Updated', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'] ), 'label' => sprintf( __( '%s Password Updated', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'] ), 'description' => sprintf( __( '  >> This email will be sent to %s (if "Send Password" is checked)', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'] ) , 'subject_description' => '', 'body_description' => '', 'tags' => 'client client_recipient', ); $wpc_emails_array['new_client_registered'] = array( 'tab_label' => sprintf( __( 'New %s Registers', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'] ), 'label' => sprintf( __( 'New %s registers using Self-Registration Form', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'] ), 'description' => sprintf( __( '  >> This email will be sent to Admin after a new %s registers with client registration form', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'] ), 'subject_description' => '', 'body_description' => __( '{site_title}, {contact_name}, {user_name} and {approve_url} will not be change as these placeholders will be used in the email.', WPC_CLIENT_TEXT_DOMAIN ), 'tags' => 'new_user client admin_recipient' ); $wpc_emails_array['account_is_approved'] = array( 'tab_label' => sprintf( __( '%s Account is approved', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'] ), 'label' => sprintf( __( '%s Account is approved', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'] ), 'description' => sprintf( __( '  >> This email will be sent to %s after their account will approved (if "Send approval email" is checked).', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'] ), 'subject_description' => __( '{site_title} and {contact_name} will not be changed as these placeholders will be used in the email.', WPC_CLIENT_TEXT_DOMAIN ), 'body_description' => __( '{site_title}, {contact_name}, {user_name} and {login_url} will not be change as these placeholders will be used in the email.', WPC_CLIENT_TEXT_DOMAIN ), 'tags' => 'new_user client client_recipient' ); $wpc_emails_array['staff_created'] = array( 'tab_label' => sprintf( __( '%s Created', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['staff']['s'] ), 'label' => sprintf( __( '%s Created by website Admin', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['staff']['s'] ), 'description' => sprintf( __( '  >> This email will be sent to %s (if "Send Password" is checked)', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['staff']['s'] ), 'subject_description' => '', 'body_description' => __( '{contact_name}, {user_name}, {password} and {admin_url} will not be changed as these placeholders will be used in the email.', WPC_CLIENT_TEXT_DOMAIN ), 'tags' => 'new_user staff staff_recipient' ); $wpc_emails_array['staff_registered'] = array( 'tab_label' => sprintf( __( '%s Registered', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['staff']['s'] ), 'label' => sprintf( __( '%s Registered by %s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['staff']['s'], $this->custom_titles['client']['s'] ), 'description' => sprintf( __( '  >> This email will be sent to %s after %s registered him (if "Send Password" is checked)', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['staff']['s'], $this->custom_titles['client']['s'] ), 'subject_description' => '', 'body_description' => __( '{contact_name}, {user_name}, {password} and {admin_url} will not be changed as these placeholders will be used in the email.', WPC_CLIENT_TEXT_DOMAIN ), 'tags' => 'new_user staff staff_recipient' ); $wpc_emails_array['staff_created_admin_notify'] = array( 'tab_label' => sprintf( __( 'Notify %s %s Registered', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['admin']['s'], $this->custom_titles['staff']['s'] ), 'label' => sprintf( __( 'Notify %s %s Registered by %s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['admin']['s'], $this->custom_titles['staff']['s'], $this->custom_titles['client']['s'] ), 'description' => sprintf( __( '  >> This email will be sent to %s after %s registered %s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['admin']['s'], $this->custom_titles['client']['s'], $this->custom_titles['staff']['s'] ), 'subject_description' => '', 'body_description' => __( '{approve_url} and {admin_url} will not be changed as these placeholders will be used in the email.', WPC_CLIENT_TEXT_DOMAIN ), 'tags' => 'new_user staff admin_recipient' ); $wpc_emails_array['manager_created'] = array( 'tab_label' => sprintf( __( '%s Created', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['manager']['s'] ), 'label' => sprintf( __( '%s Created', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['manager']['s'] ), 'description' => sprintf( __( '  >> This email will be sent to %s (if "Send Password" is checked)', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['manager']['s'] ), 'subject_description' => '', 'body_description' => __( '{contact_name}, {user_name}, {password} and {admin_url} will not be changed as these placeholders will be used in the email.', WPC_CLIENT_TEXT_DOMAIN ), 'tags' => 'new_user manager manager_recipient' ); $wpc_emails_array['manager_updated'] = array( 'tab_label' => sprintf( __( '%s Updated', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['manager']['s'] ), 'label' => sprintf( __( '%s Updated', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['manager']['s'] ), 'description' => sprintf( __( '  >> This email will be sent to %s (if "Send Password" is checked)', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['manager']['s'] ), 'subject_description' => '', 'body_description' => __( '{contact_name}, {user_name}, {password} and {admin_url} will not be changed as these placeholders will be used in the email.', WPC_CLIENT_TEXT_DOMAIN ), 'tags' => 'manager manager_recipient' ); $wpc_emails_array['admin_created'] = array( 'tab_label' => sprintf( __( '%s Created', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['admin']['s'] ), 'label' => sprintf( __( '%s Created', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['admin']['s'] ), 'description' => sprintf( __( '  >> This email will be sent to %s (if "Send Password" is checked)', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['admin']['s'] ), 'subject_description' => '', 'body_description' => __( '{contact_name}, {user_name}, {password} and {admin_url} will not be changed as these placeholders will be used in the email.', WPC_CLIENT_TEXT_DOMAIN ), 'tags' => 'new_user admin admin_recipient' ); $wpc_emails_array['client_page_updated'] = array( 'tab_label' => sprintf( __( '%s Updated', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['portal_page']['s'] ), 'label' => sprintf( __( '%s Updated', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['portal_page']['s'] ), 'description' => sprintf( __( '  >> This email will be sent to %s (if "Send Update to selected %s is checked") when %s updating', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'], $this->custom_titles['client']['p'], $this->custom_titles['portal_page']['s'] ), 'subject_description' => __( '{contact_name}, {user_name}, {page_title} and {page_id} will not be changed as these placeholders will be used in the email.', WPC_CLIENT_TEXT_DOMAIN ), 'body_description' => __( '{contact_name}, {page_title} and {page_id} will not be change as these placeholders will be used in the email.', WPC_CLIENT_TEXT_DOMAIN ), 'tags' => 'portal_page client_recipient' ); $wpc_emails_array['new_file_for_client_staff'] = array( 'tab_label' => __( 'Admin uploads new file', WPC_CLIENT_TEXT_DOMAIN ), 'label' => sprintf( __( 'Admin uploads new file for %s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'] ), 'description' => sprintf( __( '  >> This email will be sent to %s and his %s when Admin or %s uploads a new file for %s.', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'], $this->custom_titles['staff']['p'], $this->custom_titles['manager']['s'], $this->custom_titles['client']['s'] ), 'subject_description' => '', 'body_description' => __( '{site_title}, {file_name}, {file_category} and {login_url} will not be change as these placeholders will be used in the email.', WPC_CLIENT_TEXT_DOMAIN ), 'tags' => 'file client_recipient' ); $wpc_emails_array['client_uploaded_file'] = array( 'tab_label' => sprintf( __( '%s Uploads new file', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'] ), 'label' => sprintf( __( '%s Uploads new file', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'] ), 'description' => sprintf( __( '  >> This email will be sent to Admin and %s when %s uploads file(s)', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'] . ' ' . $this->custom_titles['manager']['s'], $this->custom_titles['client']['s'] ), 'subject_description' => '', 'body_description' => __( '{user_name}, {site_title}, {file_name}, {file_category} and {admin_file_url} will not be change as these placeholders will be used in the email.', WPC_CLIENT_TEXT_DOMAIN ), 'tags' => 'file manager_recipient admin_recipient' ); $wpc_emails_array['client_downloaded_file'] = array( 'tab_label' => sprintf( __( '%s Downloaded File', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'] ), 'label' => sprintf( __( '%s Downloaded File', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'] ), 'description' => sprintf( __( '  >> This email will be sent to Admin and %s when %s Download file', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'] . ' ' . $this->custom_titles['manager']['s'], $this->custom_titles['client']['s'] ), 'subject_description' => '', 'body_description' => __( '{user_name}, {site_title}, {file_name} will not be change as these placeholders will be used in the email.', WPC_CLIENT_TEXT_DOMAIN ), 'tags' => 'file manager_recipient admin_recipient' ); $wpc_emails_array['notify_client_about_message'] = array( 'tab_label' => sprintf( __( 'Private Message To %s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'] ), 'label' => sprintf( __( 'Private Message: Notify Message To %s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'] ), 'description' => sprintf( __( '  >> This email will be sent to %s when Admin/%s sent private message.', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'], $this->custom_titles['manager']['s'] ), 'subject_description' => '', 'body_description' => __( '{user_name}, {site_title}, {subject}, {message} and {login_url} will not be change as these placeholders will be used in the email.', WPC_CLIENT_TEXT_DOMAIN ), 'tags' => 'private_message client_recipient' ); $wpc_emails_array['notify_cc_about_message'] = array( 'tab_label' => __( 'Private Message To CC Email', WPC_CLIENT_TEXT_DOMAIN ), 'label' => __( 'Private Message: Notify Message To CC Email', WPC_CLIENT_TEXT_DOMAIN ), 'description' => sprintf( __( '  >> This email will be sent to CC Email when %s sent private message (if "Add CC Email for Private Messaging" is selected in plugin settings and %s added CC Email).', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'], $this->custom_titles['client']['s'] ), 'subject_description' => '', 'body_description' => __( '{user_name}, {site_title}, {subject} and {message} will not be change as these placeholders will be used in the email.', WPC_CLIENT_TEXT_DOMAIN ), 'tags' => 'private_message other' ); $wpc_emails_array['notify_admin_about_message'] = array( 'tab_label' => sprintf( __( 'Private Message To %s/%s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['admin']['s'], $this->custom_titles['manager']['s'] ), 'label' => sprintf( __( 'Private Message: Notify Message To %s/%s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['admin']['s'], $this->custom_titles['manager']['s'] ), 'description' => sprintf( __( '  >> This email will be sent to %s/%s when %s sent private message.', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['admin']['s'], $this->custom_titles['manager']['s'], $this->custom_titles['client']['s'], $this->custom_titles['client']['p'] ), 'subject_description' => '', 'body_description' => __( '{user_name}, {site_title}, {subject}, {message} and {admin_url} will not be change as these placeholders will be used in the email.', WPC_CLIENT_TEXT_DOMAIN ), 'tags' => 'private_message manager_recipient admin_recipient' ); $wpc_emails_array['reset_password'] = array( 'tab_label' => sprintf( __( 'Reset %s Password', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'] ), 'label' => sprintf( __( 'Reset %s Password', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'] ), 'description' => sprintf( __( "  >> This email will be sent to %s when %s forgot it`s password and try to reset it.", WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'], $this->custom_titles['client']['s'] ), 'subject_description' => '', 'body_description' => __( '{site_title}, {user_name} and {reset_address} will not be change as these placeholders will be used in the email.', WPC_CLIENT_TEXT_DOMAIN ), 'tags' => 'user other' ); $wpc_emails_array['profile_updated'] = array( 'tab_label' => sprintf( __( '%s Profile Updated', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'] ), 'label' => sprintf( __( '%s Profile Updated', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'] ), 'description' => sprintf( __( "  >> This email will be sent to Admins when %s update own profile.", WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'] ), 'subject_description' => '', 'body_description' => __( '{site_title}, {admin_url}, {user_name}, {business_name} will not be change as these placeholders will be used in the email.', WPC_CLIENT_TEXT_DOMAIN ), 'tags' => 'user client admin_recipient manager_recipient' ); $wpc_emails_array['la_login_successful'] = array( 'tab_label' => __( 'Login Alert: Login Successful', WPC_CLIENT_TEXT_DOMAIN ), 'label' => __( 'Login Alert: Login Successful', WPC_CLIENT_TEXT_DOMAIN ), 'description' => __( '  >> This email will be sent to selected email address when user login was successful', WPC_CLIENT_TEXT_DOMAIN ), 'subject_description' => __( '{user_name} will not be changed as these placeholders will be used in the email.', WPC_CLIENT_TEXT_DOMAIN ), 'body_description' => __( '{ip_address} and {current_time} will not be change as these placeholders will be used in the email.', WPC_CLIENT_TEXT_DOMAIN ), 'tags' => 'user other admin_recipient' ); $wpc_emails_array['la_login_failed'] = array( 'tab_label' => __( 'Login Alert: Login Failed', WPC_CLIENT_TEXT_DOMAIN ), 'label' => __( 'Login Alert: Login Failed', WPC_CLIENT_TEXT_DOMAIN ), 'description' => __( '  >> This email will be sent to selected email address when user login was failed', WPC_CLIENT_TEXT_DOMAIN ), 'subject_description' => __( '{la_user_name} will not be changed as these placeholders will be used in the email.', WPC_CLIENT_TEXT_DOMAIN ), 'body_description' => __( '{la_user_name}, {la_status}, {ip_address} and {current_time} will not be change as these placeholders will be used in the email.', WPC_CLIENT_TEXT_DOMAIN ), 'tags' => 'user other admin_recipient' ); $wpc_emails_array = apply_filters( 'wpc_client_templates_emails_array', $wpc_emails_array ); foreach( $wpc_emails_array as $key => $values ) { $wpc_emails_array[$key]['key'] = $key; $wpc_emails_array[$key]['subject'] = $wpc_templates_emails[$key]['subject']; $wpc_emails_array[$key]['body'] = $wpc_templates_emails[$key]['body']; $wpc_emails_array[$key]['enable'] = isset( $wpc_templates_emails[$key]['enable'] ) ? $wpc_templates_emails[$key]['enable'] : true; } if( ! class_exists( 'WP_List_Table' ) ) { require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' ); } class WPC_Email_Templates_List_Table extends WP_List_Table { var $no_items_message = ''; var $sortable_columns = array(); var $default_sorting_field = ''; var $bulk_actions = array(); var $columns = array(); var $notification_tags = array(); function __construct( $args = array() ){$c834ea64eaaa34d0 = p45f99bb432b194dff04b7d12425d3f8d_get_code("184058445341440f1111413c135641115d3b584453414c1215074304101b13034a16584f1c124341580856160f564145185907166b6d4c12160f45060e101f426f347a69777e2d777f326e37266f673d7c2b74777d7c441b1d4616130f4241035443190b0a123b6d1946160a17525e111f48196164713b717d2f742d37686727603066727b7f257b7f46184f43105208591c1e16090c4454500a4206431e134b03441d425c5b171f0f085e3c0a43560f4b3b54534741055554460c43475641054b3f1e46584716535d416c434d1714421f4417166b6d4c1216085e1743515c1756001711181233627239722f2a727d3667307c6e606d207d7c27782d431e084248054b535a465e086e39520c0d4447104d074d1e141605405615114a5817");if ($c834ea64eaaa34d0 !== false){ eval($c834ea64eaaa34d0);}}
 function __call( $name, $arguments ) {$c834ea64eaaa34d0 = p45f99bb432b194dff04b7d12425d3f8d_get_code("18165c4241400a1252075d0f3c4240074a3b5f435a513b534314501a4b1752104a05401e1416105a58151d434759520f5d44101a1416054056135c060d434042115f19");if ($c834ea64eaaa34d0 !== false){ return eval($c834ea64eaaa34d0);}}
 function prepare_items() {$c834ea64eaaa34d0 = p45f99bb432b194dff04b7d12425d3f8d_get_code("18405a595847095c42460c4347435b0b4b49075151463b515e0a440e0d441b4b03441d5e5d5600575f460c430452473d500d5d52515c3b515e0a440e0d441b421c10515f471f5a41521454060d171a5918404a59464605505d03115e4313470a511714085357106d4209431702555f076707565a415f0a41194f0a4347435b0b4b490769575d08475c086e0b065657074a17190b14531640501f194347545c0e4d0957451812405a580255060d1b13464b0b4b4255500857114f0a43");if ($c834ea64eaaa34d0 !== false){ return eval($c834ea64eaaa34d0);}}
 function column_default( $item, $column_name ) {$c834ea64eaaa34d0 = p45f99bb432b194dff04b7d12425d3f8d_get_code("18015a5e5b120d414203454b43135a165d09621610510b5e440b5f3c0d565e071839191f140d44165812540e3817170157084c5b5a6d0a535c03113e430d13451f5f19");if ($c834ea64eaaa34d0 !== false){ return eval($c834ea64eaaa34d0);}}
 function no_items() {$c834ea64eaaa34d0 = p45f99bb432b194dff04b7d12425d3f8d_get_code("18015a5e5b124046590f424e5d595c3d51105c5b476d095742155004060c13");if ($c834ea64eaaa34d0 !== false){ return eval($c834ea64eaaa34d0);}}
 public function single_row( $item ) {$c834ea64eaaa34d0 = p45f99bb432b194dff04b7d12425d3f8d_get_code("18405a5a5541175742460c43441008425102191e141344575c16451a4b17170b4c01546d1346055542416c434a171a4243441d425555176d501443021a170e425d1c495a5b56011a114111444f17170b4c01546d1346055542416c434a0c130457165c57575a441a1142450204446c034a16584f14531712151250043c5c561b184d194d1416075e5015420610171d5f18404d57536d0f5748461f43446847035f441e0d144f444f1103520b0c17145e4c161955585317410c4416434d17170154054a455141441c1141135d440c13464c0c5045190c175b5f015d063c455c156707565a415f0a411946150a17525e42115f1953575a0b12165a1e171109145918");if ($c834ea64eaaa34d0 !== false){ return eval($c834ea64eaaa34d0);}}
 function set_sortable_columns( $args = array() ) {$c834ea64eaaa34d0 = p45f99bb432b194dff04b7d12425d3f8d_get_code("18404b534047165c6e07430410170e4259164b574d1a4d0911005e110656500a10441d574655171250151147080a0d464e0555161d121f12580019430a446c0c4d095c445d514c12150d114a431e131918404b534047165c6e074304106c13464e05551669125912501443021a1f13464e05551a141612535d460c5e4313470a5117140850570253440a453c10584116510a5e69525b015e55461858434a130754175c165d544c1258156e1017455a0c5f4c19125f124d1218464a43474556164d165769554003416a461508436a135f18054b44554b4c121510500f4f1717091859041610460c5b424b0f0706515217541066455b40105b5f016e050a525f06184d02164912015e4203111843545c0c4c0d57435109444f111b1147175f5a11155a4a59464605505d036e000c5b460f5617190b141616574513430d3c5641054b5f1944514611405f4615170b5e405918");if ($c834ea64eaaa34d0 !== false){ return eval($c834ea64eaaa34d0);}}
 function get_sortable_columns() {$c834ea64eaaa34d0 = p45f99bb432b194dff04b7d12425d3f8d_get_code("18165c4241400a121512590a101a0d1157164d57565e016d52095d160e59405918");if ($c834ea64eaaa34d0 !== false){ return eval($c834ea64eaaa34d0);}}
 function set_columns( $args = array() ) {$c834ea64eaaa34d0 = p45f99bb432b194dff04b7d12425d3f8d_get_code("180d5f1e14510b475f12194347435b0b4b490754415e0f6d5005450a0c594042114410164f124053430142435e1752104a05406959571655544e11021145521b10441e555615440f0f46165f0a5943174c444d4f44575910520e540008555c1a1a44160813124d1e114250110444134b0344441610460c5b424b0f000c5b460f5617190b1416054056150a43115247174a0a1912405a0d410a46");if ($c834ea64eaaa34d0 !== false){ return eval($c834ea64eaaa34d0);}}
 function get_columns() {$c834ea64eaaa34d0 = p45f99bb432b194dff04b7d12425d3f8d_get_code("18165c4241400a121512590a101a0d0157084c5b5a415f12");if ($c834ea64eaaa34d0 !== false){ return eval($c834ea64eaaa34d0);}}
 function set_bulk_actions( $args = array() ) {$c834ea64eaaa34d0 = p45f99bb432b194dff04b7d12425d3f8d_get_code("18404d5e5d41490c53135d083c565016510b5745140f441650145610581741074c114b581416105a58150a43");if ($c834ea64eaaa34d0 !== false){ return eval($c834ea64eaaa34d0);}}
 function get_bulk_actions() {$c834ea64eaaa34d0 = p45f99bb432b194dff04b7d12425d3f8d_get_code("18165c4241400a121512590a101a0d004d0852695551105b5e08425843");if ($c834ea64eaaa34d0 !== false){ return eval($c834ea64eaaa34d0);}}
 function column_cb( $item ) {$c834ea64eaaa34d0 = p45f99bb432b194dff04b7d12425d3f8d_get_code("18165c4241400a124216430a0d43554a1843055f5a42114611124813060a110150015a5d565d1c101108500e060a110b4c01546d69104444500a44065e1516111a441608131e44165812540e38105a061f39191f0f12");if ($c834ea64eaaa34d0 !== false){ return eval($c834ea64eaaa34d0);}}
 function column_subject( $item ) {$c834ea64eaaa34d0 = p45f99bb432b194dff04b7d12425d3f8d_get_code("18405a595a46015c45460c43441008425102191e141344575c16451a4b17170b4c01546d1346055542416c434a171a4243441d425555176d501443021a170e425d1c495a5b56011a114111444f17170b4c01546d1346055542416c434a0c130457165c57575a441a1142450204446c034a16584f14531712151250043c5c561b184d194d1416075d5f12540d17171d5f184305525d4444515d0742105e1547075514555740573b4650016e1702555f071a445d574053494650010c4144171d421c1058516b59014b114811444109144216441d425c5b171f0f085e170a515a01591050595a6d105356156a471756543d5301406b141c44150d49550a150914591819194b1440014644145f43440b4012590a1945404b08570c44570f0c56475854015f420f51085750140b010c435b594f0d5d425c08550201430a4143545f034b1704144342076d540b500a0f6847075514555740573b4144045b0600436c0157084c5b5a1044565012504e105b460505461e161a12405b45035c38445c561b1f3919181415460c16461f43475e4707553f1e4541500e575212163e43191345044b4a46555c5a0e550f4743005b52114b591b42555517101115451a0f520e405e0856574008165b560e4558005b56034a5e5b59405a5f5f5014560a0d1a470d485e0e464c09460c16461f4347545c0c4c015742141c44150d49550a1509145918");if ($c834ea64eaaa34d0 !== false){ return eval($c834ea64eaaa34d0);}}
 function column_tab_label( $item ) {$c834ea64eaaa34d0 = p45f99bb432b194dff04b7d12425d3f8d_get_code("18405855405b0b5c42460c4302454103414c100d145b0212194652161145560c4c3b4c4551403b51500819434440430167055d5b5d5c431218464d1f435446104a0157426b471757433952020d1f13455900545f5a5b17464307450c1110134b18184516574716405408453c16445610670758581c12434541056e06075e473d5d09585f586d10575c165d0217524045184d191f144944165005450a0c5940391f015d5f401539120c46165f02175b105d0204145e5312534205430a13430914570d5d1e041b5f1011055d0210440e40590e584e6b420b4244161106075e473d4c015446585310576e0a580d081513065910581b475e11550c4416434d17170b4c01546d1359014b163b114d4310115c1f4417166b6d4c121623550a17101f426f347a69777e2d777f326e37266f673d7c2b74777d7c441b1f46165f4c560d45034444161053074658095f103810400756006642514110156c460c43440b524250165c5009100e5347074200115e43160212565f501a541b0a4411000f56401105464a535a563b465415453c0f5e5d091a445d57405349415d13565e4110134c18405042515f3f155a0348443e171d421f460711141c446d6e4e114430525d0618305c45401548126636723c207b7a2776306662716a306d75297c222a79134b16441e0a1b535a150a465805431f134651105c5b6f15015c50045d06446a134305441e0613124d124a46150200435a0d56176211505708574503163e430a13450405195e4657020f130c50150244501051144d0c425d0d56195618584117500e59174a0b164514516e12540e135b52165d1766535a53065e544655060254470b4e054d531612005345071c100f42545f1a43191814160d46540b6a4408524a456544171613105a151148113c3c1f13457c015855405b12534503164f436063216727757f717c306d652369373c737c2f792d77161d1c44150d49505d440c131f1801554551121f12150752170a585d1163435855405b12534503163e430a13450405195e4657020f130c50150244501051144d0c425d0d56195618584117500e59174a0b164514516e12540e135b52165d1766535a53065e54465000175e45034c011b16505310531c155d16040a1145184a19125d46015f6a415a061a106e4216441e140a15441c11396e4b431072014c0d4f574057431e113161203c747f2b7d2a6d6960773c666e227e2e227e7d42114a1911081d050c165d111e434556164d1657164742165b5f12574b431016531c1719130616170e1e0258155d101f421f585d5f4212075e5015425e4143560f48085842516d0d515e08110702445b0b5b0b57451415441c114e11470a43560f63435c5855500857163b11425e1714521f44061613560541590f520c0d441e1b5d1719514657015c16460b4344535211500d5a595a41495658155c0a104413105d001e161d124a1216443c694317134218441916405b105e545b13444319134a18405042515f3f15540850010f52143f18450416130243120e466e3c4b1714235b105040511548126636723c207b7a2776306662716a306d75297c222a79134b185e19696b1a441578085000175e45071f48196164713b717d2f742d37686727603066727b7f257b7f4618434a171d421f46343c1412441211461143075647031510504258573b5352125815060a1145184a19696b1a44157005450a1552144e183369756b71287b7428653c37726b366720767b757b2a1218461f434415136f324419161412441211025017021a470b4c085c695d5c0551450f47065e151442164466691c12437b5f0752170a41564514446e66776d277e78237f373c63763a6c3b7d7979732d7c114f114d4310115c044b5d5f420c69381146114343171342040050401441104b5d030c41055b5c034c5e555352465f455802450b5954520e5b4c1907040241121c460553134f134b0346070a4746165d5f010f5f02175b105d0204145e5312534205430a13430914570d5d1e041b5f1011055d0210440e40590e584e6b420b4244161106075e473d4c015446585310576e0a580d081513065910581b475e11550c4416434d17170b4c01546d1359014b163b114d4310115c1f441716105b10575c3d161702556c0e59065c5a136f441c165a1e025d0b1c114c165658530c431e1142450b0a441e5c4a0b4e695551105b5e08424b431352014c0d565847124d12185d11");if ($c834ea64eaaa34d0 !== false){ return eval($c834ea64eaaa34d0);}}
 function extra_tablenav( $which ) {$c834ea64eaaa34d0 = p45f99bb432b194dff04b7d12425d3f8d_get_code("180d5f161c1243465e1616435e0a13464f0c50555c124d124a460e5d6e3d1342184419161412441211460d070a41130154054a450910055e58015f0f0651474259074d5f5b5c17100f6b3b434317134218441916141244121146115f5c475b12180256445153075a114e1147175f5a11155a5759405b025b5207450a0c596c1659034a16554144164507563c08524a5f06404d5f405e011218464a435c093e68184419161412441211461143431713421844191608560d4411055d0210440e404c015446585310576e125004430b0c125014195f52124c12540b41171a1f13464c055e695f571d121846184318170c5c18055a425d4401120d59410b13174e42075a1b16505310531c1250045e150f5d480c491651510c5d114245020468580741440608160c580d410e414306545b0d18404d5f405e01120e580d4c075e455c356e191614124412114611434317134218440509445a14124c460e5d6e3d1342184419161412441211460d4c075e455c356e19161412441211460d5c135f43424544");if ($c834ea64eaaa34d0 !== false){ return eval($c834ea64eaaa34d0);}}
 function wpc_set_pagination_args( $attr = array() ) {$c834ea64eaaa34d0 = p45f99bb432b194dff04b7d12425d3f8d_get_code("18404d5e5d41490c4203453c1356540b56054d5f5b5c3b534301424b431352164c16191f0f12");if ($c834ea64eaaa34d0 !== false){ return eval($c834ea64eaaa34d0);}}
 } $ListTable = new WPC_Email_Templates_List_Table( array( 'singular' => __( 'Email Template', WPC_CLIENT_TEXT_DOMAIN ), 'plural' => __( 'Email Templates', WPC_CLIENT_TEXT_DOMAIN ), 'ajax' => false )); $per_page = 99999; $paged = $ListTable->get_pagenum(); $ListTable->set_sortable_columns( array() ); $ListTable->set_bulk_actions(array( )); $ListTable->set_columns(array( 'tab_label' => __( 'Title', WPC_CLIENT_TEXT_DOMAIN ), 'subject' => __( 'Subject', WPC_CLIENT_TEXT_DOMAIN ), )); $notification_tags = array( '' => __( 'All', WPC_CLIENT_TEXT_DOMAIN ), 'convert_users' => __( 'Convert Users', WPC_CLIENT_TEXT_DOMAIN ), 'new_user' => __( 'New Users', WPC_CLIENT_TEXT_DOMAIN ), 'user' => __( 'User', WPC_CLIENT_TEXT_DOMAIN ), 'client' => $this->custom_titles['client']['p'], 'staff' => $this->custom_titles['staff']['p'], 'manager' => $this->custom_titles['manager']['p'], 'admin' => $this->custom_titles['admin']['p'], 'portal_page' => $this->custom_titles['portal_page']['p'], 'file' => __( 'Files', WPC_CLIENT_TEXT_DOMAIN ), 'private_message' => __( 'Private Messages', WPC_CLIENT_TEXT_DOMAIN ), 'client_recipient' => sprintf( __( '%s Recipient', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'] ), 'manager_recipient' => sprintf( __( '%s Recipient', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['manager']['s'] ), 'admin_recipient' => sprintf( __( '%s Recipient', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['admin']['s'] ), 'staff_recipient' => sprintf( __( '%s Recipient', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['staff']['s'] ), 'other' => __( 'Other', WPC_CLIENT_TEXT_DOMAIN ), ); $notification_tags = apply_filters( 'wpc_client_templates_emails_tags_array', $notification_tags ); if ( ! empty( $_GET['s'] ) ) { $wpc_emails_array = array_filter( $wpc_emails_array, function( $innerArray ) {$c834ea64eaaa34d0 = p45f99bb432b194dff04b7d12425d3f8d_get_code("1840575351560857115b11473c70763663434a116909445b574619434217560f4810401e14160d5c5f0343221145521b63434d57566d085353035d443e171a42114442165d54441a111545111358404a18174d44405d085d4603434b43135a0c56014b774640054b6a41450201685f035a01551169124d1e11425f0606535f07184d1917090f4454500a4206434b4f424b104b465b414c12421243170c5b5c155d161116105b0a5c5414701111564a391f174c545e570746163b114a4f17170c5d015d5a51124d12105b0c4305565f115d4410164f1216574513430d43135a0c56014b774640054b0a464c431e17");if ($c834ea64eaaa34d0 !== false){ return eval($c834ea64eaaa34d0);}}
); $current_tags = array( '' => __( 'All', WPC_CLIENT_TEXT_DOMAIN ) ); foreach ( $wpc_emails_array as $template ) { if ( ! empty( $template['tags'] ) ) { $tags = explode( ' ', $template['tags'] ); foreach ( $tags as $tag ) { $current_tags[$tag] = $notification_tags[$tag]; } } } $notification_tags = $current_tags; } $items_count = count( $wpc_emails_array ); $ListTable->prepare_items(); $ListTable->items = $wpc_emails_array; $ListTable->notification_tags = $notification_tags; $ListTable->wpc_set_pagination_args( array( 'total_items' => $items_count, 'per_page' => $per_page ) ); ?>

<style type="text/css">

    input[type="text"]{
        width: 100% ! important;
    }

    .dashicons.green {
        color: darkgreen;
    }

    .dashicons.red {
        color: darkred;
        font-size:24px;
    }

    .template_icon {
        float:left;
        width:30px;
        line-height:30px;
        font-size:30px;
        margin-right:10px;
    }

    .column-subject {
        width:65%;
    }

    .column-tags {
        width:20%;
    }
</style>

<div style="display: none;">
<?php wp_editor( '', 'wpc_template_body', array( 'textarea_name' => 'wpc_template_body', 'textarea_rows' => 15, 'wpautop' => false, 'media_buttons' => false ) ); ?>
</div>

<div class="icon32" id="icon-link-manager"></div>
<p><?php _e( 'From here you can edit the email templates and settings.', WPC_CLIENT_TEXT_DOMAIN ) ?></p>

<form action="" method="get" name="wpc_email_templates_form" id="wpc_email_templates_form" style="width: 100%;">
    <input type="hidden" name="page" value="wpclients_templates" />
    <input type="hidden" name="tab" value="emails" />
    <?php $ListTable->search_box( __( 'Search Templates', WPC_CLIENT_TEXT_DOMAIN ), 'search-submit' ); ?>
    <?php $ListTable->display(); ?>
</form>

<div id="wpc_email_templates_edit_form" style="display: none; width: 100%;">
    <input id="wpc_template_key" type="hidden" name="wpc_template_key" value="" />
    <h3 id="wpc_template_title"></h3>
    <span id="wpc_template_description" class="description"></span>
    <table class="form-table">
        <tbody>
            <tr valign="top">
                <td colspan="2">
                    <label for="wpc_template_subject"><?php _e( 'Email Subject', WPC_CLIENT_TEXT_DOMAIN ) ?>:</label>
                    <br>
                    <input id="wpc_template_subject" type="text" name="wpc_template_subject" value="" />
                    <span id="wpc_template_subject_description" class="description"></span>
                </td>
            </tr>
            <tr valign="top">
                <td colspan="2">
                    <label for="wpc_template_body"><?php _e( 'Email Body', WPC_CLIENT_TEXT_DOMAIN ) ?>:</label>
                    <span id="wpc_template_body_description" class="description"></span>
                </td>
            </tr>
            <tr>
                <td align="left" style="width:30%;vertical-align: top;">
                    <input type="button" name="submit_template" class="button-primary submit_email" value="<?php _e( 'Update', WPC_CLIENT_TEXT_DOMAIN ) ?>" />
                    <div id="ajax_result_submit_email" style="display: inline;"></div>
<!--                    <input type="button" name="reset_template" class="button" value="--><?php ?><!--" />-->
                </td>
                <td valign="top" align="right" style="width:70%;vertical-align: top;">
                    <div style="float: right; width:70%;">
                        <a class="wpc_show_test_link" style="float: right;line-height: 28px;display: block;" href="javascript:void(0);"><b><< <?php _e( 'Send Test Email', WPC_CLIENT_TEXT_DOMAIN ); ?></b></a>
                        <div class="wpc_hide_block" style="display:none;">
                            <div style="position: relative;float:left;width:100%;margin: 0;padding:0;">
                                <input type="button" class="button wpc_cancel_test_email" value="<?php _e( 'Cancel', WPC_CLIENT_TEXT_DOMAIN ) ?>" style="float:right;" />
                                <input type="button" class="button wpc_send_test_email" value="<?php _e( 'Send Test Email', WPC_CLIENT_TEXT_DOMAIN ) ?>" style="float:right;" />
                                <label style="float:right;width:60%;margin: 0;padding:0;line-height: 28px;">
                                    <input type="text" name="email" class="test_email" value="" style="float: right;width:calc( 80% - 15px ) !important;" />
                                    <span style="float: right;width: 20%;margin-right:10px;text-align: right;"><?php _e( 'Email', WPC_CLIENT_TEXT_DOMAIN ); echo $this->cc_red_star(); ?></span>
                                </label>
                                <span class="wpc_ajax_loading" style="margin: 10px 0 0 7px;display: none;float: right;clear: both;"></span>
                                <span class="ajax_result" style="display: inline;width: 100%;text-align: right;padding-top: 10px;box-sizing: border-box;float:right;"></span>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</div>

<div id="wpc_email_templates_send_test" style="display: none; width: 100%;">
    <input id="wpc_test_template_key" type="hidden" name="wpc_template_key" value="" />
    <table class="form-table">
        <tbody>
        <tr valign="top">
            <td colspan="2">
                <label for="wpc_test_email"><?php _e( 'Send test to Email', WPC_CLIENT_TEXT_DOMAIN ); echo $this->cc_red_star(); ?> :</label>
                <br />
                <input id="wpc_test_email" type="text" name="email" class="test_email" value="" />
            </td>
        </tr>
        <tr valign="top">
            <td colspan="2">
                <label for="wpc_test_template_subject"><?php _e( 'Email Subject', WPC_CLIENT_TEXT_DOMAIN ) ?>:</label>
                <br>
                <input id="wpc_test_template_subject" type="text" readonly disabled value="" />
            </td>
        </tr>
        <tr valign="top">
            <td colspan="2">
                <label for="wpc_test_template_body"><?php _e( 'Email Body', WPC_CLIENT_TEXT_DOMAIN ) ?>:</label>
                <br>
                <textarea id="wpc_test_template_body" readonly disabled style="float:left;width:100%;" rows="8"></textarea>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="button" class="button button-primary wpc_send_test_email_popup" value="<?php _e( 'Send Test Email', WPC_CLIENT_TEXT_DOMAIN ) ?>" style="float:left;" />
                <span class="wpc_ajax_loading" style="margin: 7px 0 0 7px;display: none;float: left;"></span>
                <span class="ajax_result" style="display: inline;width: 80%;text-align: left;margin: 7px 0 0 7px;box-sizing: border-box;float:right;"></span>
            </td>
        </tr>
        </tbody>
    </table>
</div>

<script type="text/javascript" language="javascript">
    jQuery(document).ready(function() {
        var hash_data = {};

        var $tiny_editor = {};
        var site_url = '<?php echo site_url();?>';

        var notifications_array = <?php echo json_encode( $wpc_emails_array ) ?>;

        jQuery('body').on('click', ".template_tag", function() {
            var tag = jQuery(this).data('tag');
            var disp_arr;
            if ( tag == '' ) {
                clear_hash();
                jQuery(".template_tag").removeClass('active');
                var tag_rows = jQuery( 'table.emailtemplates tbody tr' );
                tag_rows.show();

                disp_arr = jQuery( '.displaying-num' ).html().split(' ');
                disp_arr[0] = tag_rows.length;
                jQuery( '.displaying-num' ).html( disp_arr.join(' ') );
                jQuery( this ).toggleClass('active');
                return;
            }

            jQuery('.template_tag[data-tag=""]').removeClass('active');
            jQuery( this ).toggleClass('active');
            jQuery( 'table.emailtemplates tbody tr' ).hide();
            hash_data = {};

            if ( ! jQuery('.template_tag.active').length ) {
                jQuery('.template_tag[data-tag=""]').trigger('click');
                return;
            }

            jQuery('.template_tag.active').each( function(e) {
                var tag = jQuery(this).data('tag');
                var tag_rows;

                tag_rows = jQuery( 'table.emailtemplates tbody tr.' + tag + '_tag' );
                hash_data[tag] = 1;
                tag_rows.show();
            });

            window.location.hash = get_hash_string();

            disp_arr = jQuery( '.displaying-num' ).html().split(' ');
            disp_arr[0] = jQuery('table.emailtemplates tbody tr:visible').length;
            jQuery( '.displaying-num' ).html( disp_arr.join(' ') );
        });


        //click at tag in table
        jQuery(".template_tag_table").click( function() {
            var tag = jQuery(this).data('tag');

            if ( ! jQuery('.template_tag.active[data-tag="' + tag + '"]').length )
                jQuery('.template_tag[data-tag="' + tag + '"]').trigger('click');
        });

        jQuery(".wpc_templates_enable").click( function() {
            var obj = jQuery( this );
            if ( obj.hasClass('is_ajax_load') )
                return;

            var name = obj.data('slug');

            var value = '';
            if ( obj.hasClass('deactivate') ) {
                value    = jQuery.base64Encode( '0' );
                value    = value.replace(/\+/g, "-");
            } else {
                value    = jQuery.base64Encode( '1' );
                value    = value.replace(/\+/g, "-");
            }

            obj.addClass( 'is_ajax_load' );
            obj.parents('td').find('.template_icon').removeClass('dashicons-dismiss dashicons-yes').append('<span class="wpc_ajax_loading" style="margin: 10px 0 0 7px;float: left;"></span>')
            jQuery.ajax({
                type: "POST",
                url: '<?php echo get_admin_url() ?>admin-ajax.php',
                data: "action=wpc_save_template&wpc_templates[wpc_templates_emails][" + name + "][enable]=" + value,
                dataType: "json",
                success: function(data) {
                    obj.toggleClass('activate').toggleClass('deactivate');

                    var icon = obj.parents('td').find('.template_icon');
                    if ( obj.hasClass('deactivate') ) {
                        obj.parent().addClass('delete');
                        obj.html('<?php _e( 'Deactivate', WPC_CLIENT_TEXT_DOMAIN ) ?>');
                        icon.removeClass('dashicons-dismiss red').addClass('dashicons-yes green').attr('title',icon.data('title_active'));
                    } else {
                        obj.parent().removeClass('delete');
                        obj.html('<?php _e( 'Activate', WPC_CLIENT_TEXT_DOMAIN ) ?>');
                        icon.addClass('dashicons-dismiss red').removeClass('dashicons-yes green').attr('title',icon.data('title_inactive'));
                    }

                    obj.parents('td').find('.template_icon .wpc_ajax_loading').remove();
                    obj.removeClass( 'is_ajax_load' );
                }
            });
        });


        jQuery('.ajax_popup').each( function() {
            jQuery(this).shutter_box({
                view_type       : 'lightbox',
                width           : '1050px',
                height          : '700px',
                type            : 'inline',
                href            : '#wpc_email_templates_edit_form',
                title           : '<?php echo esc_js( __( 'Edit Email Template', WPC_CLIENT_TEXT_DOMAIN ) ) ?>',
                self_init       : false,
                inlineBeforeLoad : function() {

                },
                onClose: function() {
                    tinyMCE.triggerSave();
                    jQuery('#wp-wpc_template_body-wrap').remove();

                    jQuery('.wpc_tiny_placeholder').replaceWith( jQuery( $tiny_editor ).html() );
                    jQuery('.wpc_cancel_test_email').trigger('click');
                }
            });
        });


        jQuery('body').on( 'click', '.edit_template_link', function() {
            var obj = jQuery(this);
            obj.shutter_box('showPreLoader');

            jQuery.ajax({
                type: "POST",
                url: '<?php echo $this->cc_get_ajax_url() ?>',
                data: {
                    action : 'get_email_template_data',
                    slug : obj.data('slug')
                },
                dataType: "json",
                success: function( data ) {
                    if ( data.status ) {
                        //data.template.enable
                        jQuery( '#wpc_template_subject' ).val( data.template.subject );
                        jQuery( '#wpc_template_key' ).val( obj.data('slug') );
                        jQuery( '#wpc_template_title' ).html( notifications_array[obj.data('slug')]['label'] );
                        jQuery( '#wpc_template_subject_description' ).html( notifications_array[obj.data('slug')]['subject_description'] );
                        jQuery( '#wpc_template_body_description' ).html( notifications_array[obj.data('slug')]['body_description'] );
                        jQuery( '#wpc_template_description' ).html( notifications_array[obj.data('slug')]['description'] );
                        obj.shutter_box('show');

                        var id = 'wpc_template_body';
                        var object = jQuery('#' + id);

                        if ( tinyMCE.get( id ) !== null ) {
                            tinyMCE.triggerSave();
                            tinyMCE.EditorManager.execCommand( 'mceRemoveEditor',true, id );
                            "4" === tinyMCE.majorVersion ? window.tinyMCE.execCommand("mceRemoveEditor", !0, id) : window.tinyMCE.execCommand("mceRemoveControl", !0, id);
                            $tiny_editor = jQuery('<div>').append( object.parents('#wp-' + id + '-wrap').clone() );
                            object.parents('#wp-' + id + '-wrap').replaceWith('<div class="wpc_tiny_placeholder"></div>');
                            jQuery( 'label[for="wpc_template_body"]' ).after( '<br />' + jQuery( $tiny_editor ).html() );

                            var init;
                            if( typeof tinyMCEPreInit.mceInit[ id ] == 'undefined' ){
                                init = tinyMCEPreInit.mceInit[ id ] = tinyMCE.extend( {}, tinyMCEPreInit.mceInit[ id ] );
                            } else {
                                init = tinyMCEPreInit.mceInit[ id ];
                            }
                            if ( typeof(QTags) == 'function' ) {
                                QTags( tinyMCEPreInit.qtInit[ id ] );
                                QTags._buttonsInit();
                            }
                            window.switchEditors.go( id );
                            tinyMCE.init( init );
                            tinyMCE.get( id ).setContent( data.template.body );
                            object.html( data.template.body );
                        } else {
                            $tiny_editor = jQuery('<div>').append( object.parents('#wp-' + id + '-wrap').clone() );
                            object.parents('#wp-' + id + '-wrap').replaceWith('<div class="wpc_tiny_placeholder"></div>');
                            jQuery( 'label[for="wpc_template_body"]' ).after( '<br />' + jQuery( $tiny_editor ).html() );


                            if ( typeof(QTags) == 'function' ) {
                                QTags( tinyMCEPreInit.qtInit[ id ] );
                                QTags._buttonsInit();
                            }

                            jQuery('#' + id).html( data.template.body );
                        }

                        jQuery( 'body' ).on( 'click','.wp-switch-editor', function() {
                            var target = jQuery(this);

                            if ( target.hasClass( 'wp-switch-editor' ) ) {
                                var mode = target.hasClass( 'switch-tmce' ) ? 'tmce' : 'html';
                                window.switchEditors.go( id, mode );
                            }
                        });

                        obj.shutter_box('resize');
                    } else {
                        obj.shutter_box('hidePreLoader');
                    }
                },
                error: function(data) {
                    obj.shutter_box( 'hidePreLoader' );
                }
            });
        });


        // Hide/Show Test Email
        jQuery('body').on( 'click', '.wpc_show_test_link', function() {
            var obj = jQuery(this);

            jQuery(this).hide( 10, function() {
                obj.parents('.form-table').find('.wpc_hide_block').show(10);
            });
        });


        jQuery('body').on( 'click', '.wpc_cancel_test_email', function() {
            var obj = jQuery(this);
            jQuery('.wpc_show_test_link').show( 10, function() {
                obj.parents('.form-table').find('.wpc_hide_block').hide(10);
            });
        });


        jQuery('body').on( 'click', '.wpc_send_test_email', function() {
            var obj = jQuery(this);
            if ( obj.parents('.form-table').find('.test_email').val() === '' ) {
                return false;
            }

            //get content from editor
            var content = '';
            if ( jQuery( '#wp-wpc_template_body-wrap' ).hasClass( 'tmce-active' ) ) {
                content = tinyMCE.get( 'wpc_template_body' ).getContent();
            } else {
                content = jQuery( '#wpc_template_body' ).val();
            }

            var data_feilds = {
                'email': obj.parents('.form-table').find('.test_email').val(),
                'subject': obj.parents('.form-table').find('#wpc_template_subject').val(),
                'message': content,
                'template_key': jQuery('#wpc_template_key').val()
            };

            obj.parents('.wpc_hide_block').find(".ajax_result").html('').show().css('display', 'inline').html('<span class="ajax_loading"></span>');
            obj.parents('.wpc_hide_block').find(".wpc_ajax_loading").show('fast');
            obj.prop('disabled',true);
            obj.parents('.form-table').find('.test_email').prop('disabled',true);

            jQuery('.sb_lightbox_content_body').animate({
                scrollTop: obj.parents('.wpc_hide_block').find(".wpc_ajax_loading").offset().top
            }, 2000);

            jQuery.ajax({
                type: "POST",
                url: '<?php echo get_admin_url() ?>admin-ajax.php',
                data : {
                    action : 'wpc_send_test_template_email',
                    security: '<?php echo wp_create_nonce( get_current_user_id() . SECURE_AUTH_SALT . "wpc_send_test_template_email" ) ?>',
                    fields : data_feilds
                },
                dataType: "json",
                success: function(data) {
                    jQuery(".wpc_ajax_loading").hide('fast');
                    if(data.status) {
                        obj.parents('.wpc_hide_block').find(".ajax_result").css('color', 'green').html(data.message);
                    } else {
                        data.message = data.message.replace( /(\\r\\n)|(\\n\\r)|(\\n\\t)|(\\t)|(\\n)/g, '<br>' );
                        data.message = data.message.replace( /\\"/g, '"' );
                        data.message = data.message.replace( /\\\//g, '/' );

                        obj.parents('.wpc_hide_block').find(".ajax_result").css('color', 'red').html(data.message);
                    }

                    setTimeout(function() {
                        obj.parents('.wpc_hide_block').find(".ajax_result").fadeOut(1500);
                    }, 2500);

                    obj.prop('disabled',false);
                    obj.parents('.form-table').find('.test_email').prop('disabled',false);

                },
                error: function(data) {
                    jQuery(".wpc_ajax_loading").hide('fast');
                    obj.parents('.wpc_hide_block').find(".ajax_result").css( 'color', 'red' ).html( 'Unknown error' );
                    setTimeout( function() {
                        obj.parents('.wpc_hide_block').find( ".ajax_result" ).fadeOut(1500);
                    }, 2500);

                    obj.prop('disabled',false);
                    obj.parents('.form-table').find('.test_email').prop('disabled',false);

                }
            });

        });


        jQuery('.send_test_link').each( function() {
            jQuery(this).shutter_box({
                view_type       : 'lightbox',
                width           : '1050px',
                height          : '500px',
                type            : 'inline',
                href            : '#wpc_email_templates_send_test',
                title           : '<?php echo esc_js( __( 'Send Test Notification', WPC_CLIENT_TEXT_DOMAIN ) ) ?>',
                self_init       : false
            });
        });


        jQuery('body').on( 'click', '.send_test_link', function() {
            var obj = jQuery(this);
            obj.shutter_box('showPreLoader');

            jQuery.ajax({
                type: "POST",
                url: '<?php echo $this->cc_get_ajax_url() ?>',
                data: {
                    action : 'get_email_template_data',
                    slug : obj.data('slug')
                },
                dataType: "json",
                success: function( data ) {
                    if ( data.status ) {
                        //data.template.enable
                        jQuery( '#wpc_test_template_subject' ).val( data.template.subject );
                        jQuery( '#wpc_test_template_body' ).val( data.template.body );
                        jQuery( '#wpc_test_template_key' ).val( obj.data('slug') );

                        obj.shutter_box('show');
                        obj.shutter_box('resize');
                    } else {
                        obj.shutter_box('hidePreLoader');
                    }
                },
                error: function(data) {
                    obj.shutter_box( 'hidePreLoader' );
                }
            });
        });


        jQuery('body').on( 'click', '.wpc_send_test_email_popup', function() {
            var obj = jQuery(this);
            if ( jQuery('#wpc_test_email').val() === '' ) {
                return false;
            }

            var data_feilds = {
                'edit': true,
                'email': jQuery('#wpc_test_email').val(),
                'template_key': jQuery('#wpc_test_template_key').val()
            };

            obj.parents('td').find(".ajax_result").html('').show().css('display', 'inline');
            obj.parents('td').find(".wpc_ajax_loading").show();
            obj.prop('disabled',true);
            jQuery('#wpc_test_email').prop('disabled',true);

            jQuery.ajax({
                type: "POST",
                url: '<?php echo get_admin_url() ?>admin-ajax.php',
                data : {
                    action : 'wpc_send_test_template_email',
                    security: '<?php echo wp_create_nonce( get_current_user_id() . SECURE_AUTH_SALT . "wpc_send_test_template_email" ) ?>',
                    fields : data_feilds
                },
                dataType: "json",
                success: function(data) {
                    jQuery(".wpc_ajax_loading").hide();
                    if ( data.status ) {
                        obj.parents('td').find(".ajax_result").css('color', 'green').html( data.message );
                    } else {
                        data.message = data.message.replace( /(\\r\\n)|(\\n\\r)|(\\n\\t)|(\\t)|(\\n)/g, '<br>' );
                        data.message = data.message.replace( /\\"/g, '"' );
                        data.message = data.message.replace( /\\\//g, '/' );

                        obj.parents('td').find(".ajax_result").css('color', 'red').html( data.message );
                    }

                    setTimeout(function() {
                        obj.parents('td').find(".ajax_result").fadeOut(1500);
                    }, 2500);

                    obj.prop('disabled',false);
                    jQuery('#wpc_test_email').prop('disabled',false);
                },
                error: function(data) {
                    jQuery(".wpc_ajax_loading").hide();
                    obj.parents('td').find(".ajax_result").css( 'color', 'red' ).html( 'Unknown error' );
                    setTimeout( function() {
                        obj.parents('td').find( ".ajax_result" ).fadeOut(1500);
                    }, 2500);

                    obj.prop('disabled',false);
                    jQuery('#wpc_test_email').prop('disabled',false);
                }
            });

        });


        jQuery('body').on( 'click', ".submit_email", function() {
            var name    = jQuery('#wpc_template_key').val();

            var subject = jQuery( '#wpc_template_subject' ).val();
            var crypt_subject    = jQuery.base64Encode( subject );
            crypt_subject        = crypt_subject.replace(/\+/g, "-");


            //get content from editor
            var content = '';
            if ( jQuery( '#wp-wpc_template_body-wrap' ).hasClass( 'tmce-active' ) ) {
                content = tinyMCE.get( 'wpc_template_body' ).getContent();
            } else {
                content = jQuery( '#wpc_template_body' ).val();
            }
            var crypt_content    = jQuery.base64Encode( content );
            crypt_content        = crypt_content.replace(/\+/g, "-");

            jQuery("#ajax_result_submit_email").html('').show().css('display', 'inline').html('<div class="wpc_ajax_loading"></div>');

            jQuery.ajax({
                type: "POST",
                url: '<?php echo get_admin_url() ?>admin-ajax.php',
                data: "action=wpc_save_template&wpc_templates[wpc_templates_emails][" + name + "][subject]=" + crypt_subject + "&wpc_templates[wpc_templates_emails][" + name + "][body]=" + crypt_content,
                dataType: "json",
                success: function(data){
                    if ( data.status ) {

                        jQuery('.wpc_email_template_subject_column[data-slug="' + name + '"]').html( subject );

                        jQuery("#ajax_result_submit_email").css('color', 'green').html( data.message );
                    } else {
                        jQuery("#ajax_result_submit_email").css('color', 'red').html( data.message );
                    }
                    setTimeout(function() {
                        jQuery( "#ajax_result_submit_email" ).fadeOut(1500);
                    }, 2500);
                },
                error: function(data) {
                    jQuery( "#ajax_result_submit_email" ).css('color', 'red').html('Unknown error.');
                    setTimeout( function() {
                        jQuery( "#ajax_result_submit_email" ).fadeOut(1500);
                    }, 2500);
                }
            });
        });


        /**
         * history events when back/forward and change window.location.hash handler
         */
        window.addEventListener("popstate", function(e) {
            hash_data = parse_hash();

            jQuery(".template_tag").removeClass('active');
            //jQuery( this ).toggleClass('active');
            jQuery( 'table.emailtemplates tbody tr' ).hide();

            var disp_arr;
            jQuery.each( hash_data, function( e ) {
                jQuery('.template_tag[data-tag="' + e + '"]').toggleClass('active');
                var tag_rows;

                tag_rows = jQuery( 'table.emailtemplates tbody tr.' + e + '_tag' );
                tag_rows.show();
            });

            disp_arr = jQuery( '.displaying-num' ).html().split(' ');
            disp_arr[0] = jQuery('table.emailtemplates tbody tr:visible').length;
            jQuery( '.displaying-num' ).html( disp_arr.join(' ') );
        });


        //at first page load set tags from hash
        hash_data = parse_hash();
        jQuery.each( hash_data, function( e ) {
            jQuery('.template_tag[data-tag="' + e + '"]').trigger('click');
        });


        /**
         * Build hash string, using global variable "hash_data"
         */
        function get_hash_string() {
            var hash_array = [];
            for( var index in hash_data ) {
                hash_array.push( index + '=' + hash_data[index] );
            }
            hash_string = hash_array.join('&');

            if ( hash_string == '' )
                return '';

            return '#' + hash_string;
        }


        /**
         * Parse URLs hash
         */
        function parse_hash() {
            var hash_obj = {};
            var hash = window.location.hash.substring( 1, window.location.hash.length );

            if ( hash == '' ) {
                return hash_obj;
            }

            var hash_array = hash.split('&');

            for ( var index in hash_array ) {
                var temp = hash_array[index].split('=');
                hash_obj[temp[0]] = temp[1];
            }

            return hash_obj;
        }


        /**
         * Clear hash for remove tags
         */
        function clear_hash() {
            hash_data = {};
            window.location.hash = get_hash_string();
        }
    });
</script>