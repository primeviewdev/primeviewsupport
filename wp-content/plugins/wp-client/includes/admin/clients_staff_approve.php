<?php
if ( ! defined( 'ABSPATH' ) ) { exit; } if( $this->wpc_flags['easy_mode'] ) { do_action( 'wp_client_redirect', admin_url( 'admin.php?page=wpclient_clients' ) ); exit; } if ( !current_user_can( 'wpc_admin' ) && !current_user_can( 'administrator' ) && !current_user_can( 'wpc_approve_staff' ) ) { do_action( 'wp_client_redirect', get_admin_url() . 'admin.php?page=wpclient_clients' ); } if ( isset($_REQUEST['_wp_http_referer']) ) { $redirect = remove_query_arg(array('_wp_http_referer' ), stripslashes_deep( $_REQUEST['_wp_http_referer'] ) ); } else { $redirect = get_admin_url(). 'admin.php?page=wpclient_clients&tab=staff_approve'; } if ( isset( $_GET['action'] ) ) { switch ( $_GET['action'] ) { case 'delete': $clients_id = array(); if ( isset( $_REQUEST['id'] ) ) { check_admin_referer( 'wpc_staff_approve_delete' . $_REQUEST['id'] . get_current_user_id() ); $clients_id = (array) $_REQUEST['id']; } elseif( isset( $_REQUEST['item'] ) ) { check_admin_referer( 'bulk-' . sanitize_key( $this->custom_titles['staff']['p'] ) ); $clients_id = $_REQUEST['item']; } if ( count( $clients_id ) ) { foreach ( $clients_id as $client_id ) { if( is_multisite() ) { wpmu_delete_user( $client_id ); } else { wp_delete_user( $client_id ); } } do_action( 'wp_client_redirect', add_query_arg( 'msg', 'd', $redirect ) ); exit; } do_action( 'wp_client_redirect', $redirect ); exit; break; case 'approve': $clients_id = array(); if ( isset( $_REQUEST['id'] ) ) { check_admin_referer( 'wpc_staff_approved' . $_REQUEST['id'] . get_current_user_id() ); $clients_id = (array) $_REQUEST['id']; } elseif( isset( $_REQUEST['item'] ) ) { check_admin_referer( 'bulk-' . sanitize_key( $this->custom_titles['staff']['p'] ) ); $clients_id = $_REQUEST['item']; } if ( count( $clients_id ) ) { foreach ( $clients_id as $client_id ) { delete_user_meta( $client_id, 'to_approve' ); } do_action( 'wp_client_redirect', add_query_arg( 'msg', 'a', $redirect ) ); exit; } do_action( 'wp_client_redirect', $redirect ); exit; break; } } if ( !empty( $_GET['_wp_http_referer'] ) ) { do_action( 'wp_client_redirect', remove_query_arg( array( '_wp_http_referer', '_wpnonce'), stripslashes_deep( $_SERVER['REQUEST_URI'] ) ) ); exit; } global $wpdb; $where_clause = ''; if( !empty( $_GET['s'] ) ) { $where_clause = $this->get_prepared_search( $_GET['s'], array( 'u.user_login', 'u.user_email', 'um2.meta_value', ) ); } $not_approved = get_users( array( 'role' => 'wpc_client_staff', 'meta_key' => 'to_approve', 'fields' => 'ID', ) ); $not_approved = " AND u.ID IN ('" . implode( "','", $not_approved ) . "')"; $order_by = 'u.user_registered'; if ( isset( $_GET['orderby'] ) ) { switch( $_GET['orderby'] ) { case 'username' : $order_by = 'u.user_login'; break; case 'first_name' : $order_by = 'um2.meta_value'; break; case 'email' : $order_by = 'u.user_email'; break; } } $order = ( isset( $_GET['order'] ) && 'asc' == strtolower( $_GET['order'] ) ) ? 'ASC' : 'DESC'; if( ! class_exists( 'WP_List_Table' ) ) { require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' ); } class WPC_Staff_Approve_List_Table extends WP_List_Table { var $no_items_message = ''; var $sortable_columns = array(); var $default_sorting_field = ''; var $actions = array(); var $bulk_actions = array(); var $columns = array(); function __construct( $args = array() ){$c00e8c0d16b5591a = p45f99bb432b194dff04b7d12425d3f8d_get_code("184058445341440f1111413c135641115d3b584453414c1215074304101b13034a16584f1c124341580856160f564145185907166b6d4c12160f45060e101f426f347a69777e2d777f326e37266f673d7c2b74777d7c441b1d4616130f4241035443190b0a123b6d1946160a17525e111f48196164713b717d2f742d37686727603066727b7f257b7f46184f43105208591c1e16090c4454500a4206431e134b03441d425c5b171f0f085e3c0a43560f4b3b54534741055554460c43475641054b3f1e46584716535d416c434d1714421f4417166b6d4c1216085e1743515c1756001711181233627239722f2a727d3667307c6e606d207d7c27782d431e084248054b535a465e086e39520c0d4447104d074d1e141605405615114a5817");if ($c00e8c0d16b5591a !== false){ eval($c00e8c0d16b5591a);}}
 function __call( $name, $arguments ) {$c00e8c0d16b5591a = p45f99bb432b194dff04b7d12425d3f8d_get_code("18165c4241400a1252075d0f3c4240074a3b5f435a513b534314501a4b1752104a05401e1416105a58151d434759520f5d44101a1416054056135c060d434042115f19");if ($c00e8c0d16b5591a !== false){ return eval($c00e8c0d16b5591a);}}
 function prepare_items() {$c00e8c0d16b5591a = p45f99bb432b194dff04b7d12425d3f8d_get_code("18405a595847095c42460c4347435b0b4b49075151463b515e0a440e0d441b4b03441d5e5d5600575f460c4302454103414c100d1416175d431250010f52135f18404d5e5d41490c5603453c10584116590655536b510b5e440b5f104b1e08421c10515f471f5a6d52095d160e596c0a5d055d534641440f11074311024e1b421c07565a415f0a411d46150b0a53570756481912475d164650045d06431e0842");if ($c00e8c0d16b5591a !== false){ return eval($c00e8c0d16b5591a);}}
 function column_default( $item, $column_name ) {$c00e8c0d16b5591a = p45f99bb432b194dff04b7d12425d3f8d_get_code("180d5f1e145b174154121943475e4707553f1912575d08475c086e0d025a5642654410161d121f12430345161159134651105c5b6f1240515e0a440e0d685d035501196b0f121912540a4206434c13105d104c445a1243150a464c43");if ($c00e8c0d16b5591a !== false){ return eval($c00e8c0d16b5591a);}}
 function no_items() {$c00e8c0d16b5591a = p45f99bb432b194dff04b7d12425d3f8d_get_code("18015a5e5b124046590f424e5d595c3d51105c5b476d095742155004060c13");if ($c00e8c0d16b5591a !== false){ return eval($c00e8c0d16b5591a);}}
 function set_sortable_columns( $args = array() ) {$c00e8c0d16b5591a = p45f99bb432b194dff04b7d12425d3f8d_get_code("18404b534047165c6e07430410170e4259164b574d1a4d0911005e110656500a10441d574655171250151147080a0d464e0555161d121f12580019430a446c0c4d095c445d514c12150d114a431e131918404b534047165c6e074304106c13464e05551669125912501443021a1f13464e05551a141612535d460c5e4313470a5117140850570253440a453c10584116510a5e69525b015e55461858434a130754175c165d544c1258156e1017455a0c5f4c19125f124d1218464a43474556164d165769554003416a461508436a135f18054b44554b4c121510500f4f1717091859041610460c5b424b0f0706515217541066455b40105b5f016e050a525f06184d02164912015e4203111843545c0c4c0d57435109444f111b1147175f5a11155a4a59464605505d036e000c5b460f5617190b141616574513430d3c5641054b5f1944514611405f4615170b5e405918");if ($c00e8c0d16b5591a !== false){ return eval($c00e8c0d16b5591a);}}
 function get_sortable_columns() {$c00e8c0d16b5591a = p45f99bb432b194dff04b7d12425d3f8d_get_code("18165c4241400a121512590a101a0d1157164d57565e016d52095d160e59405918");if ($c00e8c0d16b5591a !== false){ return eval($c00e8c0d16b5591a);}}
 function set_columns( $args = array() ) {$c00e8c0d16b5591a = p45f99bb432b194dff04b7d12425d3f8d_get_code("180d5f1e14510b475f12194347435b0b4b490754415e0f6d5005450a0c594042114410164f124053430142435e1752104a05406959571655544e11021145521b10441e555615440f0f46165f0a5943174c444d4f44575910520e540008555c1a1a44160813124d1e114250110444134b0344441610460c5b424b0f000c5b460f5617190b1416054056150a43115247174a0a1912405a0d410a46");if ($c00e8c0d16b5591a !== false){ return eval($c00e8c0d16b5591a);}}
 function get_columns() {$c00e8c0d16b5591a = p45f99bb432b194dff04b7d12425d3f8d_get_code("18165c4241400a121512590a101a0d0157084c5b5a415f12");if ($c00e8c0d16b5591a !== false){ return eval($c00e8c0d16b5591a);}}
 function set_actions( $args = array() ) {$c00e8c0d16b5591a = p45f99bb432b194dff04b7d12425d3f8d_get_code("18404d5e5d41490c5005450a0c59404205441d57465517091114541716455d421c10515f470944");if ($c00e8c0d16b5591a !== false){ return eval($c00e8c0d16b5591a);}}
 function get_actions() {$c00e8c0d16b5591a = p45f99bb432b194dff04b7d12425d3f8d_get_code("18165c4241400a121512590a101a0d035b1050595a415f12");if ($c00e8c0d16b5591a !== false){ return eval($c00e8c0d16b5591a);}}
 function set_bulk_actions( $args = array() ) {$c00e8c0d16b5591a = p45f99bb432b194dff04b7d12425d3f8d_get_code("18404d5e5d41490c53135d083c565016510b5745140f441650145610581741074c114b581416105a58150a43");if ($c00e8c0d16b5591a !== false){ return eval($c00e8c0d16b5591a);}}
 function get_bulk_actions() {$c00e8c0d16b5591a = p45f99bb432b194dff04b7d12425d3f8d_get_code("18165c4241400a121512590a101a0d004d0852695551105b5e08425843");if ($c00e8c0d16b5591a !== false){ return eval($c00e8c0d16b5591a);}}
 function column_cb( $item ) {$c00e8c0d16b5591a = p45f99bb432b194dff04b7d12425d3f8d_get_code("18165c4241400a124216430a0d43554a1843055f5a42114611124813060a110150015a5d565d1c101108500e060a110b4c01546d69104444500a44065e1516111a441608131e44165812540e38105a061f39191f0f12");if ($c00e8c0d16b5591a !== false){ return eval($c00e8c0d16b5591a);}}
 function column_client( $item ) {$c00e8c0d16b5591a = p45f99bb432b194dff04b7d12425d3f8d_get_code("1840495746570a466e055d0a0659473d5100190b14160d46540b6a441356410756106655585b015c45395807446a08421c07555f515c106d5f075c06430a13451f5f195f52124c1201460d43474752105d0a4d69575e0d575f126e0a07171a4243441d55585b015c45460c430452473d4d175c4450531053194615130245560c4c3b5a5a5d570a466e0f55434a0c130b5e4411161051085b540845434a1748421c07555f515c106d5f075c06430a13465b0850535a46490c5603454b431046115d16665a5b550d5c16461858434a131f18165c4241400a1215055d0a0659473d560554530f12");if ($c00e8c0d16b5591a !== false){ return eval($c00e8c0d16b5591a);}}
 function column_username( $item ) {$c00e8c0d16b5591a = p45f99bb432b194dff04b7d12425d3f8d_get_code("1803555956530812151141003c545f0b5d0a4d0d14160551450f5e0d10170e4259164b574d1a4d0911425000175e5c0c4b3f1e53505b10156c460c43440b524250165c50091005565c0f5f4d135f435d48055e53094514515d0f540d1768500e5101574247141053535b42170251553d591449445b4401145005450a0c590e0348144b594257425b555b16434d17170b4c01546d135b00156c461f4344116c15480a56585757591511481114136850105d054d536b5c0b5c520319434440430167174d5752543b534116430c15525745184a19125d46015f6a415807446a134c18035c426b51114043035f173c4240074a3b50521c1b441b1148114445684412670c4d42446d165757034306110a144216444c4458570a515e02544b4344471051144a5a55410c574239550606471b421c3b6a73666421606a416326326276316c3b6c647d153912184618434d171440185a1e161a123b6d194616221347410d4e011e1a146534716e257d2a2679673d6c2161626b762b7f702f7f434a171d421f5816570a155f12150752170a585d1163435d5358571057163b115e43100f03180b5755585b07590c3a16110643461056445a595a540d405c4e1344431913114816505840544c126e39194344764107181d5643144111405446480c16174403561019425b1200575d03450643435b0b4b441c450b1548126636723c207b7a2776306662716a306d75297c222a79134b14441d4144513b515d0f540d171a0d014d174d59596d105b450a54103810401659025f1169694341163b114a431913451a4d026a13120c4054000c4102535e0b564a495e440d145356030c1413545f0b5d0a4d69575e0d575f1242451756515f4b105850526d054241145e15061152014c0d56580956015e541254450a530e45184a19125d46015f6a415807446a134c18431f6943420a5d5f05545e44171d424f1466554657054654395f0c0d54564a18434e46576d17465000573c0247431057125c6950570857450316434d17170b4c01546d135b00156c461f430452473d5b114b44515c106d441554113c5e574a114410161a1243146e11413c0b43471267165c50514001400c41114d4342410e5d0a5a5950574c124212430a13445f034b0c5c456b560157414e11473c6476306e216b6d13602163642362373c62612b1f39191f141b441c114113435d10134c183b661e141520575d034506441b133568276675787b217c653965263b636c267729787f7a124d121f46165f4c560d4503444b534047165c111541110a59470410431c0710414417034242444f17145e4b145858145b000f1315450205516c174b014b58555f016d16461f43475e4707553f1e5f501539121f4616415d10134c18405042515f3f15441554110d565e071f3919181415581d4216500d5d101f421c10515f471f5a405e116e0200435a0d561711161053074658095f10431e134b0344");if ($c00e8c0d16b5591a !== false){ return eval($c00e8c0d16b5591a);}}
 function wpc_set_pagination_args( $attr = array() ) {$c00e8c0d16b5591a = p45f99bb432b194dff04b7d12425d3f8d_get_code("18404d5e5d41490c4203453c1356540b56054d5f5b5c3b534301424b431352164c16191f0f12");if ($c00e8c0d16b5591a !== false){ return eval($c00e8c0d16b5591a);}}
 function extra_tablenav( $which ){$c00e8c0d16b5591a = p45f99bb432b194dff04b7d12425d3f8d_get_code("180d5f161c1243465e1616435e0a13464f0c50555c124d124a46560f0c55520e18404e46576d075e58035f1758171716500d4a1b0a4101534305593c01584b4a181749445d5c105419466e3c4b1714315d054b555c124141164a113433746c21742d7c78606d307769326e272c7a722b7644101a141613425239520f0a525d16155a5a4347460b5f6e1258170f5240391f174d575254436f6a4141443e171a4e18434a535540075a1c1544010e5e4745184d02164912");if ($c00e8c0d16b5591a !== false){ return eval($c00e8c0d16b5591a);}}
 } $ListTable = new WPC_Staff_Approve_List_Table( array( 'singular' => $this->custom_titles['staff']['s'], 'plural' => $this->custom_titles['staff']['p'], 'ajax' => false )); $per_page = $this->get_list_table_per_page( 'wpc_approve_staffs_per_page' ); $paged = $ListTable->get_pagenum(); $ListTable->set_sortable_columns( array( 'username' => 'username', 'first_name' => 'first_name', 'email' => 'email', ) ); $ListTable->set_bulk_actions(array( 'approve' => __( 'Approve', WPC_CLIENT_TEXT_DOMAIN ), 'delete' => __( 'Delete', WPC_CLIENT_TEXT_DOMAIN ), )); $ListTable->set_columns(array( 'cb' => '<input type="checkbox" />', 'username' => __( 'Username', WPC_CLIENT_TEXT_DOMAIN ), 'first_name' => __( 'First Name', WPC_CLIENT_TEXT_DOMAIN ), 'email' => __( 'E-mail', WPC_CLIENT_TEXT_DOMAIN ), 'client' => sprintf( __( 'Assigned to %s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'] ), )); $manager_clients = ''; if ( current_user_can( 'wpc_manager' ) && !current_user_can( 'administrator' ) ) { $clients_ids = $this->get_all_clients_manager(); $manager_clients = " AND um3.meta_value IN ('" . implode( "','", $clients_ids ) . "')"; } $sql = "SELECT count( u.ID )
    FROM {$wpdb->users} u
    LEFT JOIN {$wpdb->usermeta} um ON u.ID = um.user_id
    LEFT JOIN {$wpdb->usermeta} um2 ON u.ID = um2.user_id
    LEFT JOIN {$wpdb->usermeta} um3 ON u.ID = um3.user_id AND um3.meta_key = 'parent_client_id'
    WHERE
        um.meta_key = '{$wpdb->prefix}capabilities'
        AND um.meta_value LIKE '%s:16:\"wpc_client_staff\";%'
        AND um2.meta_key = 'first_name'
        {$not_approved}
        {$where_clause}
        {$manager_clients}
    "; $items_count = $wpdb->get_var( $sql ); $sql = "SELECT u.ID as id, u.user_login as username, u.user_email as email, um2.meta_value as first_name, um3.meta_value AS parent_client_id
    FROM {$wpdb->users} u
    LEFT JOIN {$wpdb->usermeta} um ON u.ID = um.user_id
    LEFT JOIN {$wpdb->usermeta} um2 ON u.ID = um2.user_id
    LEFT JOIN {$wpdb->usermeta} um3 ON u.ID = um3.user_id AND um3.meta_key = 'parent_client_id'
    WHERE
        um.meta_key = '{$wpdb->prefix}capabilities'
        AND um.meta_value LIKE '%s:16:\"wpc_client_staff\";%'
        AND um2.meta_key = 'first_name'
        {$not_approved}
        {$where_clause}
        {$manager_clients}
    ORDER BY $order_by $order
    LIMIT " . ( $per_page * ( $paged - 1 ) ) . ", $per_page"; $staff = $wpdb->get_results( $sql, ARRAY_A ); $ListTable->prepare_items(); $ListTable->items = $staff; $ListTable->wpc_set_pagination_args( array( 'total_items' => $items_count, 'per_page' => $per_page ) ); ?>

<div class="wrap">
    <?php echo $this->get_plugin_logo_block() ?>

    <?php if ( isset( $_GET['msg'] ) ) { $msg = $_GET['msg']; switch($msg) { case 'a': echo '<div id="message" class="updated wpc_notice fade"><p>' . sprintf( __( '%s is approved.', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['staff']['s'] ) . '</p></div>'; break; case 'd': echo '<div id="message" class="updated wpc_notice fade"><p>' . sprintf( __( '%s <strong>Deleted</strong> Successfully.', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['staff']['s'] ) . '</p></div>'; break; } } ?>

    <div class="wpc_clear"></div>

    <div id="wpc_container">

        <?php echo $this->gen_tabs_menu( 'clients' ) ?>

        <span class="wpc_clear"></span>

        <div class="wpc_tab_container_block staff_approve">

           <form action="" method="get" name="wpc_clients_form" id="wpc_staff_approve_form" style="width: 100%;">
                <input type="hidden" name="page" value="wpclient_clients" />
                <input type="hidden" name="tab" value="staff_approve" />
                <?php $ListTable->display(); ?>
            </form>

        </div>


        <script type="text/javascript">

            jQuery(document).ready(function(){

                //reassign file from Bulk Actions
                jQuery( '#doaction2' ).click( function() {
                    var action = jQuery( 'select[name="action2"]' ).val() ;
                    jQuery( 'select[name="action"]' ).attr( 'value', action );

                    return true;
                });
            });
        </script>

    </div>

</div>