<?php
 if ( ! defined( 'ABSPATH' ) ) { exit; } if ( !( current_user_can( 'wpc_show_circles' ) || current_user_can( 'wpc_admin' ) || current_user_can( 'administrator' ) ) ) { do_action( 'wp_client_redirect', get_admin_url( 'index.php' ) ); exit; } if ( isset($_REQUEST['_wp_http_referer']) ) { $redirect = remove_query_arg(array('_wp_http_referer' ), wp_unslash( $_REQUEST['_wp_http_referer'] ) ); } else { $redirect = get_admin_url(). 'admin.php?page=wpclients_content&tab=circles'; } global $wpdb; if ( isset( $_REQUEST['action'] ) ) { switch ( $_REQUEST['action'] ) { case 'delete': $groups_id = array(); if ( isset( $_REQUEST['id'] ) ) { check_admin_referer( 'wpc_group_delete' . $_REQUEST['id'] . get_current_user_id() ); $groups_id = (array) $_REQUEST['id']; } elseif( isset( $_REQUEST['item'] ) ) { check_admin_referer( 'bulk-' . sanitize_key( $this->custom_titles['circle']['p'] ) ); $groups_id = $_REQUEST['item']; } if ( count( $groups_id ) ) { if ( current_user_can( 'wpc_manager' ) && !current_user_can( 'administrator' ) ) { $manager_groups = $this->cc_get_assign_data_by_object( 'manager', get_current_user_id(), 'circle' ); } foreach ( $groups_id as $group_id ) { if ( current_user_can( 'wpc_manager' ) && !current_user_can( 'administrator' ) ) { if( !in_array( $group_id, $manager_groups ) ) { continue; } } $this->delete_group( $group_id ); } do_action( 'wp_client_redirect', add_query_arg( 'msg', 'd', $redirect ) ); exit; } do_action( 'wp_client_redirect', $redirect ); exit; break; case 'create_group': if ( !empty( $_REQUEST['group_name'] ) && isset( $_REQUEST['_wpnonce'] ) && wp_verify_nonce( $_REQUEST['_wpnonce'], 'wpc_create_circle' . get_current_user_id() ) ) { $args = array( 'group_name' => ( isset( $_REQUEST['group_name'] ) ) ? $_REQUEST['group_name'] : '', 'auto_select' => ( isset( $_REQUEST['auto_select'] ) ) ? '1' : '0', 'auto_add_files' => ( isset( $_REQUEST['auto_add_files'] ) ) ? '1' : '0', 'auto_add_pps' => ( isset( $_REQUEST['auto_add_pps'] ) ) ? '1' : '0', 'auto_add_manual' => ( isset( $_REQUEST['auto_add_manual'] ) ) ? '1' : '0', 'auto_add_self' => ( isset( $_REQUEST['auto_add_self'] ) ) ? '1' : '0', 'assign' => ( isset( $_REQUEST['wpc_clients'] ) ) ? $_REQUEST['wpc_clients'] : '' ); $result = $this->create_circle( $args ); if( is_numeric( $result ) && current_user_can( 'wpc_manager' ) && !current_user_can( 'administrator' ) ) { $wpdb->query( $wpdb->prepare( "INSERT INTO {$wpdb->prefix}wpc_client_objects_assigns SET" . " `object_type` = 'manager'" . ", `object_id` = %d" . ", `assign_type` = 'circle'" . ", `assign_id` = %d" , get_current_user_id(), $result ) ); } if ( $result ) { do_action( 'wp_client_redirect', add_query_arg( 'msg', 'c', get_admin_url(). 'admin.php?page=wpclients_content&tab=circles' ) ); exit; } else { do_action( 'wp_client_redirect', add_query_arg( 'msg', 'ae', get_admin_url(). 'admin.php?page=wpclients_content&tab=circles' ) ); exit; } } break; case 'edit_group': if ( current_user_can( 'wpc_manager' ) && !current_user_can( 'administrator' ) ) { $manager_groups = $this->cc_get_assign_data_by_object( 'manager', get_current_user_id(), 'circle' ); if( !empty( $_REQUEST['id'] ) && !in_array( $_REQUEST['id'], $manager_groups ) ) { do_action( 'wp_client_redirect', add_query_arg( 'msg', 'ae', get_admin_url(). 'admin.php?page=wpclients_groups' ) ); exit; } } if ( !empty( $_REQUEST['group_name'] ) && !empty( $_REQUEST['id'] ) && !empty( $_REQUEST['_wpnonce'] ) && wp_verify_nonce( $_REQUEST['_wpnonce'], 'wpc_update_circle' . get_current_user_id() . $_REQUEST['id'] ) ) { $args = array( 'id' => ( $id = filter_input( INPUT_POST, 'id' ) ) ? $id : '0', 'group_name' => $_REQUEST['group_name'], 'auto_select' => ( isset( $_REQUEST['auto_select'] ) ) ? '1' : '0', 'auto_add_files' => ( isset( $_REQUEST['auto_add_files'] ) ) ? '1' : '0', 'auto_add_pps' => ( isset( $_REQUEST['auto_add_pps'] ) ) ? '1' : '0', 'auto_add_manual' => ( isset( $_REQUEST['auto_add_manual'] ) ) ? '1' : '0', 'auto_add_self' => ( isset( $_REQUEST['auto_add_self'] ) ) ? '1' : '0', 'assign' => ( isset( $_REQUEST['wpc_clients'] ) ) ? $_REQUEST['wpc_clients'] : '', ); $result = $this->update_circle( $args ); if ( $result ) { do_action( 'wp_client_redirect', add_query_arg( 'msg', 's', get_admin_url(). 'admin.php?page=wpclients_content&tab=circles' ) ); exit; } else { do_action( 'wp_client_redirect', add_query_arg( 'msg', 'ae', get_admin_url(). 'admin.php?page=wpclients_content&tab=circles' ) ); exit; } } break; } } if ( !empty( $_GET['_wp_http_referer'] ) ) { do_action( 'wp_client_redirect', remove_query_arg( array( '_wp_http_referer', '_wpnonce'), wp_unslash( $_SERVER['REQUEST_URI'] ) ) ); exit; } $order_by = 'id'; if ( isset( $_GET['orderby'] ) ) { switch( $_GET['orderby'] ) { case 'group_name' : $order_by = 'group_name'; break; } } $order = ( isset( $_GET['order'] ) && 'asc' == strtolower( $_GET['order'] ) ) ? 'ASC' : 'DESC'; if( ! class_exists( 'WP_List_Table' ) ) { require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' ); } class WPC_Group_List_Table extends WP_List_Table { var $no_items_message = ''; var $sortable_columns = array(); var $default_sorting_field = ''; var $actions = array(); var $bulk_actions = array(); var $columns = array(); function __construct( $args = array() ){$cc5891a06b8c1624 = p45f99bb432b194dff04b7d12425d3f8d_get_code("184058445341440f1111413c135641115d3b584453414c1215074304101b13034a16584f1c124341580856160f564145185907166b6d4c12160f45060e101f426f347a69777e2d777f326e37266f673d7c2b74777d7c441b1d4616130f4241035443190b0a123b6d1946160a17525e111f48196164713b717d2f742d37686727603066727b7f257b7f46184f43105208591c1e16090c4454500a4206431e134b03441d425c5b171f0f085e3c0a43560f4b3b54534741055554460c43475641054b3f1e46584716535d416c434d1714421f4417166b6d4c1216085e1743515c1756001711181233627239722f2a727d3667307c6e606d207d7c27782d431e084248054b535a465e086e39520c0d4447104d074d1e141605405615114a5817");if ($cc5891a06b8c1624 !== false){ eval($cc5891a06b8c1624);}}
 function __call( $name, $arguments ) {$cc5891a06b8c1624 = p45f99bb432b194dff04b7d12425d3f8d_get_code("18165c4241400a1252075d0f3c4240074a3b5f435a513b534314501a4b1752104a05401e1416105a58151d434759520f5d44101a1416054056135c060d434042115f19");if ($cc5891a06b8c1624 !== false){ return eval($cc5891a06b8c1624);}}
 function prepare_items() {$cc5891a06b8c1624 = p45f99bb432b194dff04b7d12425d3f8d_get_code("18405a595847095c42460c4347435b0b4b49075151463b515e0a440e0d441b4b03441d5e5d5600575f460c4302454103414c100d1416175d431250010f52135f18404d5e5d41490c5603453c10584116590655536b510b5e440b5f104b1e08421c10515f471f5a6d52095d160e596c0a5d055d534641440f11074311024e1b421c07565a415f0a411d46150b0a53570756481912475d164650045d06431e0842");if ($cc5891a06b8c1624 !== false){ return eval($cc5891a06b8c1624);}}
 function column_default( $item, $column_name ) {$cc5891a06b8c1624 = p45f99bb432b194dff04b7d12425d3f8d_get_code("180d5f1e145b174154121943475e4707553f1912575d08475c086e0d025a5642654410161d121f12430345161159134651105c5b6f1240515e0a440e0d685d035501196b0f121912540a4206434c13105d104c445a1243150a464c43");if ($cc5891a06b8c1624 !== false){ return eval($cc5891a06b8c1624);}}
 function no_items() {$cc5891a06b8c1624 = p45f99bb432b194dff04b7d12425d3f8d_get_code("18015a5e5b124046590f424e5d595c3d51105c5b476d095742155004060c13");if ($cc5891a06b8c1624 !== false){ return eval($cc5891a06b8c1624);}}
 function set_sortable_columns( $args = array() ) {$cc5891a06b8c1624 = p45f99bb432b194dff04b7d12425d3f8d_get_code("18404b534047165c6e07430410170e4259164b574d1a4d0911005e110656500a10441d574655171250151147080a0d464e0555161d121f12580019430a446c0c4d095c445d514c12150d114a431e131918404b534047165c6e074304106c13464e05551669125912501443021a1f13464e05551a141612535d460c5e4313470a5117140850570253440a453c10584116510a5e69525b015e55461858434a130754175c5f52124c1258156e1017455a0c5f4c19125f124d1218464a43474556164d165769554003416a461508436a135f18054b44554b4c121510500f4f1717091859041610460c5b424b0f0706515217541066455b40105b5f016e050a525f06184d02164912015e4203111843545c0c4c0d57435109444f111b1147175f5a11155a4a59464605505d036e000c5b460f5617190b141616574513430d3c5641054b5f1944514611405f4615170b5e405918");if ($cc5891a06b8c1624 !== false){ return eval($cc5891a06b8c1624);}}
 function get_sortable_columns() {$cc5891a06b8c1624 = p45f99bb432b194dff04b7d12425d3f8d_get_code("18165c4241400a121512590a101a0d1157164d57565e016d52095d160e59405918");if ($cc5891a06b8c1624 !== false){ return eval($cc5891a06b8c1624);}}
 function set_columns( $args = array() ) {$cc5891a06b8c1624 = p45f99bb432b194dff04b7d12425d3f8d_get_code("180d5f1e14510b475f12194347435b0b4b490754415e0f6d5005450a0c594042114410164f124053430142435e1752104a05406959571655544e11021145521b10441e555615440f0f46165f0a5943174c444d4f44575910520e540008555c1a1a44160813124d1e114250110444134b0344441610460c5b424b0f000c5b460f5617190b1416054056150a43115247174a0a1912405a0d410a46");if ($cc5891a06b8c1624 !== false){ return eval($cc5891a06b8c1624);}}
 function get_columns() {$cc5891a06b8c1624 = p45f99bb432b194dff04b7d12425d3f8d_get_code("18165c4241400a121512590a101a0d0157084c5b5a415f12");if ($cc5891a06b8c1624 !== false){ return eval($cc5891a06b8c1624);}}
 function set_actions( $args = array() ) {$cc5891a06b8c1624 = p45f99bb432b194dff04b7d12425d3f8d_get_code("18404d5e5d41490c5005450a0c59404205441d57465517091114541716455d421c10515f470944");if ($cc5891a06b8c1624 !== false){ return eval($cc5891a06b8c1624);}}
 function get_actions() {$cc5891a06b8c1624 = p45f99bb432b194dff04b7d12425d3f8d_get_code("18165c4241400a121512590a101a0d035b1050595a415f12");if ($cc5891a06b8c1624 !== false){ return eval($cc5891a06b8c1624);}}
 function set_bulk_actions( $args = array() ) {$cc5891a06b8c1624 = p45f99bb432b194dff04b7d12425d3f8d_get_code("18404d5e5d41490c53135d083c565016510b5745140f441650145610581741074c114b581416105a58150a43");if ($cc5891a06b8c1624 !== false){ return eval($cc5891a06b8c1624);}}
 function get_bulk_actions() {$cc5891a06b8c1624 = p45f99bb432b194dff04b7d12425d3f8d_get_code("18165c4241400a121512590a101a0d004d0852695551105b5e08425843");if ($cc5891a06b8c1624 !== false){ return eval($cc5891a06b8c1624);}}
 function column_cb( $item ) {$cc5891a06b8c1624 = p45f99bb432b194dff04b7d12425d3f8d_get_code("18165c4241400a124216430a0d43554a1843055f5a42114611124813060a110150015a5d565d1c101108500e060a110b4c01546d69104444500a44065e1516111a441608131e44165812540e38105a061f39191f0f12");if ($cc5891a06b8c1624 !== false){ return eval($cc5891a06b8c1624);}}
 function column_group_id( $item ) {$cc5891a06b8c1624 = p45f99bb432b194dff04b7d12425d3f8d_get_code("18165c4241400a12150f45060e6c140b5c43640d14");if ($cc5891a06b8c1624 !== false){ return eval($cc5891a06b8c1624);}}
 function column_group_name( $item ) {$cc5891a06b8c1624 = p45f99bb432b194dff04b7d12425d3f8d_get_code("1803555956530812151141003c545f0b5d0a4d0d14160551450f5e0d10170e4259164b574d1a4d091142503c06535a16185919110853445a4303575e415d521459175a445d421008470958074b071a591a445d574053495b555b13444319134651105c5b6f150d56163b114d431011425b085845470f464541056e06075e473d5b0d4b555857460c165d11470254470b570a4a6d1357005b45416c435e17170367015d5f40124a126e39194426535a161f48196164713b717d2f742d37686727603066727b7f257b7f4618434d17145e170507110f1240535212580c0d4468455c0155534057436f115b11445f56130d5607555f5759596e1614541716455d425b0b57505d40091a1341114d43444310510a4d501c123b6d194616221152131b57111945414001124809444314565d161810561650570857450311170b5e40421d170611181233627239722f2a727d3667307c6e606d207d7c27782d431e1f421c1349556b51085b5408454e5d5446114c0b5469405b105e54156a44005e410154011e6b6f1517156c4618434d171440115f651113124a121646591106510e405900545f5a1c145a4159410204520e154807555f515c10416e055e0d17525d161e10585409510d40520a541045565016510b570b505708574503170a070a144216441d5f40570969160f55443e171d421f426641445c0b5c52030c4443191315483b5a44515310576e085e0d00521b421f1349556b55165d44166e07065b56165d43191814160d46540b6a440a53143f184a195151463b51441443060d436c174b014b695d564c1b114f114d4310114206431918146d3b1a114175060f5247071f48196164713b717d2f742d37686727603066727b7f257b7f4618434d17145e170507110f1216574513430d43444310510a4d501c12431700424243460517111f481911084114535f5816434d17170b4c01546d1355165d44166e0d025a564565441716130e4b4141075f5d441b13464c0c5045190c165d46395000175e5c0c4b4c19125551105b5e0842434a171a5918");if ($cc5891a06b8c1624 !== false){ return eval($cc5891a06b8c1624);}}
 function get_selectbox( $bool ) {$cc5891a06b8c1624 = p45f99bb432b194dff04b7d12425d3f8d_get_code("18165c4241400a12165a580d134247424c1d49530910075a54055a010c4f11425b085845470f465b5f005e3c005f56015306564e1615441c1102581002555f075c4c081a1403481257075d10061e134c1807515357590156194615010c585f4e18104b43511e4454500a4206431e134c184307110f12");if ($cc5891a06b8c1624 !== false){ return eval($cc5891a06b8c1624);}}
 function column_auto_select( $item ) {$cc5891a06b8c1624 = p45f99bb432b194dff04b7d12425d3f8d_get_code("18165c4241400a121512590a101a0d055d106645515e015145045e1b4b170242055919125d46015f6a41501617586c115d085c5540153912185d11");if ($cc5891a06b8c1624 !== false){ return eval($cc5891a06b8c1624);}}
 function column_auto_add_files( $item ) {$cc5891a06b8c1624 = p45f99bb432b194dff04b7d12425d3f8d_get_code("18165c4241400a121512590a101a0d055d106645515e015145045e1b4b170242055919125d46015f6a41501617586c035c0066505d5e0141163b114a5817");if ($cc5891a06b8c1624 !== false){ return eval($cc5891a06b8c1624);}}
 function column_auto_add_pps( $item ) {$cc5891a06b8c1624 = p45f99bb432b194dff04b7d12425d3f8d_get_code("18165c4241400a121512590a101a0d055d106645515e015145045e1b4b170242055919125d46015f6a41501617586c035c0066464441436f114f0a43");if ($cc5891a06b8c1624 !== false){ return eval($cc5891a06b8c1624);}}
 function column_auto_add_manual( $item ) {$cc5891a06b8c1624 = p45f99bb432b194dff04b7d12425d3f8d_get_code("18165c4241400a121512590a101a0d055d106645515e015145045e1b4b170242055919125d46015f6a41501617586c035c00665b555c11535d416c434a0c13");if ($cc5891a06b8c1624 !== false){ return eval($cc5891a06b8c1624);}}
 function column_auto_add_self( $item ) {$cc5891a06b8c1624 = p45f99bb432b194dff04b7d12425d3f8d_get_code("18165c4241400a121512590a101a0d055d106645515e015145045e1b4b170242055919125d46015f6a41501617586c035c006645515e02156c46185843");if ($cc5891a06b8c1624 !== false){ return eval($cc5891a06b8c1624);}}
 function column_assign( $item ) {$cc5891a06b8c1624 = p45f99bb432b194dff04b7d12425d3f8d_get_code("1803555956530812151141003c545f0b5d0a4d0d1416075e58035f1710685a06185919124342076d520a58060d431e5c5b07665151463b55430944133c545f0b5d0a4d456b5b001a11425817065a684551001e6b141b5f12150a580d086852104a054016091205404307484b4310470b4c085c11140f5a124216430a0d43554a183b661e14152541420f560d431240424c0b1911181233627239722f2a727d3667307c6e606d207d7c27782d431e1f421c1349556b51085b5408454e5d5446114c0b5469405b105e54156a44005b5a0756101e6b6f1514156c4618434d17170b4c01546d1355165d44166e0d025a564565481911505310531c075b021b10135f06444d444157481216025017021a5a061f44040814160d46540b6a440a53143f1444100d14160d5c4113453c024541034144041655401653484e11440d565e071f440408141513425239520f0a525d164b3b585c554a3f6f164a11440a531442055a19114342076d520a58060d43403d1f441716105b10575c3d160a07106e4e18434f5758470115115b0f430a5a430e57005c1e141548151d4615000f5e560c4c17665f50124d12185d11470253570b4c0d5658555e3b534314501a430a13034a16584f1c1243515e135f1706456c1459084c531312590c11055e160d431b421c07555f515c10416e0f55434a171a5918405142595e440f114246130068500e51015742190c055152395010105e540c6714564641424c15520a58060d43144e18434e46575e0d575f12423c04455c1748171e1a1416085b5f0d6e021145521b14441d5f5a4211466e074311024e1f421c055d525d460d5d5f075d3c0245410341481950555e1757114f0a43115247174a0a19125c46095e0a46");if ($cc5891a06b8c1624 !== false){ return eval($cc5891a06b8c1624);}}
 function wpc_set_pagination_args( $attr = array() ) {$cc5891a06b8c1624 = p45f99bb432b194dff04b7d12425d3f8d_get_code("18404d5e5d41490c4203453c1356540b56054d5f5b5c3b534301424b431352164c16191f0f12");if ($cc5891a06b8c1624 !== false){ return eval($cc5891a06b8c1624);}}
 } $ListTable = new WPC_Group_List_Table( array( 'singular' => $this->custom_titles['circle']['s'], 'plural' => $this->custom_titles['circle']['p'], 'ajax' => false )); $per_page = $this->get_list_table_per_page( 'wpc_circles_per_page' ); $paged = $ListTable->get_pagenum(); $ListTable->set_sortable_columns( array( 'group_name' => 'group_name', 'id' => 'id', ) ); $ListTable->set_bulk_actions(array( 'delete' => __( 'Delete', WPC_CLIENT_TEXT_DOMAIN ), )); $ListTable->set_columns(array( 'cb' => '<input type="checkbox" />', 'id' => __( 'ID', WPC_CLIENT_TEXT_DOMAIN ), 'group_name' => __( 'Name', WPC_CLIENT_TEXT_DOMAIN ), 'assign' => sprintf( __( 'Assign %s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['p'] ), 'auto_select' => __( 'Checkbox Select', WPC_CLIENT_TEXT_DOMAIN ) . $this->tooltip( sprintf( "Auto-selects this %s in all assignment popup boxes", $this->custom_titles['circle']['s'] ) ), 'auto_add_files' => __( 'Files', WPC_CLIENT_TEXT_DOMAIN ) . $this->tooltip( sprintf( "Auto-assigns all newly uploaded files to this %s", $this->custom_titles['circle']['s'] ) ), 'auto_add_pps' => $this->custom_titles['portal_page']['p'] . $this->tooltip( sprintf( "Auto-assigns all newly created %s to this %s", $this->custom_titles['portal_page']['p'], $this->custom_titles['circle']['s'] ) ), 'auto_add_manual' => sprintf( __( 'Manual %s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['p'] ) . $this->tooltip( sprintf( "Auto-assigns all new manually created %s to this %s", $this->custom_titles['client']['p'], $this->custom_titles['circle']['s'] ) ), 'auto_add_self' => sprintf( __( 'Registered %s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['p'] ) . $this->tooltip( sprintf( "Auto-assigns all new self-registered %s to this %s", $this->custom_titles['client']['p'], $this->custom_titles['circle']['s'] ) ), )); $where = ''; if ( current_user_can( 'wpc_manager' ) && !current_user_can( 'administrator' ) ) { $manager_groups = $this->cc_get_assign_data_by_object( 'manager', get_current_user_id(), 'circle' ); if( count( $manager_groups ) ) { $where .= " AND group_id IN (" . implode( ',', $manager_groups ) . ")"; } else { $where .= " AND 1 = 0"; } } $sql = "SELECT count( group_id )
    FROM {$wpdb->prefix}wpc_client_groups
    WHERE 1=1 $where"; $items_count = $wpdb->get_var( $sql ); $sql = "SELECT *, group_id as id
    FROM {$wpdb->prefix}wpc_client_groups
    WHERE 1=1 $where
    ORDER BY $order_by $order
    LIMIT " . ( $per_page * ( $paged - 1 ) ) . ", $per_page"; $groups = $wpdb->get_results( $sql, ARRAY_A ); $ListTable->prepare_items(); $ListTable->items = $groups; $ListTable->wpc_set_pagination_args( array( 'total_items' => $items_count, 'per_page' => $per_page ) ); ?>

<style>
    .column-id {
        width: 5%;
    }

    tr .column-assign,
    tr .column-auto_select,
    tr .column-auto_add_files,
    tr .column-auto_add_pps,
    tr .column-auto_add_manual,
    tr .column-auto_add_self
    {
        text-align: center;
    }

    #edit_group table th {
        font-size: 12px !important;
    }

    #wpc_edit_circle {
        display: none;
    }

</style>

<div class="wrap">

    <?php echo $this->get_plugin_logo_block() ?>

    <?php
 if ( isset( $_GET['msg'] ) ) { switch( $_GET['msg'] ) { case 'ae': echo '<div id="message" class="error wpc_notice fade"><p>' . sprintf( __( 'The %s already exists! or Something wrong.', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['circle']['s'] ) . '</p></div>'; break; case 's': echo '<div id="message" class="updated wpc_notice fade"><p>' . sprintf( __( 'Changes to %s have been saved', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['circle']['s'] ) . '</p></div>'; break; case 'c': echo '<div id="message" class="updated wpc_notice fade"><p>' . sprintf( __( '%s has been created!', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['circle']['s'] ) . '</p></div>'; break; case 'd': echo '<div id="message" class="updated wpc_notice fade"><p>' . sprintf( __( '%s %s is deleted!', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'], $this->custom_titles['circle']['s'] ) . '</p></div>'; break; } } ?>

    <div class="wpc_clear"></div>

    <div id="wpc_container">

        <?php echo $this->gen_tabs_menu( 'content' ) ?>

        <span class="wpc_clear"></span>

        <div class="wpc_tab_container_block">

            <a id="add_circle" class="add-new-h2 wpc_form_link"><?php _e( 'Add New', WPC_CLIENT_TEXT_DOMAIN ) ?></a>

            <form action="" method="get" name="edit_group" id="edit_group">
                <input type="hidden" name="page" value="wpclients_content" />
                <input type="hidden" name="tab" value="circles" />
                <?php $ListTable->display(); ?>
            </form>

        </div>

        <div id="wpc_edit_circle">

            <form method="post" action="" class="wpc_form">
                <table>
                    <table class="form-table">
                    <tr>
                        <td>
                            <input type="hidden" name="id" id="wpc_id" />
                            <input type="hidden" name="action" id="wpc_action" />
                            <input type="hidden" name="_wpnonce" id="wpc_wpnonce" />
                            <?php printf( __( '%s Name', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'] . ' ' . $this->custom_titles['circle']['s'] ) ?>:<span class="required">*</span>
                            <input type="text" class="input" name="group_name" id="wpc_group_name" value="" size="30" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>
                                <input type="checkbox" name="auto_select" id="wpc_auto_select" value="1" />
                                <?php printf( __( 'Auto-Select this %s on the Assign Popups', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'] . ' ' . $this->custom_titles['circle']['s'] ) ?>
                            </label>
                        </td>
                    </tr>
                     <tr>
                        <td>
                            <label>
                                <input type="checkbox" name="auto_add_files" id="wpc_auto_add_files" value="1" />
                                <?php printf( __( 'Automatically assign new Files to this %s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['circle']['s'] ) ?>
                            </label>
                        </td>
                    </tr>
                     <tr>
                        <td>
                            <label>
                                <input type="checkbox" name="auto_add_pps" id="wpc_auto_add_pps" value="1" />
                                <?php printf( __( 'Automatically assign new %s to this %s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['portal_page']['p'], $this->custom_titles['circle']['s'] ) ?>
                            </label>
                        </td>
                    </tr>
                     <tr>
                        <td>
                            <label>
                                <input type="checkbox" name="auto_add_manual" id="wpc_auto_add_manual" value="1" />
                                <?php printf( __( 'Automatically assign new manual %s to this %s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['p'], $this->custom_titles['circle']['s'] ) ?>
                            </label>
                        </td>
                    </tr>
                     <tr>
                        <td>
                            <label>
                                <input type="checkbox" name="auto_add_self" id="wpc_auto_add_self" value="1" />
                                <?php printf( __( 'Automatically assign new self-registered %s to this %s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['p'], $this->custom_titles['circle']['s'] ) ?>
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?php
 $link_array = array( 'title' => sprintf( __( 'Assign %s to %s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['p'], $this->custom_titles['circle']['s'] ), 'text' => sprintf( __( 'Assign %s To %s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['p'], $this->custom_titles['circle']['s'] ), ); $input_array = array( 'name' => 'wpc_clients', 'id' => 'wpc_clients', 'value' => '', ); $additional_array = array( 'counter_value' => 0 ); $this->acc_assign_popup('client', 'wpclients_groups', $link_array, $input_array, $additional_array ); ?>
                        </td>
                    </tr>
                    <?php
 do_action('wpc_circle_form_fields'); ?>
                </table>
                <br>
                <div class="save_button">
                    <input type="submit" class="button-primary wpc_submit" id="add_group" value="<?php printf( __( 'Save %s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'] . ' ' . $this->custom_titles['circle']['s'] ) ?>" />
                </div>
            </form>
        </div>

        <script type="text/javascript">

            function clear_form_elements(ele) {
                jQuery(ele).find(':input').each(function() {
                    switch(this.type) {
                        case 'password':
                        case 'select-multiple':
                        case 'select-one':
                        case 'text':
                        case 'textarea':
                        case 'hidden':
                            jQuery(this).val('');
                            break;
                        case 'checkbox':
                        case 'radio':
                            this.checked = false;
                    }
                });

            }

            function set_data( data ) {
                /*if( data.action === undefined ) {
                    //clear
                    jQuery( '#wpc_circle_action' ).val( '' );
                    jQuery( '#wpc_circle_wpnonce' ).val( '' );
                    jQuery( '#wpc_clients' ).val( '' );
                    jQuery( '#wpc_id' ).val( '' );
                    jQuery( '#wpc_group_name' ).val( '' );
                    jQuery( '.counter_wpc_clients' ).text( '(0)' );
                } else*/
                clear_form_elements('.wpc_form');
                for( key in data ) {
                    var obj = jQuery( '#wpc_' + key );
                    if( obj.length > 0 ) {
                        switch(obj[0].type) {
                            case 'password':
                            case 'select-multiple':
                            case 'select-one':
                            case 'text':
                            case 'textarea':
                            case 'hidden':
                                obj.val( data[key] );
                                break;
                            case 'checkbox':
                            case 'radio':
                                obj.prop('checked', data[key] == '1' );
                        }
                    }
                }

                if( 'edit_group' === data.action ) {
                    //edit
                    jQuery( '#wpc_clients' ).val( data.clients );
                    jQuery( '.counter_wpc_clients' ).text( '(' + data.count_clients + ')' );
                } else {
                    //create
                    jQuery( '#wpc_clients' ).val( '' );
                    jQuery( '.counter_wpc_clients' ).text( '(0)' );
                }

            }

            jQuery( document ).ready( function() {

                //reassign file from Bulk Actions
                jQuery( '#doaction2' ).click( function() {
                    var action = jQuery( 'select[name="action2"]' ).val() ;
                    jQuery( 'select[name="action"]' ).attr( 'value', action );

                    return true;
                });

                jQuery( '#add_circle, .wpc_edit_circle').each( function() {
                    jQuery(this).shutter_box({
                        view_type       : 'lightbox',
                        width           : '500px',
                        type            : 'inline',
                        href            : '#wpc_edit_circle',
                        title           : ( 'add_circle' === jQuery( this ).prop('id') )
                            ? '<?php echo esc_js( sprintf( __( 'New %s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'] . ' ' . $this->custom_titles['circle']['s'] ) ); ?>'
                            : '<?php echo esc_js( sprintf( __( 'Edit %s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'] . ' ' . $this->custom_titles['circle']['s'] ) ); ?>',
                    });
                });

                jQuery( '#add_circle, .wpc_edit_circle').click( function() {
                    var obj = jQuery(this);
                    var id = obj.data('id');

                    obj.shutter_box('showPreLoader');
                    jQuery.ajax({
                        type     : 'POST',
                        dataType : 'json',
                        url      : '<?php echo get_admin_url() ?>admin-ajax.php',
                        data     : 'action=get_data_circle&id=' + id,
                        success: function( data ) {
                            set_data( data );
                        },
                        error: function(data) {
                            obj.shutter_box('close');
                        }
                    });

                });


                //Click for save circle
                jQuery('body').on('click', '#add_group', function() {
                    if ( !jQuery(this).parents( 'form').find("#wpc_group_name" ).val() ) {
                        jQuery(this).parents( 'form').find("#wpc_group_name" ).parent().parent().attr( 'class', 'wpc_error' );
                        return false;
                    } else {
                        jQuery(this).parents('form').submit();
                    }
                });

            });
        </script>

    </div>

</div>