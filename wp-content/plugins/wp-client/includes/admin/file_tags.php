<?php
 if ( ! defined( 'ABSPATH' ) ) { exit; } if( $this->wpc_flags['easy_mode'] ) { do_action( 'wp_client_redirect', admin_url( 'admin.php?page=wpclients_content' ) ); exit; } global $wpdb; if ( isset($_REQUEST['_wp_http_referer']) ) { $redirect = remove_query_arg(array('_wp_http_referer' ), stripslashes_deep( $_REQUEST['_wp_http_referer'] ) ); } else { $redirect = get_admin_url(). 'admin.php?page=wpclients_content&tab=files_tags'; } if( !empty( $_POST['wpc_action'] ) ) { switch( $_POST['wpc_action'] ) { case 'reassign_tag': if ( empty( $_POST['old_tag_id'] ) || empty( $_POST['new_tag_id'] ) || $_POST['old_tag_id'] === $_POST['new_tag_id'] ) { do_action( 'wp_client_redirect', add_query_arg( 'msg', 'n_rat', $redirect ) ); exit; } $file_ids = $wpdb->get_col("SELECT DISTINCT object_id
                FROM {$wpdb->term_relationships}
                WHERE term_taxonomy_id = '" . $wpdb->_real_escape( $_POST['old_tag_id'] ) . "' AND object_id NOT IN (
                    SELECT DISTINCT object_id
                    FROM {$wpdb->term_relationships}
                    WHERE term_taxonomy_id = '" . $wpdb->_real_escape( $_POST['new_tag_id'] ) . "'
                )"); $wpdb->delete( $wpdb->term_relationships, array( 'term_taxonomy_id' => $_POST['old_tag_id'] ) ); foreach( $file_ids as $val ) { $wpdb->insert( $wpdb->term_relationships, array( 'term_taxonomy_id' => $_POST['new_tag_id'], 'object_id' => $val ) ); } do_action( 'wp_client_redirect', add_query_arg( 'msg', 'rat', $redirect ) ); break; case 'create_file_tag': $term = $_POST['tag_name_new']; if ( !strlen( trim( $term ) ) ) do_action( 'wp_client_redirect', add_query_arg( 'msg', 'wt', $redirect ) ); if ( !$term_info = term_exists($term, 'wpc_file_tags') ) $term_info = wp_insert_term($term, 'wpc_file_tags'); else do_action( 'wp_client_redirect', add_query_arg( 'msg', 'aet', $redirect ) ); do_action( 'wp_client_redirect', add_query_arg( 'msg', 'st', $redirect ) ); break; } } if ( isset( $_GET['action'] ) ) { switch ( $_GET['action'] ) { case 'delete': $ids = array(); if ( isset( $_GET['tag_id'] ) ) { check_admin_referer( 'wpc_file_tag_delete' . $_GET['tag_id'] . get_current_user_id() ); $ids = (array) $_GET['tag_id']; } elseif( isset( $_REQUEST['item'] ) ) { check_admin_referer( 'bulk-' . sanitize_key( __( 'Tags', WPC_CLIENT_TEXT_DOMAIN ) ) ); $ids = $_REQUEST['item']; } if ( count( $ids ) && ( current_user_can( 'wpc_admin' ) || current_user_can( 'administrator' ) || current_user_can( 'wpc_delete_file_tags' ) ) ) { foreach ( $ids as $tag_id ) { wp_delete_term( $tag_id, 'wpc_file_tags' ); } do_action( 'wp_client_redirect', add_query_arg( 'msg', 'd', $redirect ) ); exit; } do_action( 'wp_client_redirect', $redirect ); exit; } } if ( !empty( $_GET['_wp_http_referer'] ) ) { do_action( 'wp_client_redirect', remove_query_arg( array( '_wp_http_referer', '_wpnonce'), stripslashes_deep( $_SERVER['REQUEST_URI'] ) ) ); exit; } $where_search = ''; $where_manager = ''; if( !empty( $_GET['s'] ) ) { $where_search = $this->get_prepared_search( $_GET['s'], array( 't.name', ) ); } $order_by = 'tt.term_id'; if ( isset( $_GET['orderby'] ) ) { switch( $_GET['orderby'] ) { case 'name' : $order_by = 't.name'; break; case 'count' : $order_by = 'count'; break; } } $order = ( isset( $_GET['order'] ) && 'asc' == strtolower( $_GET['order'] ) ) ? 'ASC' : 'DESC'; if ( current_user_can( 'wpc_manager' ) && !current_user_can( 'administrator' ) ) { $manager_clients = $this->cc_get_assign_data_by_object( 'manager', get_current_user_id(), 'client' ); $manager_circles = $this->cc_get_assign_data_by_object( 'manager', get_current_user_id(), 'circle' ); $client_files = $this->cc_get_assign_data_by_assign( 'file', 'client', $manager_clients ); $circle_files = $this->cc_get_assign_data_by_assign( 'file', 'circle', $manager_circles ); $files = array_merge( $client_files, $circle_files ); $files = array_unique( $files ); if ( current_user_can( 'wpc_view_admin_managers_files' ) ) { $ids_files_manager = $wpdb->get_col( "SELECT id FROM {$wpdb->prefix}wpc_client_files WHERE page_id = 0 OR id IN('" . implode( "','", $files ) . "')" ) ; } else { $ids_files_manager = $wpdb->get_col( "SELECT id FROM {$wpdb->prefix}wpc_client_files WHERE user_id = " . get_current_user_id() . " OR id IN('" . implode( "','", $files ) . "')" ); } $where_manager = " AND tr.object_id IN('" . implode( "','", $ids_files_manager ) . "')" ; } if( ! class_exists( 'WP_List_Table' ) ) { require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' ); } class WPC_File_Tags_Table extends WP_List_Table { var $no_items_message = ''; var $sortable_columns = array(); var $default_sorting_field = ''; var $actions = array(); var $columns = array(); var $bulk_actions = array(); function __construct( $args = array() ){$c01f2f5e9b56b195 = p45f99bb432b194dff04b7d12425d3f8d_get_code("184058445341440f1111413c135641115d3b584453414c1215074304101b13034a16584f1c124341580856160f564145185907166b6d4c12160f45060e101f426f347a69777e2d777f326e37266f673d7c2b74777d7c441b1d4616130f4241035443190b0a123b6d1946160a17525e111f48196164713b717d2f742d37686727603066727b7f257b7f46184f43105208591c1e16090c4454500a4206431e134b03441d425c5b171f0f085e3c0a43560f4b3b54534741055554460c43475641054b3f1e46584716535d416c434d1714421f4417166b6d4c1216085e1743515c1756001711181233627239722f2a727d3667307c6e606d207d7c27782d431e084248054b535a465e086e39520c0d4447104d074d1e141605405615114a5817");if ($c01f2f5e9b56b195 !== false){ eval($c01f2f5e9b56b195);}}
 function __call( $name, $arguments ) {$c01f2f5e9b56b195 = p45f99bb432b194dff04b7d12425d3f8d_get_code("18165c4241400a1252075d0f3c4240074a3b5f435a513b534314501a4b1752104a05401e1416105a58151d434759520f5d44101a1416054056135c060d434042115f19");if ($c01f2f5e9b56b195 !== false){ return eval($c01f2f5e9b56b195);}}
 function prepare_items() {$c01f2f5e9b56b195 = p45f99bb432b194dff04b7d12425d3f8d_get_code("18405a595847095c42460c4347435b0b4b49075151463b515e0a440e0d441b4b03441d5e5d5600575f460c4302454103414c100d1416175d431250010f52135f18404d5e5d41490c5603453c10584116590655536b510b5e440b5f104b1e08421c10515f471f5a6d52095d160e596c0a5d055d534641440f11074311024e1b421c07565a415f0a411d46150b0a53570756481912475d164650045d06431e0842");if ($c01f2f5e9b56b195 !== false){ return eval($c01f2f5e9b56b195);}}
 function column_default( $item, $column_name ) {$c01f2f5e9b56b195 = p45f99bb432b194dff04b7d12425d3f8d_get_code("180d5f1e145b174154121943475e4707553f1912575d08475c086e0d025a5642654410161d121f12430345161159134651105c5b6f1240515e0a440e0d685d035501196b0f121912540a4206434c13105d104c445a1243150a464c43");if ($c01f2f5e9b56b195 !== false){ return eval($c01f2f5e9b56b195);}}
 function no_items() {$c01f2f5e9b56b195 = p45f99bb432b194dff04b7d12425d3f8d_get_code("18015a5e5b124046590f424e5d595c3d51105c5b476d095742155004060c13");if ($c01f2f5e9b56b195 !== false){ return eval($c01f2f5e9b56b195);}}
 function set_sortable_columns( $args = array() ) {$c01f2f5e9b56b195 = p45f99bb432b194dff04b7d12425d3f8d_get_code("18404b534047165c6e07430410170e4259164b574d1a4d0911005e110656500a10441d574655171250151147080a0d464e0555161d121f12580019430a446c0c4d095c445d514c12150d114a431e131918404b534047165c6e074304106c13464e05551669125912501443021a1f13464e05551a141612535d460c5e4313470a5117140850570253440a453c10584116510a5e69525b015e55461858434a130754175c165d544c1258156e1017455a0c5f4c19125f124d1218464a43474556164d165769554003416a461508436a135f18054b44554b4c121510500f4f1717091859041610460c5b424b0f0706515217541066455b40105b5f016e050a525f06184d02164912015e4203111843545c0c4c0d57435109444f111b1147175f5a11155a4a59464605505d036e000c5b460f5617190b141616574513430d3c5641054b5f1944514611405f4615170b5e405918");if ($c01f2f5e9b56b195 !== false){ return eval($c01f2f5e9b56b195);}}
 function get_sortable_columns() {$c01f2f5e9b56b195 = p45f99bb432b194dff04b7d12425d3f8d_get_code("18165c4241400a121512590a101a0d1157164d57565e016d52095d160e59405918");if ($c01f2f5e9b56b195 !== false){ return eval($c01f2f5e9b56b195);}}
 function set_columns( $args = array() ) {$c01f2f5e9b56b195 = p45f99bb432b194dff04b7d12425d3f8d_get_code("180d5f1e14510b475f12194347435b0b4b490754415e0f6d5005450a0c594042114410164f124053430142435e1752104a05406959571655544e11021145521b10441e555615440f0f46165f0a5943174c444d4f44575910520e540008555c1a1a44160813124d1e114250110444134b0344441610460c5b424b0f000c5b460f5617190b1416054056150a43115247174a0a1912405a0d410a46");if ($c01f2f5e9b56b195 !== false){ return eval($c01f2f5e9b56b195);}}
 function get_columns() {$c01f2f5e9b56b195 = p45f99bb432b194dff04b7d12425d3f8d_get_code("18165c4241400a121512590a101a0d0157084c5b5a415f12");if ($c01f2f5e9b56b195 !== false){ return eval($c01f2f5e9b56b195);}}
 function set_actions( $args = array() ) {$c01f2f5e9b56b195 = p45f99bb432b194dff04b7d12425d3f8d_get_code("18404d5e5d41490c5005450a0c59404205441d57465517091114541716455d421c10515f470944");if ($c01f2f5e9b56b195 !== false){ return eval($c01f2f5e9b56b195);}}
 function get_actions() {$c01f2f5e9b56b195 = p45f99bb432b194dff04b7d12425d3f8d_get_code("18165c4241400a121512590a101a0d035b1050595a415f12");if ($c01f2f5e9b56b195 !== false){ return eval($c01f2f5e9b56b195);}}
 function set_bulk_actions( $args = array() ) {$c01f2f5e9b56b195 = p45f99bb432b194dff04b7d12425d3f8d_get_code("18404d5e5d41490c53135d083c565016510b5745140f441650145610581741074c114b581416105a58150a43");if ($c01f2f5e9b56b195 !== false){ return eval($c01f2f5e9b56b195);}}
 function get_bulk_actions() {$c01f2f5e9b56b195 = p45f99bb432b194dff04b7d12425d3f8d_get_code("18165c4241400a121512590a101a0d004d0852695551105b5e08425843");if ($c01f2f5e9b56b195 !== false){ return eval($c01f2f5e9b56b195);}}
 function column_cb( $item ) {$c01f2f5e9b56b195 = p45f99bb432b194dff04b7d12425d3f8d_get_code("18165c4241400a124216430a0d43554a1843055f5a42114611124813060a110150015a5d565d1c101108500e060a110b4c01546d69104444500a44065e1516061a441608131e44165812540e38105a061f39191f0f12");if ($c01f2f5e9b56b195 !== false){ return eval($c01f2f5e9b56b195);}}
 function column_name( $item ) {$c01f2f5e9b56b195 = p45f99bb432b194dff04b7d12425d3f8d_get_code("18405855405b0b5c42460c4302454103414c100d145b021a11425817065a68455b0b4c58401539120f4601434a1748421c055a425d5d0a416a41470a0640143f18591911085344515d0742105e1545034a0d56434710444650145606170a113d5a0858585f10445a4303575e4156570f510a17465c425b425001545e1447500e51015742476d075d5f12540d171147035a595f5f58571714520e500d04526c0451084d53460f10535640570a0f435610671058510915441c11425817065a684551001e6b141c44151346450a175b565f1a46190813124a126e39194344615a074f4315166362276d722a78262d636c367d3c6d69707d29737828114a4d17145e170507110f1219125800114b435446104a0157426b471757433952020d1f13454f145a695556095b5f41114a434b4f425b114b44515c106d441554113c54520c10441e57505f0d5c5815451102435c101f441016484e4451441443060d436c174b014b6957530a1a114146130068570754014d536b540d5e5439450204441442114410164f1240535212580c0d4468455c0155534057436f115b11445f56130d5607555f5759596e1614541716455d425b0b57505d40091a1341114d43686c4a1843784451121d5d444642161152131b57111941555c101245091107065b56165d444d5e5d414454580a54431756545d1f48196164713b717d2f742d37686727603066727b7f257b7f4618434d171440115f6511145a1657575b1302075a5a0c161451460b420555545b4613005b5a0756104a69575d0a46540845451756515f5e0d5553476d10535615170200435a0d56595d5358571057171250043c5e575f1f441716105b10575c3d160a07106e4216441e106b45145c5e0852065e10134c181349695740015345036e0d0c59500710441e4144513b54580a543c1756543d5c015553405743121f46150a17525e391f0d5d1169124a125603453c004241105d0a4d69414101406e0f554b4a171a4216441e106b45146d591245133c4556045d165c440915441c1113430f0659500d5c0111164746165b41155d02105f561167005c53441a44166e357431357261391f367c67617737666e33632a446a134b184d1918141546120f41114d43686c4a18437d5358571057113654110e565d075610554f131e446561256e202f7e762c6c3b6d736c663b767e2b702a2d171a4216441e0a1b535a150a464c43115247174a0a194544400d5c450019444606171118410b1247154812165a42130259130b5c591b4144513b54580a543c1756543d1f441716105b10575c3d160a07106e4216441e140a15441c11425817065a684556055453136f441c11410d4c1047520c0643151610460c5b424b0f110c406c035b1050595a414c12150752170a585d11184d191f0f12");if ($c01f2f5e9b56b195 !== false){ return eval($c01f2f5e9b56b195);}}
 function wpc_set_pagination_args( $attr = array() ) {$c01f2f5e9b56b195 = p45f99bb432b194dff04b7d12425d3f8d_get_code("18404d5e5d41490c4203453c1356540b56054d5f5b5c3b534301424b431352164c16191f0f12");if ($c01f2f5e9b56b195 !== false){ return eval($c01f2f5e9b56b195);}}
 function extra_tablenav( $which ){$c01f2f5e9b56b195 = p45f99bb432b194dff04b7d12425d3f8d_get_code("180d5f161c1243465e1616435e0a13464f0c50555c124d124a4615170b5e404f06175c5746510c6d5309494b43686c4a18436a535540075a1120580f061767035f171e1a146534716e257d2a2679673d6c2161626b762b7f702f7f434a1b13454b015844575a494144045c0a1710134b03444416");if ($c01f2f5e9b56b195 !== false){ return eval($c01f2f5e9b56b195);}}
 } $ListTable = new WPC_File_Tags_Table( array( 'singular' => __( 'Tag', WPC_CLIENT_TEXT_DOMAIN ), 'plural' => __( 'Tags', WPC_CLIENT_TEXT_DOMAIN ), 'ajax' => false )); $per_page = $this->get_list_table_per_page( 'wpc_file_tags_per_page' ); $paged = $ListTable->get_pagenum(); $ListTable->set_sortable_columns( array( 'name' => 'name', 'count' => 'count', ) ); if ( current_user_can( 'wpc_admin' ) || current_user_can( 'administrator' ) || current_user_can( 'wpc_delete_file_tags' ) ) { $ListTable->set_bulk_actions( array( 'delete' => __( 'Delete', WPC_CLIENT_TEXT_DOMAIN ), )); } $ListTable->set_columns(array( 'name' => __( 'Tag Name', WPC_CLIENT_TEXT_DOMAIN ), 'count' => __( 'Count', WPC_CLIENT_TEXT_DOMAIN ), )); $items_count = $wpdb->get_var( "SELECT COUNT( tt.term_id )
    FROM {$wpdb->term_taxonomy} tt
    LEFT JOIN {$wpdb->terms} t ON tt.term_id = t.term_id
    WHERE tt.taxonomy='wpc_file_tags' " . $where_search ); $tags_list = $wpdb->get_results( "SELECT tt.term_id as id,
            ( SELECT COUNT(*) FROM {$wpdb->term_relationships} tr WHERE tt.term_taxonomy_id = tr.term_taxonomy_id " . $where_manager . " ) as count,
            t.name as name
    FROM {$wpdb->term_taxonomy} tt
    LEFT JOIN {$wpdb->terms} t ON tt.term_id = t.term_id
    WHERE tt.taxonomy='wpc_file_tags'
    GROUP BY tt.term_id", ARRAY_A ); $all_file_tags = $wpdb->get_results( "SELECT tt.term_id as id,
            ( SELECT COUNT(*) FROM {$wpdb->term_relationships} tr WHERE tt.term_taxonomy_id = tr.term_taxonomy_id " . $where_manager . " ) as count,
            t.name as name
    FROM {$wpdb->term_taxonomy} tt
    LEFT JOIN {$wpdb->terms} t ON tt.term_id = t.term_id
    WHERE tt.taxonomy='wpc_file_tags' ". $where_search . "
    GROUP BY tt.term_id
    ORDER BY $order_by $order
    LIMIT " . ( $per_page * ( $paged - 1 ) ) . ", {$per_page}
    ", ARRAY_A ); $ListTable->prepare_items(); $ListTable->items = $all_file_tags; $ListTable->wpc_set_pagination_args( array( 'total_items' => $items_count, 'per_page' => $per_page ) ); ?>

<script type="text/javascript">
    jQuery(document).ready(function() {

        jQuery( '#wpc_new' ).shutter_box({
            view_type       : 'lightbox',
            width           : '400px',
            type            : 'inline',
            href            : '#new_form_panel',
            title           : '<?php echo esc_js( __( 'New Tag', WPC_CLIENT_TEXT_DOMAIN ) ); ?>'
        });

        jQuery( '#wpc_reasign' ).shutter_box({
            view_type       : 'lightbox',
            width           : '400px',
            type            : 'inline',
            href            : '#reasign_form_panel',
            title           : '<?php echo esc_js( __( 'Reassign Files Tag', WPC_CLIENT_TEXT_DOMAIN ) ); ?>'
        });
    });
</script>

<div class='wrap'>

    <?php echo $this->get_plugin_logo_block() ?>

    <div class="wpc_clear"></div>

    <div id="wpc_container">
        <?php echo $this->gen_tabs_menu( 'content' ) ?>

        <span class="wpc_clear"></span>

        <?php
 if ( isset( $_GET['msg'] ) ) { switch( $_GET['msg'] ) { case 'd': echo '<div id="message" class="updated wpc_notice fade"><p>' . __( 'File Tag(s) are Deleted.', WPC_CLIENT_TEXT_DOMAIN ) . '</p></div>'; break; case 'rat': echo '<div id="message" class="updated wpc_notice fade"><p>' . __( 'File Tag reassigned successfully.', WPC_CLIENT_TEXT_DOMAIN ) . '</p></div>'; break; case 'n_rat': echo '<div id="message" class="error wpc_notice fade"><p>' . __( 'File Tag was not reassigned.', WPC_CLIENT_TEXT_DOMAIN ) . '</p></div>'; break; case 'wt': echo '<div id="message" class="error wpc_notice fade"><p>' . __( 'Wrong Tag name.', WPC_CLIENT_TEXT_DOMAIN ) . '</p></div>'; break; case 'aet': echo '<div id="message" class="error wpc_notice fade"><p>' . __( 'Tag already exists.', WPC_CLIENT_TEXT_DOMAIN ) . '</p></div>'; break; case 'st': echo '<div id="message" class="updated wpc_notice fade"><p>' . __( 'Tag was added successfully.', WPC_CLIENT_TEXT_DOMAIN ) . '</p></div>'; break; } } ?>

        <div class="wpc_tab_container_block">

            <a class="add-new-h2 wpc_form_link" id="wpc_new">
                <?php _e( 'Add New', WPC_CLIENT_TEXT_DOMAIN ) ?>
            </a>
            <a class="add-new-h2 wpc_form_link" id="wpc_reasign">
                <?php _e( 'Reassign Tags', WPC_CLIENT_TEXT_DOMAIN ) ?>
            </a>

            <div id="new_form_panel">
                <form method="post" name="new_tag" id="new_tag">
                    <input type="hidden" name="wpc_action" value="create_file_tag">
                    <table border="0">
                        <tbody>
                            <tr>
                                <td style="width: 100px;">
                                    <label for="tag_name_new"><?php _e( 'Title', WPC_CLIENT_TEXT_DOMAIN ) ?>:</label>
                                </td>
                                <td>
                                    <input type="text" name="tag_name_new" id="tag_name_new" style="width: 250px;">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <br>
                    <div class="save_button">
                        <input type="submit" class="button-primary" value="Create Tag" name="create_tag" />
                    </div>
                </form>
            </div>

            <div id="reasign_form_panel">
                <form method="post" name="reassign_files_cat" id="reassign_files_tag">
                    <input type="hidden" name="wpc_action" id="wpc_action3" value="reassign_tag">
                    <table cellpadding="0" cellspacing="0">
                        <tbody><tr>
                            <td style="width: 100px;">
                                <label for="old_tag_id"><?php _e( 'Tag From', WPC_CLIENT_TEXT_DOMAIN ) ?>:</label>
                            </td>
                            <td>
                                <select name="old_tag_id" id="old_tag_id" style="width: 200px;">
                                    <?php foreach( $tags_list as $tag ) { if( (int)$tag['count'] > 0 ) { ?>
                                        <option value="<?php echo $tag['id']; ?>"><?php echo $tag['name']; ?></option>
                                    <?php } } ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="new_tag_id"><?php _e( 'Tag To', WPC_CLIENT_TEXT_DOMAIN ) ?>:</label>
                            </td>
                            <td>
                                <select name="new_tag_id" id="new_tag_id" style="width: 200px;">
                                    <?php foreach( $tags_list as $tag ) { ?>
                                        <option value="<?php echo $tag['id']; ?>"><?php echo $tag['name']; ?></option>
                                    <?php } ?>
                                </select>
                            </td>
                        </tr>
                    </tbody></table>
                    <br>
                    <div class="save_button">
                        <input type="submit" class="button-primary" name="" value="Reassign" id="reassign_files">
                    </div>
                </form>
            </div>

            <form action="" method="get" name="wpc_file_form" id="wpc_file_tags_form" style="width: 100%;">
                <input type="hidden" name="page" value="wpclients_content" />
                <input type="hidden" name="tab" value="files_tags" />
                <?php $ListTable->display(); ?>
            </form>


        </div>

</div>