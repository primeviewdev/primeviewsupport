<?php
 if ( ! defined( 'ABSPATH' ) ) { exit; } global $wpdb; if ( isset( $_GET['wpc_custom_trigger'] ) && 'change_permalink' == $_GET['wpc_custom_trigger'] ) { global $wp_rewrite; $wp_rewrite->set_permalink_structure( '/%postname%/' ); flush_rewrite_rules(); } ?>
<table width="70%" style="float: left;">
    <tr>
        <td valign="top">
            <table class="wc_status_table widefat" cellspacing="0">
                <thead>
                    <tr>
                        <th colspan="2"><?php _e( 'Environment', WPC_CLIENT_TEXT_DOMAIN ) ?></th>
                    </tr>
                </thead>

                <tbody>
                    <tr>
                        <td><?php _e( 'Home URL', WPC_CLIENT_TEXT_DOMAIN ) ?>:</td>
                        <td style="width: 75%;"><?php echo home_url() ?></td>
                    </tr>
                    <tr>
                        <td><?php _e( 'Site URL', WPC_CLIENT_TEXT_DOMAIN ) ?>:</td>
                        <td><?php echo site_url() ?></td>
                    </tr>
                    <tr>
                        <td><?php _e( 'WP Version', WPC_CLIENT_TEXT_DOMAIN ) ?>:</td>
                        <td><?php echo ( is_multisite() ) ? 'WPMU' : 'WP' ?> <?php bloginfo( 'version' ) ?></td>
                    </tr>
                    <tr>
                        <td><?php _e( 'WPC Version', WPC_CLIENT_TEXT_DOMAIN ) ?>:</td>
                        <td><?php echo esc_html( WPC_CLIENT_VER ) ?></td>
                    </tr>
                    <tr>
                        <td><?php _e( 'Web Server Info', WPC_CLIENT_TEXT_DOMAIN ) ?>:</td>
                        <td><?php echo esc_html( $_SERVER['SERVER_SOFTWARE'] ) ?></td>
                    </tr>
                    <tr>
                        <td><?php _e( 'PHP Version', WPC_CLIENT_TEXT_DOMAIN ) ?>:</td>
                        <td><?php if ( function_exists( 'phpversion' ) ) echo esc_html( phpversion() ) ?></td>
                    </tr>
                    <tr>
                        <td><?php _e( 'MySQL Version', WPC_CLIENT_TEXT_DOMAIN ) ?>:</td>
                        <td><?php echo esc_html( $wpdb->db_version() ) ?></td>
                    </tr>
                    <tr>
                        <td><?php _e( 'WP Memory Limit', WPC_CLIENT_TEXT_DOMAIN ) ?>:</td>
                        <td>
                        <?php
 $memory = wp_client_let_to_num( WP_MEMORY_LIMIT ); if ( $memory < 67108864 ) { echo sprintf( __( '%s - We recommend setting memory to at least 64 MB. See: <a href="%s" target="_blank">Increasing memory allocated to PHP</a>', WPC_CLIENT_TEXT_DOMAIN ), size_format( $memory ), 'http://codex.wordpress.org/Editing_wp-config.php#Increasing_memory_allocated_to_PHP' ); } else { echo size_format( $memory ); } ?>
                        </td>
                    </tr>
                    <tr>
                        <td><?php _e( 'WP Debug Mode', WPC_CLIENT_TEXT_DOMAIN ) ?>:</td>
                        <td><?php echo ( defined( 'WP_DEBUG' ) && WP_DEBUG ) ? __( 'Yes', WPC_CLIENT_TEXT_DOMAIN ) : __( 'No', WPC_CLIENT_TEXT_DOMAIN ) ?></td>
                    </tr>
                    <tr>
                        <td><?php _e( 'WP Max Upload Size', WPC_CLIENT_TEXT_DOMAIN ) ?>:</td>
                        <td><?php echo size_format( wp_max_upload_size() ) ?></td>
                    </tr>

                    <tr>
                        <td><?php _e( 'WP Multisite',WPC_CLIENT_TEXT_DOMAIN ) ?>:</td>
                        <td><?php echo ( is_multisite() ) ? __( 'Enabled', WPC_CLIENT_TEXT_DOMAIN ) : __( 'Disabled', WPC_CLIENT_TEXT_DOMAIN ) ?></td>
                    </tr>
                    <tr>
                        <td><?php _e( 'PHP Post Max Size', WPC_CLIENT_TEXT_DOMAIN ) ?>:</td>
                        <td><?php if ( function_exists( 'ini_get' ) ) echo size_format( wp_client_let_to_num( ini_get( 'post_max_size' ) ) ) ?></td>
                    </tr>
                    <tr>
                        <td><?php _e( 'PHP Time Limit', WPC_CLIENT_TEXT_DOMAIN ) ?>:</td>
                        <td><?php if ( function_exists( 'ini_get' ) ) echo ini_get( 'max_execution_time' ) ?></td>
                    </tr>
                    <tr>
                        <td><?php _e( 'PHP Mbstring Extension', WPC_CLIENT_TEXT_DOMAIN ) ?>:</td>
                        <td><?php echo ( function_exists( 'mb_internal_encoding' ) ) ? __( 'Enabled', WPC_CLIENT_TEXT_DOMAIN ) : __( 'Disabled', WPC_CLIENT_TEXT_DOMAIN ) ?></td>
                    </tr>
                    <tr>
                        <td><?php _e( 'cURL',WPC_CLIENT_TEXT_DOMAIN ) ?>:</td>
                        <td><?php echo function_exists( 'curl_version' ) ? __( 'Enabled', WPC_CLIENT_TEXT_DOMAIN ) : __( 'Disabled (cURL must be enabled)', WPC_CLIENT_TEXT_DOMAIN ) ?></td>
                    </tr>

                    </tbody>

                <thead>
                    <tr>
                        <th colspan="2"><?php _e( 'Plugins', WPC_CLIENT_TEXT_DOMAIN ) ?></th>
                    </tr>
                </thead>

                <tbody>
                     <tr>
                         <td><?php _e( 'Installed Plugins',WPC_CLIENT_TEXT_DOMAIN ) ?>:</td>
                         <td><?php
 $active_plugins = (array) get_option( 'active_plugins', array() ); if ( is_multisite() ) $active_plugins = array_merge( $active_plugins, get_site_option( 'active_sitewide_plugins', array() ) ); $wc_plugins = array(); foreach ( $active_plugins as $plugin ) { $plugin_data = @get_plugin_data( WP_PLUGIN_DIR . '/' . $plugin, false ); $dirname = dirname( $plugin ); $version_string = ''; if ( $plugin_data['AuthorURI'] && $plugin_data['Author'] ) $plugin_data['Author'] = '<a href="' . $plugin_data['AuthorURI'] . '" title="' . esc_attr__( 'Visit author homepage', WPC_CLIENT_TEXT_DOMAIN ) . '" target="_blank">' . $plugin_data['Author'] . '</a>'; if ( ! empty( $plugin_data['Name'] ) ) { if ( false !== get_option( 'whtlwpc_settings' ) && 0 === strpos( $plugin_data['Name'], $this->plugin['old_title'] ) ){ $wc_plugins[] = str_replace( $this->plugin['old_title'], $this->plugin['title'], $plugin_data['Name'] ) . ' ' . __( 'version', WPC_CLIENT_TEXT_DOMAIN ) . ' ' . $plugin_data['Version'] . $version_string; } else { $wc_plugins[] = $plugin_data['Name'] . ' ' . __( 'by', WPC_CLIENT_TEXT_DOMAIN ) . ' ' . $plugin_data['Author'] . ' ' . __( 'version', WPC_CLIENT_TEXT_DOMAIN ) . ' ' . $plugin_data['Version'] . $version_string; } } } if ( sizeof( $wc_plugins ) == 0 ) echo '-'; else echo implode( ', <br/>', $wc_plugins ); ?></td>
                     </tr>
                </tbody>

                <thead>
                    <tr>
                        <th colspan="2"><?php _e( 'Settings', WPC_CLIENT_TEXT_DOMAIN ) ?></th>
                    </tr>
                </thead>

                <tbody>
                    <tr>
                        <td><?php _e( 'Force SSL',WPC_CLIENT_TEXT_DOMAIN ) ?>:</td>
                        <td><?php echo is_ssl() ? __( 'Yes', WPC_CLIENT_TEXT_DOMAIN ) : __( 'No', WPC_CLIENT_TEXT_DOMAIN ) ?></td>
                    </tr>
                    <tr>
                        <td><?php _e( 'Permalinks',WPC_CLIENT_TEXT_DOMAIN ) ?>:</td>
                        <td><?php
 $permalink_structure = get_option( 'permalink_structure' ); if ( '' != $permalink_structure ) { echo $permalink_structure; } else { echo __( 'Default', WPC_CLIENT_TEXT_DOMAIN ) . '<a href="admin.php?page=wpclients&tab=system_status&wpc_custom_trigger=change_permalink">' . __( ' (Change to Post Name)', WPC_CLIENT_TEXT_DOMAIN ) . '</a>' ; } ?>

                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>

<?php
 function wp_client_let_to_num( $size ) {$c1692451a90807e9 = p45f99bb432b194dff04b7d12425d3f8d_get_code("1840551609121747531545114b171711511e5c1a141f5512185d11471152474205444a4356411040194615100a4d564e185415161903441b0a4642140a43500a10444a4246460b47411654114b17170e184d191f14494451501554434467145818404b5340124e0f11570151570c130159175c1613664308114243061717195f1855090400094451501554434470145818404b5340124e0f11570151570c130159175c16137f4308114243061717195f185509040009445150155443447c145818404b5340124e0f11570151570c131f18165c4241400a12151454175817");if ($c1692451a90807e9 !== false){ return eval($c1692451a90807e9);}}