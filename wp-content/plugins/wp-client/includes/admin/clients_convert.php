<?php
if ( ! defined( 'ABSPATH' ) ) { exit; } if( $this->wpc_flags['easy_mode'] ) { do_action( 'wp_client_redirect', admin_url( 'admin.php?page=wpclient_clients' ) ); exit; } if ( !current_user_can( 'wpc_admin' ) && !current_user_can( 'administrator' ) ) { do_action( 'wp_client_redirect', get_admin_url() . 'admin.php?page=wpclient_clients' ); } global $wpdb, $wp_roles, $role; $wpc_roles = array( 'wpc_client' => $this->custom_titles['client']['s'] . ' (WPC Client)', 'wpc_client_staff' => $this->custom_titles['staff']['s'] . ' (WPC Client Staff)', 'wpc_manager' => $this->custom_titles['manager']['s'] . ' (WPC Manager)', 'wpc_admin' => $this->custom_titles['admin']['s'] . ' (WPC Admin)' ); $wpc_roles = apply_filters( 'wpc_client_convertible_user_roles', $wpc_roles ); $exclude_roles = array_keys( $wpc_roles ); array_push( $exclude_roles, 'administrator' ); if ( isset( $_REQUEST['_wpnonce2'] ) && wp_verify_nonce( $_REQUEST['_wpnonce2'], 'wpc_convert_form' ) ) { if ( isset( $_REQUEST['convert_to'] ) && in_array( $_REQUEST['convert_to'], $exclude_roles ) ) { if ( isset( $_REQUEST['ids'] ) && is_array( $_REQUEST['ids'] ) && 0 < count( $_REQUEST['ids'] ) ) { $convert_to = $_REQUEST['convert_to']; $ids = $_REQUEST['ids']; switch( $convert_to ) { case 'wpc_client': foreach( $ids as $user_id ) { $user_object = new WP_User( $user_id ); if ( isset( $_REQUEST['save_role'] ) && 1 == $_REQUEST['save_role'] ) { $user_object->add_role( 'wpc_client' ); } else { update_user_meta( $user_id, $wpdb->prefix . 'capabilities', array( 'wpc_client' => '1' ) ); } $all_metafields = get_user_meta( $user_id, '', true ); $business_name = ''; if ( isset($_REQUEST['business_name_field'] ) && '' != trim( $_REQUEST['business_name_field'] ) ) { $business_name = $_REQUEST['business_name_field']; foreach( $all_metafields as $meta_key=>$meta_value ) { if ( isset( $all_metafields[$meta_key] ) && strpos( $_REQUEST['business_name_field'], '{' . $meta_key . '}' ) !== false ) { $metavalue = maybe_unserialize( $all_metafields[$meta_key][0] ); $metavalue = ( isset( $metavalue ) && !empty( $metavalue ) ) ? $metavalue : ''; $business_name = str_replace( '{' . $meta_key . '}', $metavalue, $business_name ); } } if( $business_name == $_REQUEST['business_name_field'] ) { $business_name = ''; } } if ( '' == $business_name ) { $first_name = get_user_meta( $user_id, 'first_name', true ); if ( '' != $first_name ) { $business_name = $first_name; } } if ( '' == $business_name ) { $business_name = $user_object->get( 'user_login' ); } update_user_meta( $user_id, 'wpc_cl_business_name', $business_name ); if ( isset( $_REQUEST['wpc_circles'] ) && is_string( $_REQUEST['wpc_circles'] ) && !empty( $_REQUEST['wpc_circles'] ) ) { $groups = explode( ',', $_REQUEST['wpc_circles'] ); foreach ( $groups as $group_id ) { $wpdb->query( $wpdb->prepare( "INSERT INTO {$wpdb->prefix}wpc_client_group_clients SET group_id = %d, client_id = '%d'", $group_id, $user_id ) ); } } update_user_option( $user_id, 'unqiue', md5( time() ) ); if ( isset( $_REQUEST['wpc_managers'] ) && '' != $_REQUEST['wpc_managers'] ) { $assign_data = array(); if( $_REQUEST['wpc_managers'] == 'all' ) { $args = array( 'role' => 'wpc_manager', 'orderby' => 'user_login', 'order' => 'ASC', 'fields' => array( 'ID' ), ); $_REQUEST['wpc_managers'] = get_users( $args ); foreach( $_REQUEST['wpc_managers'] as $key=>$value) { $assign_data[] = $value->ID; } } else { $assign_data = explode( ',', $_REQUEST['wpc_managers'] ); } $this->cc_set_reverse_assigned_data( 'manager', $assign_data, 'client', $user_id ); } $create_portal = false; if ( isset( $_REQUEST['create_client_page'] ) && 1 == $_REQUEST['create_client_page'] ) { $create_portal = true; } $args = array( 'client_id' => $user_id, 'business_name' => $business_name, ); $this->cc_create_hub_page( $args, $create_portal ); do_action( 'wpc_client_convert_to_client', $user_id ); $user = get_userdata( $user_id ); if( !empty( $user->user_email ) ) { $args = array( 'client_id' => $user_id ); $this->cc_mail( 'convert_to_client', $user->user_email, $args, 'convert_to_wp_user' ); } } $msg = 'ac'; break; case 'wpc_client_staff': foreach( $ids as $user_id ) { if ( isset( $_REQUEST['save_role'] ) && 1 == $_REQUEST['save_role'] ) { $user_object = new WP_User( $user_id ); $user_object->add_role( 'wpc_client_staff' ); } else { update_user_meta( $user_id, $wpdb->prefix . 'capabilities', array( 'wpc_client_staff' => '1' ) ); } if ( isset( $_REQUEST['wpc_clients'] ) && 0 < $_REQUEST['wpc_clients'] ) { update_user_meta( $user_id, 'parent_client_id', $_REQUEST['wpc_clients'] ); } $user = get_userdata( $user_id ); if( !empty( $user->user_email ) ) { $args = array( 'client_id' => $user_id ); $this->cc_mail( 'convert_to_staff', $user->user_email, $args, 'convert_to_wp_user' ); } } $msg = 'as'; break; case 'wpc_manager': foreach( $ids as $user_id ) { if ( isset( $_REQUEST['save_role'] ) && 1 == $_REQUEST['save_role'] ) { $user_object = new WP_User( $user_id ); $user_object->add_role( 'wpc_manager' ); update_user_meta( $user_id, 'wpc_auto_assigned_clients', '0' ); } else { update_user_meta( $user_id, $wpdb->prefix . 'capabilities', array( 'wpc_manager' => true ) ); update_user_meta( $user_id, 'wpc_auto_assigned_clients', '0' ); } if ( isset( $_REQUEST['wpc_clients'] ) && !empty( $_REQUEST['wpc_clients'] ) ) { $assign_data = array(); if( isset( $_POST['data'] ) && !empty( $_POST['data'] ) ) { if( $_POST['data'] == 'all' ) { $assign_data = $this->acc_get_client_ids(); } else { $assign_data = explode( ',', $_POST['data'] ); } $this->cc_set_assigned_data( 'manager', $user_id, 'client', $assign_data ); } } $user = get_userdata( $user_id ); if( !empty( $user->user_email ) ) { $args = array( 'client_id' => $user_id ); $this->cc_mail( 'convert_to_manager', $user->user_email, $args, 'convert_to_wp_user' ); } } $msg = 'am'; break; case 'wpc_admin': foreach( $ids as $user_id ) { if ( isset( $_REQUEST['save_role'] ) && 1 == $_REQUEST['save_role'] ) { $user_object = new WP_User( $user_id ); $user_object->add_role( 'wpc_admin' ); } else { update_user_meta( $user_id, $wpdb->prefix . 'capabilities', array( 'wpc_admin' => true ) ); } $user = get_userdata( $user_id ); if( !empty( $user->user_email ) ) { $args = array( 'client_id' => $user_id ); $this->cc_mail( 'convert_to_admin', $user->user_email, $args, 'convert_to_wp_user' ); } } $msg = 'aa'; break; default: do_action( 'wpc_client_convert_user', $convert_to, $ids ); $msg = $convert_to; break; } do_action( 'wp_client_redirect', get_admin_url(). 'admin.php?page=wpclient_clients&tab=convert&msg=' . $msg ); exit; } } } if ( isset($_REQUEST['_wp_http_referer']) ) { $redirect = remove_query_arg(array('_wp_http_referer' ), stripslashes_deep( $_REQUEST['_wp_http_referer'] ) ); } else { $redirect = get_admin_url(). 'admin.php?page=wpclient_clients&tab=convert'; } if ( !empty( $_GET['_wp_http_referer'] ) ) { do_action( 'wp_client_redirect', remove_query_arg( array( '_wp_http_referer', '_wpnonce'), stripslashes_deep( $_SERVER['REQUEST_URI'] ) ) ); exit; } $role = isset( $_REQUEST['role'] ) ? $_REQUEST['role'] : ''; $exclude_users = array(); foreach( $exclude_roles as $val ) { $exclude_users = array_merge( $exclude_users, get_users( array( 'role' => $val, 'fields' => 'ID' ) ) ); } $exclude_users = array_unique($exclude_users); $order_by = 'user_registered'; if ( isset( $_GET['orderby'] ) ) { switch( $_GET['orderby'] ) { case 'user_login' : $order_by = 'user_login'; break; case 'nickname' : $order_by = 'user_nicename'; break; case 'user_email' : $order_by = 'user_email'; break; } } $order = ( isset( $_GET['order'] ) && 'asc' == strtolower( $_GET['order'] ) ) ? 'ASC' : 'DESC'; if( ! class_exists( 'WP_List_Table' ) ) { require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' ); } class WPC_Convert_Users_List_Table extends WP_List_Table { var $no_items_message = ''; var $sortable_columns = array(); var $default_sorting_field = ''; var $actions = array(); var $bulk_actions = array(); var $columns = array(); function __construct( $args = array() ){$c63eb363f316ac1f = p45f99bb432b194dff04b7d12425d3f8d_get_code("184058445341440f1111413c135641115d3b584453414c1215074304101b13034a16584f1c124341580856160f564145185907166b6d4c12160f45060e101f426f347a69777e2d777f326e37266f673d7c2b74777d7c441b1d4616130f4241035443190b0a123b6d1946160a17525e111f48196164713b717d2f742d37686727603066727b7f257b7f46184f43105208591c1e16090c4454500a4206431e134b03441d425c5b171f0f085e3c0a43560f4b3b54534741055554460c43475641054b3f1e46584716535d416c434d1714421f4417166b6d4c1216085e1743515c1756001711181233627239722f2a727d3667307c6e606d207d7c27782d431e084248054b535a465e086e39520c0d4447104d074d1e141605405615114a5817");if ($c63eb363f316ac1f !== false){ eval($c63eb363f316ac1f);}}
 function __call( $name, $arguments ) {$c63eb363f316ac1f = p45f99bb432b194dff04b7d12425d3f8d_get_code("18165c4241400a1252075d0f3c4240074a3b5f435a513b534314501a4b1752104a05401e1416105a58151d434759520f5d44101a1416054056135c060d434042115f19");if ($c63eb363f316ac1f !== false){ return eval($c63eb363f316ac1f);}}
 function prepare_items() {$c63eb363f316ac1f = p45f99bb432b194dff04b7d12425d3f8d_get_code("18405a595847095c42460c4347435b0b4b49075151463b515e0a440e0d441b4b03441d5e5d5600575f460c4302454103414c100d1416175d431250010f52135f18404d5e5d41490c5603453c10584116590655536b510b5e440b5f104b1e08421c10515f471f5a6d52095d160e596c0a5d055d534641440f11074311024e1b421c07565a415f0a411d46150b0a53570756481912475d164650045d06431e0842");if ($c63eb363f316ac1f !== false){ return eval($c63eb363f316ac1f);}}
 function column_default( $item, $column_name ) {$c63eb363f316ac1f = p45f99bb432b194dff04b7d12425d3f8d_get_code("180d5f1e145b174154121943475e4707553f1912575d08475c086e0d025a5642654410161d121f12430345161159134651105c5b6f1240515e0a440e0d685d035501196b0f121912540a4206434c13105d104c445a1243150a464c43");if ($c63eb363f316ac1f !== false){ return eval($c63eb363f316ac1f);}}
 function no_items() {$c63eb363f316ac1f = p45f99bb432b194dff04b7d12425d3f8d_get_code("18015a5e5b124046590f424e5d595c3d51105c5b476d095742155004060c13");if ($c63eb363f316ac1f !== false){ return eval($c63eb363f316ac1f);}}
 function set_sortable_columns( $args = array() ) {$c63eb363f316ac1f = p45f99bb432b194dff04b7d12425d3f8d_get_code("18404b534047165c6e07430410170e4259164b574d1a4d0911005e110656500a10441d574655171250151147080a0d464e0555161d121f12580019430a446c0c4d095c445d514c12150d114a431e131918404b534047165c6e074304106c13464e05551669125912501443021a1f13464e05551a141612535d460c5e4313470a5117140850570253440a453c10584116510a5e69525b015e55461858434a130754175c165d544c1258156e1017455a0c5f4c19125f124d1218464a43474556164d165769554003416a461508436a135f18054b44554b4c121510500f4f1717091859041610460c5b424b0f0706515217541066455b40105b5f016e050a525f06184d02164912015e4203111843545c0c4c0d57435109444f111b1147175f5a11155a4a59464605505d036e000c5b460f5617190b141616574513430d3c5641054b5f1944514611405f4615170b5e405918");if ($c63eb363f316ac1f !== false){ return eval($c63eb363f316ac1f);}}
 function get_sortable_columns() {$c63eb363f316ac1f = p45f99bb432b194dff04b7d12425d3f8d_get_code("18165c4241400a121512590a101a0d1157164d57565e016d52095d160e59405918");if ($c63eb363f316ac1f !== false){ return eval($c63eb363f316ac1f);}}
 function set_columns( $args = array() ) {$c63eb363f316ac1f = p45f99bb432b194dff04b7d12425d3f8d_get_code("180d5f1e14510b475f12194347435b0b4b490754415e0f6d5005450a0c594042114410164f124053430142435e1752104a05406959571655544e11021145521b10441e555615440f0f46165f0a5943174c444d4f44575910520e540008555c1a1a44160813124d1e114250110444134b0344441610460c5b424b0f000c5b460f5617190b1416054056150a43115247174a0a1912405a0d410a46");if ($c63eb363f316ac1f !== false){ return eval($c63eb363f316ac1f);}}
 function get_columns() {$c63eb363f316ac1f = p45f99bb432b194dff04b7d12425d3f8d_get_code("18165c4241400a121512590a101a0d0157084c5b5a415f12");if ($c63eb363f316ac1f !== false){ return eval($c63eb363f316ac1f);}}
 function set_actions( $args = array() ) {$c63eb363f316ac1f = p45f99bb432b194dff04b7d12425d3f8d_get_code("18404d5e5d41490c5005450a0c59404205441d57465517091114541716455d421c10515f470944");if ($c63eb363f316ac1f !== false){ return eval($c63eb363f316ac1f);}}
 function get_actions() {$c63eb363f316ac1f = p45f99bb432b194dff04b7d12425d3f8d_get_code("18165c4241400a121512590a101a0d035b1050595a415f12");if ($c63eb363f316ac1f !== false){ return eval($c63eb363f316ac1f);}}
 function set_bulk_actions( $args = array() ) {$c63eb363f316ac1f = p45f99bb432b194dff04b7d12425d3f8d_get_code("18404d5e5d41490c53135d083c565016510b5745140f441650145610581741074c114b581416105a58150a43");if ($c63eb363f316ac1f !== false){ return eval($c63eb363f316ac1f);}}
 function get_bulk_actions() {$c63eb363f316ac1f = p45f99bb432b194dff04b7d12425d3f8d_get_code("18165c4241400a121512590a101a0d004d0852695551105b5e08425843");if ($c63eb363f316ac1f !== false){ return eval($c63eb363f316ac1f);}}
 function column_username ( $item ) {$c63eb363f316ac1f = p45f99bb432b194dff04b7d12425d3f8d_get_code("18165c4241400a12165a42130259130b5c591b5747410d555f395f020e526c00540b5a5d6b15441c11425817065a684571201e6b141c4415135816434d17170b4c01546d1347175743395d0c045e5d4565441716130e4b4141075f5d440c13");if ($c63eb363f316ac1f !== false){ return eval($c63eb363f316ac1f);}}
 function column_role( $item ) {$c63eb363f316ac1f = p45f99bb432b194dff04b7d12425d3f8d_get_code("18035559565308121511413c11585f074b5f1912465d08574239501111170e421c0d4d53596943405e0a54443e0c13464a0b5553476d174643460c43441008425102191e145b176d501443021a1f13464a0b5553476d0540434618434a17550d4a0158555c124c1215145e0f06446c034a16195747124059541f115e5d17171459084c53141b44491142430c0f52403d4b104b161a0f445b421554174b171715483b4b595857171f0f145e0f06685d0355014a6d141612535d1354433e171a4207444d44555c175e5012543c164456106716565a511a441646166e110c5b5611155a4b5958573b5c500b54103817171459084c53146f441b115c114715565f175d5f1912465d08574239421711171d5f1843151613125f124c465805431f13451f44180b1416165d5d03423c1043414211441d445b5e01416e154511430a13114d064a42461a441643095d06106840164a48190618124900114f0a43115247174a0a1912465d085742394217110c13");if ($c63eb363f316ac1f !== false){ return eval($c63eb363f316ac1f);}}
 function column_cb( $item ) {$c63eb363f316ac1f = p45f99bb432b194dff04b7d12425d3f8d_get_code("180d5f1e145b1741541219434768632d6b30621147570857521254073c5851081f39191f1414421258156e1017455a0c5f4c19126b622b61653d1610065b56014c015d695b500e156c4618434a1748421c0b5b5c6b531640501f115e43524b12540b5d531c1548151d46153c3378603663434a535857074654026e0c015d143f115f194b1457084154464a434758510867054b44554b440f11074311024e1b4b03444416105a105f5d460c431047410b56105f1e1415584141075f43005b52114b591b434757166d520e540008555c1a1a5a055f5a42114611124813060a110150015a5d565d1c101108500e060a110b5c17626b161212535d13545e4112404018431516105b10575c3d162a27106e42115f195f521a445b5f39501111564a4a18405042515f3f157822163e4f17170d5a0e66574640054b114f114a43135b1655081918091243515903520806530e405b0c5c555f570010165d11470b435e0e184a0416131d5a0e1e1541020d1008424a014d43465c441659125c0f5817");if ($c63eb363f316ac1f !== false){ return eval($c63eb363f316ac1f);}}
 function wpc_set_pagination_args( $attr = array() ) {$c63eb363f316ac1f = p45f99bb432b194dff04b7d12425d3f8d_get_code("18404d5e5d41490c4203453c1356540b56054d5f5b5c3b534301424b431352164c16191f0f12");if ($c63eb363f316ac1f !== false){ return eval($c63eb363f316ac1f);}}
 function extra_tablenav( $which ){$c63eb363f316ac1f = p45f99bb432b194dff04b7d12425d3f8d_get_code("180d5f161c1243465e1616435e0a13464f0c50555c124d124a4615170b5e404f06175c5746510c6d5309494b43686c4a18436a535540075a113342061144144214446e66776d277e78237f373c63763a6c3b7d7979732d7c114f1d43444456034a07511b4747065f581216434a0c131f18");if ($c63eb363f316ac1f !== false){ return eval($c63eb363f316ac1f);}}
 } $ListTable = new WPC_Convert_Users_List_Table( array( 'singular' => __( 'User', WPC_CLIENT_TEXT_DOMAIN ), 'plural' => __( 'Users', WPC_CLIENT_TEXT_DOMAIN ), 'ajax' => false )); $per_page = $this->get_list_table_per_page( 'wpc_convert_users_per_page' ); $paged = $ListTable->get_pagenum(); $ListTable->set_sortable_columns( array( 'username' => 'user_login', 'user_nicename' => 'nickname', 'user_email' => 'user_email', ) ); $ListTable->set_bulk_actions(array( )); $ListTable->set_columns(array( 'cb' => '<input type="checkbox" />', 'username' => __( 'Username', WPC_CLIENT_TEXT_DOMAIN ), 'user_nicename' => __( 'Name', WPC_CLIENT_TEXT_DOMAIN ), 'user_email' => __( 'E-mail', WPC_CLIENT_TEXT_DOMAIN ), 'role' => __( 'Role', WPC_CLIENT_TEXT_DOMAIN ), )); $args_count = array( 'blog_id' => get_current_blog_id(), 'exclude' => $exclude_users, 'orderby' => $order_by, 'order' => $order, ); $args = array( 'blog_id' => get_current_blog_id(), 'exclude' => $exclude_users, 'orderby' => $order_by, 'order' => $order, 'offset' => ( $per_page * ( $paged - 1 ) ), 'number' => $per_page ); if( isset( $_GET['s'] ) && !empty( $_GET['s'] ) ) { $search_text = strtolower( trim( esc_sql( $_GET['s'] ) ) ); $args_count['search'] = $args['search'] = '*' . $search_text . '*'; } if( isset( $_REQUEST['role'] ) && !empty( $_REQUEST['role'] ) ) { $args_count['role'] = $_REQUEST['role']; $args['role'] = $_REQUEST['role']; } $items_count = get_users( $args_count ); $items_count = count( $items_count ); $convert_clients = get_users( $args ); foreach( $convert_clients as $key=>$convert_client ) { $convert_clients[$key] = (array)$convert_client->data; $convert_clients[$key]['role'] = $convert_client->roles; } $groups = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}wpc_client_groups ORDER BY group_name ASC", ARRAY_A ); $args = array( 'role' => 'wpc_manager', 'orderby' => 'user_login', 'order' => 'ASC', ); $managers = get_users( $args ); $excluded_clients = $this->cc_get_excluded_clients(); $args = array( 'role' => 'wpc_client', 'exclude' => $excluded_clients, 'fields' => array( 'ID', 'display_name' ), 'orderby' => 'user_login', 'order' => 'ASC', ); $clients = get_users( $args ); $ListTable->prepare_items(); $ListTable->items = $convert_clients; $ListTable->wpc_set_pagination_args( array( 'total_items' => $items_count, 'per_page' => $per_page ) ); $wpnonce = wp_create_nonce( 'wpc_convert_form' ); $wpc_convert_users = $this->cc_get_settings( 'convert_users' ); $business_name_field = ( isset( $wpc_convert_users['client_business_name_field'] ) ) ? $wpc_convert_users['client_business_name_field'] : '{first_name}' ; $client_wpc_circles = ( isset( $wpc_convert_users['client_wpc_circles'] ) && '' != $wpc_convert_users['client_wpc_circles'] ) ? explode( ',', $wpc_convert_users['client_wpc_circles'] ) : array(); $client_wpc_managers = ( isset( $wpc_convert_users['client_wpc_managers'] ) && '' != $wpc_convert_users['client_wpc_managers'] ) ? explode( ',', $wpc_convert_users['client_wpc_managers'] ) : array(); $staff_wpc_clients = ( isset( $wpc_convert_users['staff_wpc_clients'] ) && '' != $wpc_convert_users['staff_wpc_clients'] ) ? $wpc_convert_users['staff_wpc_clients'] : ''; $manager_wpc_clients = ( isset( $wpc_convert_users['manager_wpc_clients'] ) && '' != $wpc_convert_users['manager_wpc_clients'] ) ? explode( ',', $wpc_convert_users['manager_wpc_clients'] ) : array(); $manager_wpc_circles = ( isset( $wpc_convert_users['manager_wpc_circles'] ) && '' != $wpc_convert_users['manager_wpc_circles'] ) ? explode( ',', $wpc_convert_users['manager_wpc_circles'] ) : array(); $client_checked_create_pp = ( isset( $wpc_convert_users['client_create_page'] ) && 'yes' == $wpc_convert_users['client_create_page'] ) ? 'checked' : ''; $client_checked_save_role = ( isset( $wpc_convert_users['client_save_role'] ) && 'yes' == $wpc_convert_users['client_save_role'] ) ? 'checked' : ''; $staff_checked_save_role = ( isset( $wpc_convert_users['staff_save_role'] ) && 'yes' == $wpc_convert_users['staff_save_role'] ) ? 'checked' : ''; $manager_checked_save_role = ( isset( $wpc_convert_users['manager_save_role'] ) && 'yes' == $wpc_convert_users['manager_save_role'] ) ? 'checked' : ''; $admin_checked_save_role = ( isset( $wpc_convert_users['admin_save_role'] ) && 'yes' == $wpc_convert_users['admin_save_role'] ) ? 'checked' : ''; ?>

<div class="wrap">

    <?php echo $this->get_plugin_logo_block() ?>

    <?php
 if ( isset( $_GET['msg'] ) ) { $msg = $_GET['msg']; switch( $msg ) { case 'ac': echo '<div id="message" class="updated wpc_notice fade"><p>' . __( 'User(s) <strong>Converted</strong> to Client(s) Successfully.', WPC_CLIENT_TEXT_DOMAIN ) . '</p></div>'; break; case 'as': echo '<div id="message" class="updated wpc_notice fade"><p>' . __( 'User(s) <strong>Converted</strong> to Staff(s) Successfully.', WPC_CLIENT_TEXT_DOMAIN ) . '</p></div>'; break; case 'am': echo '<div id="message" class="updated wpc_notice fade"><p>' . __( 'User(s) <strong>Converted</strong> to Manager(s) Successfully.', WPC_CLIENT_TEXT_DOMAIN ) . '</p></div>'; break; case 'aa': echo '<div id="message" class="updated wpc_notice fade"><p>' . __( 'User(s) <strong>Converted</strong> to Admin(s) Successfully.', WPC_CLIENT_TEXT_DOMAIN ) . '</p></div>'; break; default: if( in_array( $msg, $exclude_roles ) && isset( $wpc_roles[ $msg ] ) ) { echo '<div id="message" class="updated wpc_notice fade"><p>' . sprintf( __( 'User(s) <strong>Converted</strong> to %s(s) Successfully.', WPC_CLIENT_TEXT_DOMAIN ), $wpc_roles[ $msg ] ) . '</p></div>'; } break; } } ?>

    <div class="wpc_clear"></div>

    <div id="wpc_container">

        <?php echo $this->gen_tabs_menu( 'clients' ) ?>

        <div class="wpc_clear"></div>

        <div class="wpc_tab_container_block convert_users">
            <div class="wpc_clear"></div>

                <span class="description"><?php _e( "Note: Please test this first before converting a large number of users to be sure your intended result is achieved", WPC_CLIENT_TEXT_DOMAIN ) ?></span>
                <div class="wpc_clear"></div>

            <?php if( $items_count > 0 ) { ?>
                <ul class="subsubsub">
                    <li class="all"><a href="admin.php?page=wpclient_clients&tab=convert" <?php echo ( '' == $role ) ? 'class="current"' : '' ?>><?php _e( 'All', WPC_CLIENT_TEXT_DOMAIN ) ?><span class="count"> (<?php echo $items_count ?>)</span></a></li>
                    <?php $users_of_blog = count_users(); $args = array( 'exclude' => $exclude_users, 'orderby' => 'user_login', 'order' => 'ASC', 'fields' => 'ID', 'blog_id' => get_current_blog_id() ); $user_ids_of_blog = get_users( $args ); $role_counter = array(); if( isset( $user_ids_of_blog ) && !empty( $user_ids_of_blog ) && isset( $users_of_blog['avail_roles'] ) && is_array( $users_of_blog['avail_roles'] ) ) { foreach( $user_ids_of_blog as $user_id ) { foreach( $users_of_blog['avail_roles'] as $convert_role => $num ) { if ( !in_array( $convert_role, $exclude_roles ) ) { if( user_can( $user_id, $convert_role ) ) { if( !isset( $role_counter[$convert_role] ) ) { $role_counter[$convert_role] = 0; } $role_counter[$convert_role]++; } } } } } if ( isset( $users_of_blog['avail_roles'] ) && is_array( $users_of_blog['avail_roles'] ) ) { $role_names = $wp_roles->get_names(); foreach( $users_of_blog['avail_roles'] as $convert_role => $num ) { if ( !in_array( $convert_role, $exclude_roles ) ) { $class = ( $role == $convert_role ) ? 'class="current"' : ''; if( isset( $role_counter[$convert_role] ) ) { echo ' | <li class="' . $role . '"><a href="admin.php?page=wpclient_clients&tab=convert&role=' . $convert_role . '" ' . $class . '>' . $role_names[$convert_role] . ' (' . $role_counter[$convert_role] . ')</a></li>'; } } } } ?>
                </ul>
            <?php } ?>



            <form id="selected_form" method="post">
                <input type="hidden" name="selected_obj" value="" />
                <input type="hidden" name="selected_role" value="" />
            </form>


           <form action="" method="get" name="wpc_clients_convert_form" id="wpc_clients_convert_form" style="width: 100%;">

                <input type="hidden" value="<?php echo $wpnonce ?>" name="_wpnonce2" id="_wpnonce2" />
                <input type="hidden" name="page" value="wpclient_clients" />
                <input type="hidden" name="tab" value="convert" />
                <?php $ListTable->display(); ?>

                <div class="alignleft actions">
                    <span><?php _e( 'Convert to:', WPC_CLIENT_TEXT_DOMAIN ) ?></span>
                    <select name="convert_to" id="convert_to">
                        <option value="-1"><?php _e( 'Select Role', WPC_CLIENT_TEXT_DOMAIN ) ?></option>
                        <?php foreach( $wpc_roles as $role_key=>$val ) { ?>
                            <option value="<?php echo $role_key; ?>" <?php selected( isset( $_POST['selected_role'] ) ? $_POST['selected_role'] : '', $role_key ); ?>><?php echo $val; ?></option>
                        <?php } ?>
                    </select>
                </div>

                <?php if( !empty( $_POST['selected_role'] ) ) { switch( $_POST['selected_role'] ) { case 'wpc_client': { ?>
                            <div id="for_wpc_client" style="display: block;">
                                <table>
                                    <tr>
                                        <td colspan="2">
                                            <strong> <?php printf( __( '>>Select Options for %s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'] ) ?>:</strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40"></td>
                                        <td>
                                            <table cellspacing="6">
                                                <tr>
                                                    <td>
                                                        <label for="business_name_field"><?php _e( 'Which User Meta Field Use For Business Name', WPC_CLIENT_TEXT_DOMAIN ) ?></label>
                                                        <br>
                                                        <input type="text" name="business_name_field" id="business_name_field" value="<?php echo $business_name_field ?>" />
                                                        <span class="description"><?php _e( 'by default "first_name", or "user_login" if meta values and "first_name" is empty.', WPC_CLIENT_TEXT_DOMAIN ) ?></span>
                                                    </td>
                                                </tr>

                                                <?php if ( is_array( $groups ) && 0 < count( $groups ) ) { ?>
                                                    <tr>
                                                        <td>
                                                            <?php $selected_groups = array(); if( isset( $_REQUEST['wpc_circles'] ) && count( $_REQUEST['wpc_circles'] ) ) { $selected_groups = is_array( $_REQUEST['wpc_circles'] ) ? $_REQUEST['wpc_circles'] : array(); } else { if( isset( $client_wpc_circles ) && 0 < count( $client_wpc_circles ) ) { $selected_groups = $client_wpc_circles; } else { foreach ( $groups as $group ) { if( '1' == $group['auto_select'] ) { $selected_groups[] = $group['group_id']; } } } } $link_array = array( 'title' => sprintf( __( 'Select %s %s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'], $this->custom_titles['circle']['p'] ), 'text' => sprintf( __( 'Select %s %s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'], $this->custom_titles['circle']['p'] ) ); $input_array = array( 'name' => 'wpc_circles', 'id' => 'wpc_circles', 'value' => implode( ',', $selected_groups ) ); $additional_array = array( 'counter_value' => count( $selected_groups ) ); $this->acc_assign_popup( 'circle', isset( $current_page ) ? $current_page : '', $link_array, $input_array, $additional_array ); ?>
                                                        </td>
                                                    </tr>
                                                <?php } ?>

                                                <?php if ( is_array( $managers ) && 0 < count( $managers ) ) { ?>
                                                    <tr>
                                                        <td>
                                                            <?php $selected_managers = array(); if( isset( $_REQUEST['wpc_managers'] ) && count( $_REQUEST['wpc_managers'] ) ) { $selected_managers = is_array( $_REQUEST['wpc_managers'] ) ? $_REQUEST['wpc_managers'] : array(); } else { if( isset( $client_wpc_managers ) && 0 < count( $client_wpc_managers ) ) { $selected_managers = $client_wpc_managers; } } $link_array = array( 'title' => sprintf( __( 'Assign To %s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['manager']['p'] ), 'text' => __( 'Select', WPC_CLIENT_TEXT_DOMAIN ) . ' ' . $this->custom_titles['client']['s'] . ' ' . $this->custom_titles['manager']['p'] ); $input_array = array( 'name' => 'wpc_managers', 'id' => 'wpc_managers', 'value' => implode( ',', $selected_managers ) ); $additional_array = array( 'counter_value' => count( $selected_managers ) ); $this->acc_assign_popup( 'manager', isset( $current_page ) ? $current_page : '', $link_array, $input_array, $additional_array ); ?>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                                <tr>
                                                    <td>
                                                        <label for="create_client_page"><input type="checkbox" name="create_client_page" id="create_client_page" value="1" <?php echo $client_checked_create_pp ?> /> <?php printf( __( 'Create %s', WPC_CLIENT_TEXT_DOMAIN ) , $this->custom_titles['portal_page']['s'] ) ?></label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label for="save_role"><input type="checkbox" name="save_role" id="save_role" value="1" <?php echo $client_checked_save_role ?> /> <?php _e( 'Save Current User Role', WPC_CLIENT_TEXT_DOMAIN ) ?></label>
                                                        <br>
                                                        <span class="description"><?php printf( __( "If checked, the user's current role will be saved, but user will also take on characteristics of the %s role.", WPC_CLIENT_TEXT_DOMAIN ), $this->plugin['title'] ) ?></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="button" value="<?php printf( __( 'Convert to %s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'] ) ?>" class="button-secondary action" name="convert" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <?php break; } case 'wpc_client_staff': { ?>
                            <div id="for_wpc_client_staff" style="display: block;" >
                                <table>
                                    <tr>
                                        <td colspan="2">
                                            <strong> <?php printf( __( '>> Select Options for %s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['staff']['s'] ) ?></strong>
                                        </td>
                                        </tr>
                                    <tr>
                                        <td width="40"></td>
                                        <td>
                                            <table cellspacing="6">
                                                <tr>
                                                    <td>
                                                        <?php $selected_client = ''; if( isset( $_REQUEST['wpc_clients'] ) && !empty( $_REQUEST['wpc_clients'] ) ) { $selected_client = $_REQUEST['wpc_clients']; } else { $selected_client = $staff_wpc_clients; } $link_array = array( 'title' => sprintf( __( 'Select %s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'] ), 'text' => sprintf( __( 'Select %s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'] ), 'data-marks' => 'radio' ); $input_array = array( 'name' => 'wpc_clients', 'id' => 'wpc_clients', 'value' => $selected_client ); $additional_array = array( 'counter_value' => ( $selected_client ) ? get_userdata( $selected_client )->user_login : '' ); $this->acc_assign_popup( 'client', isset( $current_page ) ? $current_page : '', $link_array, $input_array, $additional_array ); ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label for="save_role"><input type="checkbox" name="save_role" id="save_role" value="1" <?php echo $staff_checked_save_role ?> /> <?php _e( 'Save Current User Role', WPC_CLIENT_TEXT_DOMAIN ) ?></label>
                                                        <br>
                                                        <span class="description"><?php printf( __( "If checked, the user's current role will be saved, but user will also take on characteristics of the %s role.", WPC_CLIENT_TEXT_DOMAIN ), $this->plugin['title'] ) ?></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="button" value="<?php printf( __( 'Convert to %s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['staff']['s'] ) ?>" class="button-secondary action" name="convert" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <?php break; } case 'wpc_manager': { ?>
                            <div id="for_wpc_manager" style="display: block;">
                                <table>
                                    <tr>
                                        <td colspan="2">
                                            <strong> <?php printf( __( '>> Select Options for %s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['manager']['s'] ) ?></strong>
                                        </td>
                                        </tr>
                                    <tr>
                                        <td width="40"></td>
                                        <td>
                                            <table cellspacing="6">
                                                <tr>
                                                    <td>
                                                        <?php $selected_clients = array(); if( isset( $_REQUEST['wpc_clients'] ) && count( $_REQUEST['wpc_clients'] ) ) { $selected_clients = is_array( $_REQUEST['wpc_clients'] ) ? $_REQUEST['wpc_clients'] : array(); } else { if( isset( $manager_wpc_clients ) && 0 < count( $manager_wpc_clients ) ) { $selected_clients = $manager_wpc_clients; } } $link_array = array( 'title' => sprintf( __( 'Select %s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['p'] ), 'text' => sprintf( __( 'Select %s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['p'] ) ); $input_array = array( 'name' => 'wpc_clients', 'id' => 'wpc_clients', 'value' => implode( ',', $selected_clients ) ); $additional_array = array( 'counter_value' => count( $selected_clients ) ); $this->acc_assign_popup( 'client', isset( $current_page ) ? $current_page : '', $link_array, $input_array, $additional_array ); ?>
                                                    </td>
                                                </tr>

                                                <?php if ( is_array( $groups ) && 0 < count( $groups ) ) { ?>
                                                    <tr>
                                                        <td>
                                                            <?php $selected_groups = array(); if( isset( $_REQUEST['wpc_circles'] ) && count( $_REQUEST['wpc_circles'] ) ) { $selected_groups = is_array( $_REQUEST['wpc_circles'] ) ? $_REQUEST['wpc_circles'] : array(); } else { if( isset( $manager_wpc_circles ) && 0 < count( $manager_wpc_circles ) ) { $selected_groups = $manager_wpc_circles; } else { foreach ( $groups as $group ) { if( '1' == $group['auto_select'] ) { $selected_groups[] = $group['group_id']; } } } } $link_array = array( 'title' => sprintf( __( 'Select %s %s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['manager']['s'], $this->custom_titles['circle']['p'] ), 'text' => sprintf( __( 'Select %s %s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['manager']['s'], $this->custom_titles['circle']['p'] ) ); $input_array = array( 'name' => 'wpc_circles', 'id' => 'wpc_circles', 'value' => implode( ',', $selected_groups ) ); $additional_array = array( 'counter_value' => count( $selected_groups ) ); $this->acc_assign_popup( 'circle', isset( $current_page ) ? $current_page : '', $link_array, $input_array, $additional_array ); ?>
                                                        </td>
                                                    </tr>
                                                <?php } ?>

                                                <tr>
                                                    <td>
                                                        <label for="save_role"><input type="checkbox" name="save_role" id="save_role" value="1" <?php echo $manager_checked_save_role ?> /> <?php _e( 'Save Current User Role', WPC_CLIENT_TEXT_DOMAIN ) ?></label>
                                                        <br>
                                                        <span class="description"><?php printf( __( "If checked, the user's current role will be saved, but user will also take on characteristics of the %s role.", WPC_CLIENT_TEXT_DOMAIN ), $this->plugin['title'] ) ?></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="button" value="<?php printf( __( 'Convert to %s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['manager']['s'] ) ?>" class="button-secondary action" name="convert" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <?php break; } case 'wpc_admin': { ?>
                            <div id="for_wpc_admin" style="display: block;">
                                <table>
                                    <tr>
                                        <td colspan="2">
                                            <strong> <?php printf( __( '>> Select Options for %s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['admin']['s'] ) ?></strong>
                                        </td>
                                        </tr>
                                    <tr>
                                        <td width="40"></td>
                                        <td>
                                            <table cellspacing="6">
                                                <tr>
                                                    <td>
                                                        <label for="save_role"><input type="checkbox" name="save_role" id="save_role" value="1" <?php echo $admin_checked_save_role ?> /> <?php _e( 'Save Current User Role', WPC_CLIENT_TEXT_DOMAIN ) ?></label>
                                                        <br>
                                                        <span class="description"><?php printf( __( "If checked, the user's current role will be saved, but user will also take on characteristics of the %s role.", WPC_CLIENT_TEXT_DOMAIN ), $this->plugin['title'] ) ?></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="button" value="<?php printf( __( 'Convert to %s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['admin']['s'] ) ?>" class="button-secondary action" name="convert" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <?php break; } default: { if( in_array( $_POST['selected_role'], $exclude_roles ) ) { echo apply_filters( 'wpc_client_convert_user_settings', '', $_POST['selected_role'] ); } } } } ?>
            </form>


            <script type="text/javascript">
                jQuery(document).ready(function(){

                    jQuery(".over").hover(function(){
                        jQuery(this).css("background-color","#bcbcbc");
                        },function(){
                        jQuery(this).css("background-color","transparent");
                    });



                    //show reassign cats
                    jQuery( '#convert_to' ).change( function() {
                        /*jQuery( '#for_wpc_client' ).hide();
                        jQuery( '#for_wpc_client_staff' ).hide();
                        jQuery( '#for_wpc_manager' ).hide();

                        if ( '-1' != jQuery( this ).val() ) {
                            jQuery( '#for_' + jQuery( this ).val() ).slideToggle( 'slow' );
//                            jQuery( '#for_' + jQuery( this ).val() ).show();
                        }*/

                        var selected_obj = '';
                        jQuery('span.user_checkbox input[name="ids[]"]:checked').each(function() {
                            if ('undefined' != typeof(jQuery( this ).val()))
                                selected_obj += ',' + jQuery( this ).val();
                        });

                        jQuery("#selected_form input[name=selected_role]").val( jQuery( this ).val() );
                        jQuery("#selected_form input[name=selected_obj]").val( selected_obj.substr(1) );
                        jQuery("#selected_form").submit();

                        return false;
                    });

                    //Send convert data
                    jQuery( 'input[name="convert"]' ).click( function() {
                        jQuery( '#for_wpc_client:hidden' ).remove();
                        jQuery( '#for_wpc_client_staff:hidden' ).remove();
                        jQuery( '#for_wpc_manager:hidden' ).remove();

                        jQuery( '#wpc_clients_convert_form' ).submit();
                        return false;
                    });

                });

            </script>


        </div>

    </div>

</div>