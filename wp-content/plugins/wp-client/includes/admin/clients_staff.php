<?php
if ( ! defined( 'ABSPATH' ) ) { exit; } if( $this->wpc_flags['easy_mode'] ) { do_action( 'wp_client_redirect', admin_url( 'admin.php?page=wpclient_clients' ) ); exit; } if ( !current_user_can( 'wpc_admin' ) && !current_user_can( 'administrator' ) && !current_user_can( 'wpc_add_staff' ) ) { do_action( 'wp_client_redirect', get_admin_url() . 'admin.php?page=wpclient_clients' ); } if ( isset($_REQUEST['_wp_http_referer']) ) { $redirect = remove_query_arg(array('_wp_http_referer' ), stripslashes_deep( $_REQUEST['_wp_http_referer'] ) ); } else { $redirect = get_admin_url(). 'admin.php?page=wpclient_clients&tab=staff'; } if ( isset( $_GET['action'] ) ) { switch ( $_GET['action'] ) { case 'delete': case 'delete_from_blog': case 'delete_mu': $clients_id = array(); if ( isset( $_REQUEST['id'] ) ) { check_admin_referer( 'wpc_staff_delete' . $_REQUEST['id'] . get_current_user_id() ); $clients_id = (array) $_REQUEST['id']; } elseif( isset( $_REQUEST['item'] ) ) { check_admin_referer( 'bulk-' . sanitize_key( $this->custom_titles['staff']['p'] ) ); $clients_id = $_REQUEST['item']; } if ( count( $clients_id ) ) { foreach ( $clients_id as $client_id ) { $custom_fields = $this->cc_get_settings( 'custom_fields' ); if( isset( $custom_fields ) && !empty( $custom_fields ) ) { foreach( $custom_fields as $key=>$value ) { if( isset( $value['type'] ) && 'file' == $value['type'] ) { if ( isset( $value['nature'] ) && ( 'staff' == $value['nature'] || 'both' == $value['nature'] ) ) { $filedata = get_user_meta( $client_id, $key, true ); if ( !empty( $filedata ) && isset( $filedata['filename'] ) ) { $filepath = $this->get_upload_dir( 'wpclient/_custom_field_files/' . $key . '/' ) . $filedata['filename']; if ( file_exists( $filepath ) ) { unlink( $filepath ); } } } } } } if( $_GET['action'] == 'delete_mu' ) { wpmu_delete_user( $client_id ); } else { wp_delete_user( $client_id ); } } do_action( 'wp_client_redirect', add_query_arg( 'msg', 'd', $redirect ) ); exit; } do_action( 'wp_client_redirect', $redirect ); exit; break; case 'temp_password': $staff_ids = array(); if ( isset( $_REQUEST['id'] ) ) { check_admin_referer( 'staff_temp_password' . $_REQUEST['id'] . get_current_user_id() ); $staff_ids = ( is_array( $_REQUEST['id'] ) ) ? $_REQUEST['id'] : (array) $_REQUEST['id']; } elseif( isset( $_REQUEST['item'] ) ) { check_admin_referer( 'bulk-' . sanitize_key( $this->custom_titles['staff']['p'] ) ); $staff_ids = $_REQUEST['item']; } foreach ( $staff_ids as $staff_id ) { $this->set_temp_password( $staff_id ); } if( 1 < count( $staff_ids ) ) { do_action( 'wp_client_redirect', add_query_arg( 'msg', 'pass_s', $redirect ) ); } else if( 1 === count( $staff_ids ) ) { do_action( 'wp_client_redirect', add_query_arg( 'msg', 'pass', $redirect ) ); } else { do_action( 'wp_client_redirect', $redirect ); } exit; } } if ( !empty( $_GET['_wp_http_referer'] ) ) { do_action( 'wp_client_redirect', remove_query_arg( array( '_wp_http_referer', '_wpnonce'), stripslashes_deep( $_SERVER['REQUEST_URI'] ) ) ); exit; } global $wpdb; $where_clause = ''; if( !empty( $_GET['s'] ) ) { $where_clause = $this->get_prepared_search( $_GET['s'], array( 'u.user_login', 'u.user_email', 'um2.meta_value', 'um3.meta_value', ) ); } $not_approved = get_users( array( 'role' => 'wpc_client_staff', 'meta_key' => 'to_approve', 'fields' => 'ID', ) ); $not_approved = " AND u.ID NOT IN ('" . implode( ',', $not_approved ) . "')"; $order = ( isset( $_GET['order'] ) && 'asc' == strtolower( $_GET['order'] ) ) ? 'ASC' : 'DESC'; $order_by = 'u.user_registered ' . $order; if ( isset( $_GET['orderby'] ) ) { switch( $_GET['orderby'] ) { case 'username' : $order_by = 'u.user_login ' . $order; break; case 'name' : $order_by = 'um2.meta_value ' . $order . ', um3.meta_value ' . $order; break; case 'email' : $order_by = 'u.user_email ' . $order; break; case 'client' : $client_ids = $wpdb->get_col("SELECT meta_value FROM {$wpdb->usermeta} WHERE meta_key = 'parent_client_id'"); if( count( $client_ids ) ) { $client_ids = $wpdb->get_col("SELECT ID FROM {$wpdb->users} WHERE ID IN ('" . implode( "','", $client_ids ) . "') ORDER BY user_login $order"); $order_by = "FIELD( parent_client_id, '" . implode( "','", $client_ids ) . "', '' )"; } break; } } if( ! class_exists( 'WP_List_Table' ) ) { require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' ); } class WPC_Staff_List_Table extends WP_List_Table { var $no_items_message = ''; var $sortable_columns = array(); var $default_sorting_field = ''; var $actions = array(); var $bulk_actions = array(); var $columns = array(); function __construct( $args = array() ){$c85806352bc480dd = p45f99bb432b194dff04b7d12425d3f8d_get_code("184058445341440f1111413c135641115d3b584453414c1215074304101b13034a16584f1c124341580856160f564145185907166b6d4c12160f45060e101f426f347a69777e2d777f326e37266f673d7c2b74777d7c441b1d4616130f4241035443190b0a123b6d1946160a17525e111f48196164713b717d2f742d37686727603066727b7f257b7f46184f43105208591c1e16090c4454500a4206431e134b03441d425c5b171f0f085e3c0a43560f4b3b54534741055554460c43475641054b3f1e46584716535d416c434d1714421f4417166b6d4c1216085e1743515c1756001711181233627239722f2a727d3667307c6e606d207d7c27782d431e084248054b535a465e086e39520c0d4447104d074d1e141605405615114a5817");if ($c85806352bc480dd !== false){ eval($c85806352bc480dd);}}
 function __call( $name, $arguments ) {$c85806352bc480dd = p45f99bb432b194dff04b7d12425d3f8d_get_code("18165c4241400a1252075d0f3c4240074a3b5f435a513b534314501a4b1752104a05401e1416105a58151d434759520f5d44101a1416054056135c060d434042115f19");if ($c85806352bc480dd !== false){ return eval($c85806352bc480dd);}}
 function prepare_items() {$c85806352bc480dd = p45f99bb432b194dff04b7d12425d3f8d_get_code("18405a595847095c42460c4347435b0b4b49075151463b515e0a440e0d441b4b03441d5e5d5600575f460c4302454103414c100d1416175d431250010f52135f18404d5e5d41490c5603453c10584116590655536b510b5e440b5f104b1e08421c10515f471f5a6d52095d160e596c0a5d055d534641440f11074311024e1b421c07565a415f0a411d46150b0a53570756481912475d164650045d06431e0842");if ($c85806352bc480dd !== false){ return eval($c85806352bc480dd);}}
 function column_default( $item, $column_name ) {$c85806352bc480dd = p45f99bb432b194dff04b7d12425d3f8d_get_code("180d5f1e145b174154121943475e4707553f1912575d08475c086e0d025a5642654410161d121f12430345161159134651105c5b6f1240515e0a440e0d685d035501196b0f121912540a4206434c13105d104c445a1243150a464c43");if ($c85806352bc480dd !== false){ return eval($c85806352bc480dd);}}
 function no_items() {$c85806352bc480dd = p45f99bb432b194dff04b7d12425d3f8d_get_code("18015a5e5b124046590f424e5d595c3d51105c5b476d095742155004060c13");if ($c85806352bc480dd !== false){ return eval($c85806352bc480dd);}}
 function set_sortable_columns( $args = array() ) {$c85806352bc480dd = p45f99bb432b194dff04b7d12425d3f8d_get_code("18404b534047165c6e07430410170e4259164b574d1a4d0911005e110656500a10441d574655171250151147080a0d464e0555161d121f12580019430a446c0c4d095c445d514c12150d114a431e131918404b534047165c6e074304106c13464e05551669125912501443021a1f13464e05551a141612535d460c5e4313470a5117140850570253440a453c10584116510a5e69525b015e55461858434a130754175c165d544c1258156e1017455a0c5f4c19125f124d1218464a43474556164d165769554003416a461508436a135f18054b44554b4c121510500f4f1717091859041610460c5b424b0f0706515217541066455b40105b5f016e050a525f06184d02164912015e4203111843545c0c4c0d57435109444f111b1147175f5a11155a4a59464605505d036e000c5b460f5617190b141616574513430d3c5641054b5f1944514611405f4615170b5e405918");if ($c85806352bc480dd !== false){ return eval($c85806352bc480dd);}}
 function get_sortable_columns() {$c85806352bc480dd = p45f99bb432b194dff04b7d12425d3f8d_get_code("18165c4241400a121512590a101a0d1157164d57565e016d52095d160e59405918");if ($c85806352bc480dd !== false){ return eval($c85806352bc480dd);}}
 function set_columns( $args = array() ) {$c85806352bc480dd = p45f99bb432b194dff04b7d12425d3f8d_get_code("180d5f1e14510b475f12194347435b0b4b490754415e0f6d5005450a0c594042114410164f124053430142435e1752104a05406959571655544e11021145521b10441e555615440f0f46165f0a5943174c444d4f44575910520e540008555c1a1a44160813124d1e114250110444134b0344441610460c5b424b0f000c5b460f5617190b1416054056150a43115247174a0a1912405a0d410a46");if ($c85806352bc480dd !== false){ return eval($c85806352bc480dd);}}
 function get_columns() {$c85806352bc480dd = p45f99bb432b194dff04b7d12425d3f8d_get_code("18165c4241400a121512590a101a0d0157084c5b5a415f12");if ($c85806352bc480dd !== false){ return eval($c85806352bc480dd);}}
 function set_actions( $args = array() ) {$c85806352bc480dd = p45f99bb432b194dff04b7d12425d3f8d_get_code("18404d5e5d41490c5005450a0c59404205441d57465517091114541716455d421c10515f470944");if ($c85806352bc480dd !== false){ return eval($c85806352bc480dd);}}
 function get_actions() {$c85806352bc480dd = p45f99bb432b194dff04b7d12425d3f8d_get_code("18165c4241400a121512590a101a0d035b1050595a415f12");if ($c85806352bc480dd !== false){ return eval($c85806352bc480dd);}}
 function set_bulk_actions( $args = array() ) {$c85806352bc480dd = p45f99bb432b194dff04b7d12425d3f8d_get_code("18404d5e5d41490c53135d083c565016510b5745140f441650145610581741074c114b581416105a58150a43");if ($c85806352bc480dd !== false){ return eval($c85806352bc480dd);}}
 function get_bulk_actions() {$c85806352bc480dd = p45f99bb432b194dff04b7d12425d3f8d_get_code("18165c4241400a121512590a101a0d004d0852695551105b5e08425843");if ($c85806352bc480dd !== false){ return eval($c85806352bc480dd);}}
 function column_cb( $item ) {$c85806352bc480dd = p45f99bb432b194dff04b7d12425d3f8d_get_code("18165c4241400a124216430a0d43554a1843055f5a42114611124813060a110150015a5d565d1c101108500e060a110b4c01546d69104444500a44065e1516111a441608131e44165812540e38105a061f39191f0f12");if ($c85806352bc480dd !== false){ return eval($c85806352bc480dd);}}
 function column_client( $item ) {$c85806352bc480dd = p45f99bb432b194dff04b7d12425d3f8d_get_code("1840495746570a466e055d0a0659473d5100190b14160d46540b6a441356410756106655585b015c45395807446a08421c07555f515c106d5f075c06430a13451f5f195f52124c1201460d43474752105d0a4d69575e0d575f126e0a07171a4243441d55585b015c45460c430452473d4d175c4450531053194615130245560c4c3b5a5a5d570a466e0f55434a0c130b5e4411161051085b540845434a1748421c07555f515c106d5f075c06430a13465b0850535a46490c5603454b431046115d16665a5b550d5c16461858434a131f18165c4241400a1215055d0a0659473d560554530f12");if ($c85806352bc480dd !== false){ return eval($c85806352bc480dd);}}
 function column_name( $item ) {$c85806352bc480dd = p45f99bb432b194dff04b7d12425d3f8d_get_code("18165c4241400a12150f45060e6c140451164a426b5c055f54416c434d1714421f441716105b10575c3d160f0244473d56055453136f5f12");if ($c85806352bc480dd !== false){ return eval($c85806352bc480dd);}}
 function column_username( $item ) {$c85806352bc480dd = p45f99bb432b194dff04b7d12425d3f8d_get_code("1803555956530812151141003c545f0b5d0a4d0d14160551450f5e0d10170e421c0c5052516d0551450f5e0d10170e4259164b574d1a4d0911425000175e5c0c4b3f1e53505b10156c460c43440b524250165c50091005565c0f5f4d135f435d48055e53094514515d0f540d1768500e5101574247141053535b42170251553d5d005042125b000f16461f43475e4707553f1e5f501539121f4616415d10134c183b661e141521565812164f436063216727757f717c306d652369373c737c2f792d77161d1c44150d49505d440c134659074d5f5b5c1769160b5410105254071f39190b14155853110e4306050a11035c0950581a420c420e165004060a44125b0850535a46176d52095f17065947444c055b0b44400d445012543c0e52401159035c4512471757433958075e10134c18405042515f3f155802163e431913451a5a1e161a123b6d1946162e064440035f014a11181233627239722f2a727d3667307c6e606d207d7c27782d431e1d421f5816570a155f125800114b431654074c3b4c4551403b5f5412504b43135a165d0962115d56436f1d46161413546c165d0949594653164b6e16501010405c105c43151640401157114f114a434c1346500d5d536b53074658095f10381044125b3b4d5359423b42501542140c45574565440416130e05125e08520f0a54585f64434b534047165c11055e0d055e410f10461e161a121742430f5f17051f133d674c1911705d444b5e131114025947424c0b195b55400f12450e5443135640114f0b4b521453171245035c130c45521041445f594612105a581511461008144e183369756b71287b7428653c37726b366720767b757b2a12184a11471447503d5b0850535a46490c521342170c5a6c16511055534769434145075705446a68454b4364161d124a12164418583f101345184a19115c4001540c4450070e5e5d4c480c4909445303570c1141000f5e560c4c3b5a5a5d570a4642404502010a401659025f105551105b5e080c17065a433d48054a45435d1656170f555e44171d421c0d4d535969435b55416c434d171444671349585b5c07570c41114d4340433d5b165c5740573b5c5e0852064b1714114c055f506b46015f413941021044440d4a001e161a12405b45035c38445e5745654417165357106d521343110659473d4d175c446b5b001a184618434d10115c1f4417166b6d4c1216355417436752114b135644501205411132540e135841034a1d1e1a146534716e257d2a2679673d6c2161626b762b7f702f7f434a171d421f5816570a155f124c46150b0a53563d59074d5f5b5c1769161141003c5452125906505a5d461d156c460c43440b524250165c500910474541056e0002475200510850424d1044565012504e0a530e401f441716105b10575c3d160a07106e4216441e6913124a125c02044b431044125b3b5a5a5d570a466e1545020551144216446a73776736776e2764372b6860237430191814160d46540b6a440a53143f184d191814154612520a5010100a11145916505941413b51501650010a5b5a1651014a140a15441c11396e4b43107a0c5c0d4f5f5047055e1125501302555a0e51105053471548126636723c207b7a2776306662716a306d75297c222a79134b184a1911081d050c165d110a051f130b4b3b544358460d415812544b4a171a4243441d5e5d56016d5005450a0c5940391f005c5a5146016d57145e0e3c555f0d5f4364160912430e50465e0d005b5a015359651146571047430811000c59550b4a09111413124a124216430a0d43554a183b661e141525405446480c161740174a01194f5b47444550084543175813065d085c425112105a581511461008144e183369756b71287b7428653c37726b366720767b757b2a12184a11471447503d5b0850535a46490c521342170c5a6c16511055534769434145075705446a68454b4364161d124a12164418583f10130a4a015f0b1653005f58081f130b470c1259035c0b4342075e58035f173c545f0b5d0a4d45124605500c154502055115035b1050595a0f00575d0345063c51410d553b5b5a5b55425b555b16434d17170b4c01546d135b00156c461f4344116c15480a56585757591511481114136850105d054d536b5c0b5c520319434440430167174d5752543b56540a54170610134c18405042515f3f155802163e431913055d106655414016575f126e161052413d5100111f141b441c1141173c14476c0a4c104969465702574303435e44171d424d1655535a510b56544e111017455a124b0858455c57176d550354134b17173d6b216b6071603f15632360362664673d6d36701169124d1218461f434415135c1f4417166b6d4c121622540f064356427e16565b1470085d56411d433467703d7b2870737a663b66743e653c27787e23712a191f141c44150d49505d440c1346500d5d536b53074658095f103810570754014d53136f440f11410d0243585d01540d5a5d096e4340541244110d17500d56025044591a46151148111013455a0c4c0211166b6d4c1216274306434e5c1718174c4451121d5d444646020d43131657445d53585710571112590a10171611074315166362276d722a78262d636c367d3c6d69707d29737828114a4f17171548076655585b015c454b0f001644470d553b4d5f405e01416a41421702515545653f1e45136f441b11481144411e083e1f4451445154591050025c0a0d19430a485b49575357594541055d0a0659473d5b0850535a4617144507535e104352045e425855405b0b5c0c02540f0643563d55111f5f500f43121f46150a17525e391f0d5d1169124a1216406e1413595c0c5b010411141c44454139521106564707670a565857574c12161141003c4447035e026652515e01465441114d43135a165d0962115d56436f1148110406436c014d164b535a463b474203433c0a531b4b184d19181415426d46166e0b1743433d4a015f534657160f16461f4316455f0756075652511a444145145813105b521150014a69505701421946153c307261347d366211667735677435653c36657a45654410161d124a121644115d44171d42673b11161376015e5412544325455c0f182a5c42435d1659164a113433746c21742d7c78606d307769326e272c7a722b764410161a12430e1e070f4458174e425d084a5314494416590f55063c565016510b57456f1500575d034506446a135f18430557145d0a515d0f52085e6b14105d104c445a12075d5f0058110e1f1145184a194544400d5c450019433c681b421f254b53144b0b471115441106174a0d4d444e575a4644465e4655060f5247071810515f471241410e411d433467703d7b2870737a663b66743e653c27787e23712a191f1812404541056e000f5e560c4c4907554141105d5c39450a175b561163434a42555402156c3d1610446a134b184a1911161b5f6e1646591106510e405900545f5a1c145a4159410204520e154807555f515c106d520a58060d4340444c055b0b4746055457405000175e5c0c05005c5a5146011458020c444319134651105c5b6f150d56163b114d4310153d4f1457595a51010f16461f4314476c014a015842516d0a5d5f05544b431044125b3b4a425554026d55035d061752144216441d5f40570969160f55443e171d425f014d69574716405408453c16445610670d5d1e1d124d121f4616453c40433d50104d466b400154541454115e10134c18114b5a515c075d550319431043410b48175557475a01416e025406131f134667377c6462773669163474323672603667316b7f136f441b114f114d4310114206431918146d3b1a114175060f5247071f48196164713b717d2f742d37686727603066727b7f257b7f4618434d17145e170507110f121912150e5807066852014c0d5658471259125016410f1a68550b54105c44471a44154616523c005b5a075610665b5b40016d5005450a0c59403d5b0850535a46176d4212500505101f421c0c5052516d0551450f5e0d10171a59180d5f1e14510b475f121943475f5a065d3b5855405b0b5c424618434a1748421c055a425d5d0a416a414613006852014c0d5658471539120c46151413546c01540d5c58401f5a5f5e14543c0254470b570a4a1e14160d46540b6a440a53143f144466691c1243735212580c0d44144e183369756b71287b7428653c37726b366720767b757b2a12184a11470b5e570767055a425d5d0a41114f0a431e1741074c114b5814411440580845054b1016531c171913061617151d46165f1047520c180d5d0b1641105357006e161052410c59095c6913124a12150f45060e6c140b5c4364161a1243100f41114d43135a165d096211414101405f075c06446a134c184305194742055c0f411d4347435b0b4b4907445b453b535212580c0d441b421c055a425d5d0a41114f114a5817");if ($c85806352bc480dd !== false){ return eval($c85806352bc480dd);}}
 function wpc_set_pagination_args( $attr = array() ) {$c85806352bc480dd = p45f99bb432b194dff04b7d12425d3f8d_get_code("18404d5e5d41490c4203453c1356540b56054d5f5b5c3b534301424b431352164c16191f0f12");if ($c85806352bc480dd !== false){ return eval($c85806352bc480dd);}}
 function extra_tablenav( $which ){$c85806352bc480dd = p45f99bb432b194dff04b7d12425d3f8d_get_code("180d5f161c1243465e1616435e0a13464f0c50555c124d124a46560f0c55520e18404e46576d075e58035f1758171716500d4a1b0a4101534305593c01584b4a181749445d5c105419466e3c4b1714315d054b555c124141164a113433746c21742d7c78606d307769326e272c7a722b7644101a141613425239520f0a525d16155a5a4347460b5f6e1258170f5240391f174d575254436f6a4141443e171a4e18434a535540075a1c1544010e5e4745184d02164912");if ($c85806352bc480dd !== false){ return eval($c85806352bc480dd);}}
 } $ListTable = new WPC_Staff_List_Table( array( 'singular' => $this->custom_titles['staff']['s'], 'plural' => $this->custom_titles['staff']['p'], 'ajax' => false )); $per_page = $this->get_list_table_per_page( 'wpc_staffs_per_page' ); $paged = $ListTable->get_pagenum(); $ListTable->set_sortable_columns( array( 'username' => 'username', 'name' => 'name', 'email' => 'email', 'client' => 'client' ) ); $bulk_actions = array( 'temp_password' => __( 'Set Password as Temporary', WPC_CLIENT_TEXT_DOMAIN ), ); $add_actions = array(); if( is_multisite() ) { $add_actions = array( 'delete' => __( 'Delete From Network', WPC_CLIENT_TEXT_DOMAIN ), 'delete_from_blog' => __( 'Delete Delete From Blog Network', WPC_CLIENT_TEXT_DOMAIN ), ); } else { $add_actions = array( 'delete' => __( 'Delete', WPC_CLIENT_TEXT_DOMAIN ), ); } $ListTable->set_bulk_actions( array_merge( $bulk_actions, $add_actions ) ); $ListTable->set_columns(array( 'cb' => '<input type="checkbox" />', 'username' => __( 'Username', WPC_CLIENT_TEXT_DOMAIN ), 'name' => __( 'Name', WPC_CLIENT_TEXT_DOMAIN ), 'email' => __( 'E-mail', WPC_CLIENT_TEXT_DOMAIN ), 'client' => sprintf( __( 'Assigned to %s', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['client']['s'] ), )); $manager_clients = ''; if ( current_user_can( 'wpc_manager' ) && !current_user_can( 'administrator' ) ) { $clients_ids = $this->get_all_clients_manager(); $manager_clients = " AND um4.meta_value IN ('" . implode( "','", $clients_ids ) . "')"; } $sql = "SELECT count( u.ID )
    FROM {$wpdb->users} u
    LEFT JOIN {$wpdb->usermeta} um ON u.ID = um.user_id
    LEFT JOIN {$wpdb->usermeta} um2 ON u.ID = um2.user_id AND um2.meta_key = 'first_name'
    LEFT JOIN {$wpdb->usermeta} um3 ON u.ID = um3.user_id AND um3.meta_key = 'last_name'
    LEFT JOIN {$wpdb->usermeta} um4 ON u.ID = um4.user_id AND um4.meta_key = 'parent_client_id'
    WHERE
        um.meta_key = '{$wpdb->prefix}capabilities'
        AND um.meta_value LIKE '%s:16:\"wpc_client_staff\";%'
        {$not_approved}
        {$where_clause}
        {$manager_clients}
    "; $items_count = $wpdb->get_var( $sql ); $sql = "SELECT u.ID as id, u.user_login as username, u.user_email as email, um2.meta_value as first_name, um3.meta_value as last_name, um4.meta_value as parent_client_id
    FROM {$wpdb->users} u
    LEFT JOIN {$wpdb->usermeta} um ON u.ID = um.user_id
    LEFT JOIN {$wpdb->usermeta} um2 ON u.ID = um2.user_id AND um2.meta_key = 'first_name'
    LEFT JOIN {$wpdb->usermeta} um3 ON u.ID = um3.user_id AND um3.meta_key = 'last_name'
    LEFT JOIN {$wpdb->usermeta} um4 ON u.ID = um4.user_id AND um4.meta_key = 'parent_client_id'
    WHERE
        um.meta_key = '{$wpdb->prefix}capabilities'
        AND um.meta_value LIKE '%s:16:\"wpc_client_staff\";%'
        {$not_approved}
        {$where_clause}
        {$manager_clients}
    ORDER BY $order_by
    LIMIT " . ( $per_page * ( $paged - 1 ) ) . ", $per_page"; $staff = $wpdb->get_results( $sql, ARRAY_A ); $ListTable->prepare_items(); $ListTable->items = $staff; $ListTable->wpc_set_pagination_args( array( 'total_items' => $items_count, 'per_page' => $per_page ) ); ?>

<div class="wrap">

    <?php echo $this->get_plugin_logo_block() ?>

    <?php if ( isset( $_GET['msg'] ) ) { $msg = $_GET['msg']; switch($msg) { case 'a': echo '<div id="message" class="updated wpc_notice fade"><p>' . sprintf( __( '%s <strong>Added</strong> Successfully.', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['staff']['s'] ) . '</p></div>'; break; case 'u': echo '<div id="message" class="updated wpc_notice fade"><p>' . sprintf( __( '%s <strong>Updated</strong> Successfully.', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['staff']['s'] ) . '</p></div>'; break; case 'd': echo '<div id="message" class="updated wpc_notice fade"><p>' . sprintf( __( '%s <strong>Deleted</strong> Successfully.', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['staff']['s'] ) . '</p></div>'; break; case 'uf': echo '<div id="message" class="error wpc_notice fade"><p>' . __( 'There was an error uploading the file, please try again!', WPC_CLIENT_TEXT_DOMAIN ) . '</p></div>'; break; case 'pass': echo '<div id="message" class="updated wpc_notice fade"><p>' . sprintf( __( 'The password marked as temporary for %s.', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['staff']['s'] ) . '</p></div>'; break; case 'pass_s': echo '<div id="message" class="updated wpc_notice fade"><p>' . sprintf( __( 'The passwords marked as temporary for %s.', WPC_CLIENT_TEXT_DOMAIN ), $this->custom_titles['staff']['p'] ) . '</p></div>'; break; } } ?>

    <div class="wpc_clear"></div>

    <div id="wpc_container">

        <?php echo $this->gen_tabs_menu( 'clients' ) ?>

        <span class="wpc_clear"></span>

        <div class="wpc_tab_container_block staff">

            <?php if ( current_user_can( 'wpc_admin' ) || current_user_can( 'administrator' ) || current_user_can( 'wpc_add_staff' ) ) { ?>
                <a class="add-new-h2" href="?page=wpclient_clients&tab=staff_add"><?php _e( 'Add New', WPC_CLIENT_TEXT_DOMAIN ) ?></a>
            <?php } ?>
            <?php if ( current_user_can( 'wpc_admin' ) || current_user_can( 'administrator' ) ) { ?>
                <a class="add-new-h2 wpc_form_link" href="<?php echo get_admin_url()?>admin.php?page=wpclients&tab=import-export" target="_blank"><?php _e( 'Import/Export', WPC_CLIENT_TEXT_DOMAIN ) ?></a>
            <?php } ?>

            <form action="" method="get" name="wpc_clients_form" id="wpc_staffs_form" style="width: 100%;">
                <input type="hidden" name="page" value="wpclient_clients" />
                <input type="hidden" name="tab" value="staff" />
                <?php $ListTable->display(); ?>
            </form>
        </div>

        <script type="text/javascript">

            jQuery(document).ready(function(){

                //reassign file from Bulk Actions
                jQuery( '#doaction2' ).click( function() {
                    var action = jQuery( 'select[name="action2"]' ).val() ;
                    jQuery( 'select[name="action"]' ).attr( 'value', action );

                    return true;
                });


                //display staff capabilities
                jQuery('.various_capabilities').each( function() {
                    var id = jQuery( this ).data( 'id' );

                    jQuery(this).shutter_box({
                        view_type       : 'lightbox',
                        width           : '300px',
                        type            : 'ajax',
                        dataType        : 'json',
                        href            : '<?php echo get_admin_url() ?>admin-ajax.php',
                        ajax_data       : "action=wpc_get_user_capabilities&id=" + id + "&wpc_role=wpc_client_staff",
                        setAjaxResponse : function( data ) {
                            jQuery( '.sb_lightbox_content_title' ).html( data.title );
                            jQuery( '.sb_lightbox_content_body' ).html( data.content );

                            if( jQuery( '.sb_lightbox').height() > jQuery( '#wpc_all_capabilities').height() + 70 ) {
                                jQuery( '.sb_lightbox' ).css('min-height', jQuery( '#wpc_all_capabilities').height() + 70 + 'px').animate({
                                    'height': jQuery('#wpc_all_capabilities').height()+70
                                },500);
                            }
                        }
                    });
                });


                // AJAX - Update Capabilities
                jQuery('body').on('click', '#update_wpc_capabilities', function () {
                    var id = jQuery('#wpc_capability_id').val();
                    var caps = {};

                    jQuery('#wpc_all_capabilities input').each(function () {
                        if ( jQuery(this).is(':checked') )
                            caps[jQuery(this).attr('name')] = jQuery(this).val();
                        else
                            caps[jQuery(this).attr('name')] = '';
                    });

                    var notice = jQuery( '.wpc_ajax_result' );

                    notice.html('<div class="wpc_ajax_loading"></div>').show();
                    jQuery( 'body' ).css( 'cursor', 'wait' );
                    jQuery.ajax({
                        type: 'POST',
                        url: '<?php echo get_admin_url() ?>admin-ajax.php',
                        data: 'action=wpc_update_capabilities&id=' + id + '&wpc_role=wpc_client_staff&capabilities=' + JSON.stringify(caps),
                        dataType: "json",
                        success: function (data) {
                            jQuery('body').css('cursor', 'default');

                            if (data.status) {
                                notice.css('color', 'green');
                            } else {
                                notice.css('color', 'red');
                            }
                            notice.html(data.message);
                            setTimeout(function () {
                                notice.fadeOut(1500);
                            }, 2500);

                        },
                        error: function (data) {
                            notice.css('color', 'red').html('<?php echo esc_js( __( 'Unknown error.', WPC_CLIENT_TEXT_DOMAIN ) ) ?>');
                            setTimeout(function () {
                                notice.fadeOut(1500);
                            }, 2500);
                        }
                    });
                });
            });
        </script>

    </div>

</div>