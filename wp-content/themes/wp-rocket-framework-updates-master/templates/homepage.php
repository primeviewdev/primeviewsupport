<?php
/**
 * Template Name: Front Page Template
 *
 */
get_header(); ?>
	<div id="primary" class="homepage site-content">
	
		<div class="jumbotron">
			<div class="container">
				<div class="row" role="main">
					<div class="col-md-12">		
						<h1 class="display-3">Primeview Support</h1>
						<p class="lead">Your portal to connect with our support staff and administrators or submit a support request ticket</p>
						<p><a class="btn btn-lg btn-success" href="/portal/login/" role="button">Log In Here</a></p>
						<p><a class="btn btn-lg btn-success" href="/primeview-support-new-account-registration/" role="button">Register Here</a></p>
					</div>
				</div>
			</div>
		</div>
		<div class="container p-3">
			<div class="row">
				<div class="col-md-12">
					<h2 class="mb-3">Content</h2>
					<p>Welcome to your Primeview and Optimizex support portal!</p>
					<p>Support calendar can be viewed from your personal portal page</p>
					<p><a href="/portal/login/">Login</a> or <a href="/primeview-support-new-account-registration/">register</a></p>
				</div>
			</div>
		</div>
		
	</div><!-- .primary -->
<?php get_footer(); ?>